package graf.art.allpicturesintheworld;

import java.awt.Color;

public class Pixel {
	
	private static final Color on = Color.black;
	private static final Color off = Color.white;

	private int val = 0;
	private Pixel nextPixel;
	private int x, y;
	
	private int exponent = 0;
	
	public Pixel(int x, int y, int val, Pixel nextPixel) {
		this.x = x;
		this.y = y;
		this.val = val;
		this.nextPixel = nextPixel;
	}
	
	public void setExponent(int exponent) {
		this.exponent = exponent;
	}
	
	public int getExponent() {
		return exponent;
	}
	
	public int getVal() {
		return val;
	}
	
	public void increaseVal() {
		val++;
		if (val==2) {
			val=0;
			if (nextPixel!=null) nextPixel.increaseVal();
		}
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public Color getColor() {
		return (val==0?off:on);
	}

	public void setX(int x) {
		this.x=x;
	}
	
	public void setY(int y) {
		this.y=y;
	}
}
