package graf.art.allpicturesintheworld;

public class PixelPainting {

	public static final int CLEAR = 0;
	public static final int RANDOM = 1;
	
	private int width, height;
	private Pixel[][] pixel;

	public PixelPainting(int width, int height, int init) {
		this.width = width;
		this.height = height;

		Pixel lastPixel = null;
		pixel = new Pixel[width][height];
		for (int y=0;y<height;y++) {
			for (int x=0;x<width;x++) {
				pixel[x][y] = new Pixel(x,y,(init==CLEAR?0:(Math.random()<0.5?0:1)),lastPixel);
				lastPixel = pixel[x][y];
			}
		}
		
		int exponent=0;
		for (int y=height-1;y>=0;y--) {
			for (int x=width-1;x>=0;x--) {
				pixel[x][y].setExponent(exponent++);
			}
		}
	}

	public void increase() {
		pixel[width-1][height-1].increaseVal();
	}

	public Pixel getPixel(int x, int y) {
		return pixel[x][y];
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void swapPixelPositions() {
		for (int y=0;y<height;y++) for (int x=0;x<width;x++) {
			int x0 = x;//(int)(Math.random()*(width-1));
			int y0 = y;//(int)(Math.random()*(height-1));
			int x1=x0, y1=y0;
			while (x1==x0&&y1==y0) {
				x1 = (int)(Math.random()*(width-1));
				y1 = (int)(Math.random()*(height-1));
			}

			int oldXpos = pixel[x0][y0].getX();
			int oldYpos = pixel[x0][y0].getY();
			pixel[x0][y0].setX(pixel[x1][y1].getX());
			pixel[x0][y0].setY(pixel[x1][y1].getY());
			pixel[x1][y1].setX(oldXpos);
			pixel[x1][y1].setY(oldYpos);
		}
	}

	public void getAsNumber() {
		long number = 0;
		for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
			number += pixel[x][y].getVal() * Math.pow(2,pixel[x][y].getExponent());
		}
		//TODO
	}

}
