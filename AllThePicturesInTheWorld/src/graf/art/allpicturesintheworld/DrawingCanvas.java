package graf.art.allpicturesintheworld;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

public class DrawingCanvas extends Canvas {

	int width, height, pixelSize;
	Image backbuffer;
	Graphics backg;

	public DrawingCanvas(int width, int height, int pixelSize) {
		super();
		this.width = width;
		this.height = height;
		this.pixelSize = pixelSize;
		setPreferredSize(new Dimension(width*pixelSize,height*pixelSize));
		setVisible(true);
	}

	public void init() {
		backbuffer = createImage(width*pixelSize,height*pixelSize);
		backg = backbuffer.getGraphics();
	}

	/*public void drawRectangles() {
		for (int i=0;i<50;i++) {
			if (Math.random()<0.5) 
				backg.setColor( Color.white );
			else
				backg.setColor( Color.black );
			backg.fillRect((int)(Math.random()*100), (int)(Math.random()*100), 4, 4);
		}
	}*/

	public void drawPixelPainting(PixelPainting painting) {
		for (int y=0;y<painting.getHeight();y++) {
			for (int x=0;x<painting.getWidth();x++) {
				Pixel p = painting.getPixel(x,y);
				backg.setColor(p.getColor());
				backg.fillRect(p.getX()*pixelSize, p.getY()*pixelSize, pixelSize, pixelSize);
			}
		}
	}

	public void paint(Graphics g) {
		update(g);
	}

	public void update(Graphics g) {
		g.drawImage(backbuffer,0,0,this);
	}




}
