package graf.art.allpicturesintheworld;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JFrame;

public class Main extends JFrame {
	
	public static void main(String[] args) {
		new Main(200,200,1);
	}
	
	private static final int pixelsToCalcPerCycle = (int)(Math.pow(2, 20)-1);
	private static final int cycleTime = 1;
	
	private final int width, height, pixelSize;	
	private DrawingCanvas canvas;
	
	public Main(int width, int height, int pixelSize) {
		super("All the pictures in the world");
		this.width=width;
		this.height=height;
		this.pixelSize = pixelSize;
		setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(width*pixelSize,26+height*pixelSize));
		//setResizable(false);
		canvas = new DrawingCanvas(width,height,pixelSize);
		add(canvas,BorderLayout.SOUTH);
		pack();
		setVisible(true);
		canvas.init();
		
		PixelPainting painting = new PixelPainting(width,height,PixelPainting.RANDOM);
		painting.swapPixelPositions();
		
		while(true) {
			painting = new PixelPainting(width,height,PixelPainting.RANDOM);
			//for (int i=0;i<pixelsToCalcPerCycle;i++) painting.increase();
			//painting.getAsNumber();
			canvas.drawPixelPainting(painting);
			canvas.repaint();
			try {
				Thread.sleep(cycleTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
