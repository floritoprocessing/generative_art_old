void setup() {
  size(400,400); 
  colorMode(HSB,255); background(0,0,0);
  //smooth();
  //framerate(25);
  initGravity();
  setupParticles();
}

void draw() {
  //background(0,0,0);
  updateGravity();
  updateParticles();
  drawParticles();
}




void initGravity() { grav=new Vec(random(TWO_PI),0.0002); }
void updateGravity() { if (random(1)<0.1) { grav.setDir(random(TWO_PI)); } }

Vec grav;
class Vec {
  float rd=0, st=0, x=0, y=0;
  Vec(float _rd, float _st) { rd=_rd; st=_st; createXY(); }
  void setDir(float _rd) {rd=_rd; createXY();}
  void setStren(float _st) {st=_st; createXY(); }
  void createXY() { x=st*cos(rd); y=st*sin(rd); } 
}



/*
PARTICLE CLASS
*/

int nrOfParticles=50;
Particle[] particle;

void setupParticles() {
  particle=new Particle[nrOfParticles];
  for (int i=0;i<nrOfParticles;i++) {
    particle[i]=new Particle(width,height);
  }
}

void updateParticles() { for (int i=0;i<nrOfParticles;i++) { particle[i].update(); } }
void drawParticles() { for (int i=0;i<nrOfParticles;i++) { particle[i].toScreen(); } }

class Particle {
  float m=0.005;
  float sqm=sq(m)+sq(m);
  float damp=0.98;
  float gravDevFreq=random(0.5,5);
  float x=random(1), y=random(1), lx=x, ly=y;
  float xm=random(-m,m), ym=random(-m,m);
  float h=random(12), hm=0.25;
  float xscl,yscl;
  
  Particle(float _xscl, float _yscl) {
    xscl=_xscl; yscl=_yscl;
  }
  
  void newRandomPos() { x=random(1); y=random(1); lx=x; ly=y; }
  
  void update() {
    h+=hm;
    lx=x; ly=y;
    float gravDev=radians(15)*sin(millis()/(1000.0*gravDevFreq));
    Vec tgrav=new Vec(grav.rd+gravDev,grav.st);
    xm+=tgrav.x; ym+=tgrav.y;
    xm*=damp; ym*=damp;
    x+=xm; if (x>1||x<0) { newRandomPos(); }
    y+=ym; if (y>1||y<0) { newRandomPos(); }
  }
  
  void toScreen() {
    float i=(sq(xm)+sq(ym))/sqm;
    float th=i*10+h; if (th>255) {th-=255;}
    float ts=255-i*700; 
    if (ts>0) {
      color c=color(i*th,255,ts);
      stroke(c);
      line(lx*xscl,ly*yscl,x*xscl,y*yscl);
    }
  }
}
