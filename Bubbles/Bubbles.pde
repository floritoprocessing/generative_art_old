// trajectory of rising bubbles
// code based on the article "Trajectories of rising bubbles" by Shizuo Yoshida and Pichard Manasseh
// published on the web: http://resources.highett.cmit.csiro.au/RManasseh/a916/a916.html

int actBubble, nrOfBubbles;
color navyBlue=#23238e, lightskyblue1=#b0e2ff, black=#000000;

Bubble[] bubble;
PhaseSet[] phaseset; int actphaseset, nrOfPhaseSets;


void setup() {
  // INIT SCREEN:
  size(600,400,P3D); background(navyBlue); //framerate(50);
  
  // INIT PHASESET: (used by Bubbles)
  actphaseset=0; nrOfPhaseSets=width*height;
  phaseset=new PhaseSet[nrOfPhaseSets];
  for (int i=0;i<nrOfPhaseSets;i++) {
    phaseset[i]=new PhaseSet();
  }
  
  // INIT BUBBLES
  actBubble=0; nrOfBubbles=2000;
  bubble=new Bubble[nrOfBubbles];
  for (int i=0;i<nrOfBubbles;i++) {
    bubble[i]=new Bubble();
    bubble[i].active=false;
  }
  actBubble=0;
}



void draw() {
  background(navyBlue);
  
  // CREATE BUBBLES WHERE MOUSE IS
  actphaseset=mouseX+width*mouseY;
  bubble[actBubble].init(actBubble,phaseset[actphaseset],phaseset[actphaseset].s,mouseX,mouseY);
  // increase activeBubbleNr
  actBubble++; if (actBubble==nrOfBubbles) {actBubble=0;}
  
  // UPDATE ALL BUBBLES
  for (int i=0;i<nrOfBubbles;i++) {
    bubble[i].update();
  }
}



// class Bubble
// ------------------------------------------------------------------
// bubble moves dependent from its size
// bubble needs to be initialized through init(indexNr, Phaseset, size, xpos, ypos)
// buuble updates through update()
class Bubble {
  float x,y,z,xs,ys,zs;  // position and startposition
  float s;               // size in mm (2..4)
  float sms;             // time in milliseconds at creation
  float ymov=300.0/50.0; // 300 mm/sec y-movement
  float wavlen, xfreq, zfreq; // wavelength -> x- and z-frequency (dependent from s);
  float critHeight;           // height at wich sin-movement is fully extended (dependent from s);
  int nrOfTrajectories;       // nr of trajectories that a bubble could follow (2..5) -> see '5' in phaseset (dependent from s);
  float amp;                  // amplitude of sin-movement (dependent from s);
  float xph=0.0, zph=0.0;     // phase of sin-movement (dependent from pixel where bubble is launched)
  float mainScale=.8;         // scale for translating mm to pixel
  boolean active;
  int nr;

  Bubble() {
    active=false;
  }
  
  void init(int n, PhaseSet phset, float si, float xi, float yi) {
    nr=n; 
    s=si;                                       // in mm                 | 4..2
    wavlen=50+(4.0-s)*50.0;                     // in mm                 | 50 .. 100
    xfreq=(ymov*50.0)/wavlen; zfreq=xfreq-0.01; // wavelength of sin-movement
    critHeight=35.0+5.0*(s-2.0);                // in mm                 | 45  .. 35 
    nrOfTrajectories=2+int(1.5*(4.0-s));        //                       | 2   .. 5
    amp=4.5+2.5*(4.0-s);                        // in mm                 | 4.5 .. 9.5                
    xph=phset.x[nr%nrOfTrajectories]; zph=phset.z[nr%nrOfTrajectories];
    x=xi; xs=xi; y=yi; ys=yi; z=0.0; zs=0.0; 
    active=true; sms=millis();
  }
  
  void update() {
    if (active) {
      float sintrav=(ys-y)/critHeight; if (sintrav>1.0) {sintrav=1.0;};
      sintrav=pow(sintrav,2);
      x=xs+mainScale*( sintrav*(amp*sin(xfreq*TWO_PI*(millis()-sms)/1000.0+xph) + amp*cos(zfreq*TWO_PI*(millis()-sms)/1000.0+zph)) );
      z=zs+mainScale*( sintrav*(amp*cos(xfreq*TWO_PI*(millis()-sms)/1000.0+xph) + amp*sin(zfreq*TWO_PI*(millis()-sms)/1000.0+zph)) );
      y-=mainScale*ymov;
      if (y<1) {
        active=false;
      } else {
        stroke(lightskyblue1);
        point(x,y,z);
        for (float rd=0;rd<TWO_PI;rd+=TWO_PI/12.0) {
          point(x+mainScale*s*cos(rd),y+mainScale*s*sin(rd),z);
        }
      }
    }
  }
}
// ------------------------------------------------------------------
// end class Bubble



// class PhaseSet
// ------------------------------------------------------------------
// holds 5 x- and y-phases (0..TWO_PI) 
// holds one s value (2..4)
// used for storing phases and bubble sizes for each pixel on screen
class PhaseSet {
  float[] x,z;
  float s;
  PhaseSet() {
    x=new float[5]; z=new float[5];
    for (int i=0;i<5;i++) {
      x[i]=random(TWO_PI); z[i]=random(TWO_PI);
    }
    s=pow(random(1),4)*2.0+2.0;
  }
}
// ------------------------------------------------------------------
// end class PhaseSet
