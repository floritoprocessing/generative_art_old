import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; import netscape.javascript.*; import javax.comm.*; import javax.sound.midi.*; import javax.sound.midi.spi.*; import javax.sound.sampled.*; import javax.sound.sampled.spi.*; import javax.xml.parsers.*; import javax.xml.transform.*; import javax.xml.transform.dom.*; import javax.xml.transform.sax.*; import javax.xml.transform.stream.*; import org.xml.sax.*; import org.xml.sax.ext.*; import org.xml.sax.helpers.*; public class Constellations extends BApplet {BImage imgBlob;
int nrOfBlobs=400; Blob[] blob=new Blob[400];
float gDiag;

void setup() {
  size(400,300); noFill(); stroke(255); ellipseMode(CENTER_RADIUS); framerate(100); imageMode(CENTER_DIAMETER);
  gDiag=sqrt(sq(width)+sq(height));
  
  imgBlob = loadImage("blob.gif");
  brightToAlpha(imgBlob);
  
  for (int i=0;i<nrOfBlobs;i++) {
    blob[i]=new Blob(i);
  }
}

void loop() {
  background(0);
  for (int i=0;i<nrOfBlobs;i++) {
    blob[i].update();
  }
  for (int i=0;i<nrOfBlobs;i++) {
    blob[i].drawBlob();
//    blob[i].drawCircle();
    blob[i].drawConnections();
  }
  
//  displayFramerate();
//  saveFrame("c:/constel-####.tga");
}

class Blob {
  int id;
  float x,y,xv,yv;
  float r, rb;
  float amp,ampb, freq,ph;
  boolean[] connected;
  
  Blob(int in_id) {
    id=in_id; 
    connected=new boolean[nrOfBlobs]; for (int i=0;i<nrOfBlobs;i++) {connected[i]=false;}
    x=random(width);y=random(height);
    rb=random(6,10)/1.3f;
    ampb=random(3,6)/1.3f; amp=ampb; freq=random(2,4); ph=random(TWO_PI);
  }
  
  void update() {
    // invert direction and bounce more if edge:
    if (x-r<0) {xv=abs(xv)+abs(x-r)*0.1f;amp+=(ampb-amp)*0.1f;}
    if (x+r>width) {xv=-abs(xv)-abs(x+r-width)*0.1f;amp+=(ampb-amp)*0.1f;}
    if (y-r<0) {yv=abs(yv)+abs(y-r)*0.1f;amp+=(ampb-amp)*0.1f;}
    if (y+r>height) {yv=-abs(yv)-abs(y+r-height)*0.1f;amp+=(ampb-amp)*0.1f;}
    
    // random movement:
    xv+=random(-0.05f,0.05f); yv+=random(-0.05f,0.05f);
    
    // urge to go to the mouse:
    xv+=(width/2.0f-x)*0.0001f; yv+=(height/2.0f-y)*0.0001f;
    
    int nrOfConnections=0;
    // check if intersecting with one of the other blobs:
    for (int i=0;i<nrOfBlobs;i++) {
      if (i!=id) {
        // IF NOT CONNECTED:
        if (!connected[i]) {
          // bounce off other ones and increase selfbounce..
          if (dist(x,y,blob[i].x,blob[i].y)-r-blob[i].r<0) {
            if (random(1)<0.005f) {
              // .. and sometimes connect instead of rebound
              connected[i]=true;
              blob[i].connected[id]=true;
            } else {
              xv+=(x-blob[i].x)*0.005f;
              yv+=(y-blob[i].y)*0.005f;
              amp+=(ampb-amp)*0.08f;
            }
          }
        }
        // IF CONNECTED:
        if (connected[i]) {
          amp+=(blob[i].amp-amp);
          nrOfConnections++; // count connections
          float ds=dist(x,y,blob[i].x,blob[i].y)-r-blob[i].r;
          if (ds>0) { // if touching or further: 
            if (ds>2*(blob[i].r+r)) { // disconnect if over a certain distance:
              connected[i]=false;
              blob[i].connected[id]=false;
            } else { // else draw to eachother
              xv+=(blob[i].x-x)*0.001f;
              yv+=(blob[i].y-y)*0.001f;
            }
          } else { // go away from each other to avoid overlap;
            xv+=(x-blob[i].x)*0.001f;
            yv+=(y-blob[i].y)*0.001f;
            amp+=(ampb-amp)*0.04f;
          }
        }
      }
    }
    // break if too many connections:
    if (nrOfConnections>3) {
      for (int i=0;i<nrOfBlobs;i++) {
        if (connected[i]) {
          connected[i]=false;
          blob[i].connected[id]=false;
        }
      }
      if (random(1)<0.01f) {blob[id]=new Blob(id);}  // random re-init
    }
    
    // check if intersecting with mouse
    if (mousePressed) {
      float dm=dist(x,y,mouseX,mouseY);
      if (dm<50&&pmouseX!=0&&pmouseY!=0) {
        amp+=(ampb-amp)*0.5f*dist(mouseX,mouseY,pmouseX,pmouseY)/gDiag*30;
        xv+=(mouseX-pmouseX)*0.03f*(dm/50.0f);
        yv+=(mouseY-pmouseY)*0.03f*(dm/50.0f);
      }
    }
    
    //damping of movement:
    xv*=0.94f; yv*=0.94f;
    x+=xv; y+=yv;
    amp*=0.9f;
    r=rb+amp*cos(freq*millis()/1000.0f+ph);

    if (random(1)<0.0001f) {blob[id]=new Blob(id);}  // random re-init
  }
  
  void drawConnections() {
    stroke(255,255,255,32);
    for (int ti=0;ti<nrOfBlobs;ti++) {
      if (id!=ti&&blob[id].connected[ti]) {
        line(x,y,blob[ti].x,blob[ti].y);
      }
    }
  }
  
  void drawCircle() {
    stroke(255); noFill();
    ellipse(x,y,r,r);
  }
  
  void drawBlob() {
    push();
    translate(0,0,-1);
    image(imgBlob, x, y, r*amp/2, r*amp/2);
    pop();
  }
}

int lastFrame;
void displayFramerate() {
  fps=1000/(float)(millis()-lastFrame);
  lastFrame=millis();
  stroke(128);
  for (int i=1;i<=fps;i++) {
    if (i%10==0) {fill(255,0,0);} else {fill(0,0,0);}
    rect(i*4,4,3,3);
  }
}

void brightToAlpha(BImage b) {
  b.format = RGBA;
  for(int i=0; i < b.pixels.length; i++) {
    b.pixels[i] = color(255,255,255,brightness(b.pixels[i])/1.2f);
  }
}
}