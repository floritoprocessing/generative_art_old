Splatter splat;
float diag;

float z;

void setup() {
  size(600,300); background(255,255,255); diag=sqrt(sq(width)+sq(height));
  z=diag*0.01;
  
  splat=new Splatter(z); splat.drawMe();
}


void draw() {
  z*=1.002;
  if (z<diag*0.1) {
    if (random(1)<0.5) {splat=new Splatter(z); println("Z: "+z+" diag: "+diag*0.1);}
    splat.randomize();
    splat.drawMe();
  }
}


class Stengel {
  float x,y,l,sl;
  int c;
  float[] fr=new float[3]; float[] amp=new float[3];
  
  Stengel(float in_x, float in_y, float in_l) {
    x=in_x; y=in_y; l=in_l; sl=y+l*random(4,12);
    c=(int)random(150,230)<<16|(int)random(60,140)<<8|(int)random(0,40);  // set color
    for (int i=0;i<3;i++) {fr[i]=random(0.5,2); amp[i]=pm_random(0,0.5*l); }
  }
  
  void drawMe() {
    float xp;
    for (int jj=0;jj<l*5;jj++) {
      for (float yp=y; yp<sl; yp+=0.5) {
        xp=x;
        for (int i=0;i<3;i++) { xp+=amp[i]*sin(fr[i]*(yp-y)/(sl-y)*PI); }
        softSet(int(xp),int(yp),c,0.1);
      }
      for (int i=0;i<3;i++) { 
        fr[i]+=random(-0.05,0.05);
        amp[i]+=random(-0.01*l,0.01*l);
      }
    }
  }
}



class Splatter {
  float x,y,r;
  int c;
  
  float ra; int[] fr; float[] amp; float[] ph;
  
  Splatter(float rr) {
    c=(int)random(64)<<16|(int)random(64,256)<<8|(int)random(64);  // set color
    x=random(width); y=random(height); r=random(rr*0.1,rr);        // set position and size
    fr=new int[10]; amp=new float[10]; ph=new float[10];           // outline sinuses
    for (int i=0;i<10;i++) { fr[i]=(int)random(24); amp[i]=random(0.2*r); ph[i]=random(TWO_PI); }
    Stengel stengel=new Stengel(x,y,r);
    stengel.drawMe();
  }
  
  void randomize() {
    float rnd_pos=0.001;  // random position change
    float rnd_r=0.002;    // random radius change
    x+=random(-width*rnd_pos,width*rnd_pos); y+=random(-height*rnd_pos,height*rnd_pos);
    r+=random(-diag*rnd_r,diag*rnd_r);
    int nm=(int)random(10);
    for (int n=0;n<nm;n++) {
      int i=(int)random(10);
      fr[i]+=(int)random(-1,1);
      amp[i]+=random(-0.01*r,0.01*r);
      ph[i]+=random(-TWO_PI/1000.0,TWO_PI/1000.0);
    }
  }
  
  void drawMe() {
    for (float mr=0;mr<=r;mr+=0.5) {
      float cs=mr/r; cs=0.5+cs*0.5;
      for (float rd=0;rd<=TWO_PI;rd+=TWO_PI/720.0) {
        ra=mr;
        for (int i=0;i<10;i++) { ra+=amp[i]*cos(fr[i]*rd+ph[i]); }
        softSet(int(x+ra*cos(rd)),int(y+ra*sin(rd)),c,0.4*cs);
      }
    }
  }
}

void softSet(int x, int y, int c, float p) {
  if (x>=0&&x<width&&y>=0&y<height) {
    float ps=1.0-p;
    float rc=c>>16&255, gc=c>>8&255, bc=c&255;
    int s=get(x,y);
    float rs=s>>16&255, gs=s>>8&255, bs=s&255;
    float rr=p*rc+ps*rs, gg=p*gc+ps*gs, bb=p*bc+ps*bs;
    int dc=255<<24 | int(rr)<<16|int(gg)<<8|int(bb);
    set(x,y,dc);
  }
}


float pm_random(float a,float b) {
  return (random(1)<0.5?-1:1)*random(a,b);
}
