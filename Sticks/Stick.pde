class Stick {
  
  Vec position = new Vec();
  float len;
  float anchor;
  double direction;
  double rotation;
  Vec[] lastDot = new Vec[2];
  Vec[] dot = new Vec[2];
  
  Stick(Vec _position, float _len, float _anchor, float _direction, float _rotation) {
    position.set(_position);
    len = _len;
    anchor = min(1,max(0,_anchor));
    direction = _direction;
    rotation = _rotation;
    dot[0] = new Vec();
    dot[1] = new Vec();
    lastDot[0] = new Vec();
    lastDot[1] = new Vec();
    makeDots();
  }
  
  void rotate() {
    direction += rotation;
    makeDots();
  }
  
  void makeDots() {
    lastDot[0].set(dot[0]);
    lastDot[1].set(dot[1]);
    dot[0].set(0,len*anchor,0);
    dot[1].set(0,-len*(1-anchor),0);
    dot[0].rotZ(direction);
    dot[1].rotZ(direction);
    dot[0].add(position);
    dot[1].add(position);
  }
  
  void lineOnImage(GrafImage img) {
    img.line(lastDot[0].X,lastDot[0].Y,dot[0].X,dot[0].Y,0xFFFF5020,0.1);
    img.line(lastDot[1].X,lastDot[1].Y,dot[1].X,dot[1].Y,0xFFFF5020,0.1);
  }
  
  void drawOnImage(GrafImage img) {
    img.set((int)dot[0].X,(int)dot[0].Y,0xFFFF5020,0.1);
    img.set((int)dot[1].X,(int)dot[1].Y,0xFFFF5020,0.1);
  }
  
  void show() {
    stroke(255,255,255,128);
    fill(0,0,0,128);
    ellipseMode(RADIUS);
    ellipse((float)position.X,(float)position.Y,5,5);
    ellipse((float)dot[0].X,(float)dot[0].Y,2,2);
    ellipse((float)dot[1].X,(float)dot[1].Y,2,2);
    line((float)dot[0].X,(float)dot[0].Y,(float)dot[1].X,(float)dot[1].Y);
  }
  
  
}
