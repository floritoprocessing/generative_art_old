class StickGroup extends Vector {
  
  StickGroup() {
    super();
  }
  
  void attachSticks(int child, int parent, int dot) {
    ((Stick)elementAt(child)).position = ((Stick)elementAt(parent)).dot[dot];
  }
  
  void rotateSticks() {
    for (int i=0;i<size();i++)
      ((Stick)elementAt(i)).rotate();
  }
  
  void drawOnImage(GrafImage img) {
    for (int i=0;i<size();i++)
      ((Stick)elementAt(i)).drawOnImage(img);
  }
  
  void showSticks() {
    for (int i=0;i<size();i++)
      ((Stick)elementAt(i)).show();
  }
}
