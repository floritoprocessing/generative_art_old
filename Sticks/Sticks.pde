import Graf.*;
import java.util.Vector;

Bmp bmp;
StickGroup group = new StickGroup();
GrafImage drawImage;
int drawCount;
int savedFrame=0;

float pmRandom(float a, float b) {
  return ((random(1.0)<0.5)?-1:1)*random(a,b);
}

void setup() {
  size(500,500,P3D);
  group.clear();
  for (int i=0;i<5;i++)
    group.add(new Stick(new Vec(width*random(0.4,0.6),height*random(0.4,0.6)),random(100,200),random(1.0),random(TWO_PI),pmRandom(0.01,0.05)));
    
  group.attachSticks(1,0,0);
  group.attachSticks(2,1,0);
  group.attachSticks(3,0,1);
  group.attachSticks(4,1,1);
  
  drawImage = new GrafImage(width,height);
  drawImage.setBlendMode(2);//GrafImage.BLEND_MODE_ADD);
  drawImage.loadPixels();
  for (int i=0;i<drawImage.pixels.length;i++) drawImage.pixels[i] = 0xFF000000;
  drawImage.updatePixels();
  drawCount = 0;
}

void mousePressed() {
  println(drawCount);
  bmp=new Bmp(width,height,24);
  bmp.capture24Bit();
  bmp.saveAs("Sticks_"+nf(savedFrame,3)+".bmp");
  savedFrame++;
  setup();
}

void draw() {
  image(drawImage,0,0);
  for (int i=0;i<100;i++) {
  group.rotateSticks();
  ((Stick)group.elementAt(2)).lineOnImage(drawImage);
  ((Stick)group.elementAt(4)).lineOnImage(drawImage);
  //group.showSticks();
  drawCount++;
  }
}
