// Wallpaper

float RENDER_DRAW_INTENSITY=0.012;
int COLOR_MODE = GraphicUtil.COLOR_MODE_NORMAL;

float RENDER_BLUR_INTENSITY=0.05;
float RENDER_CLEAR_INTENSITY=0.01;
float RENDER_COLOR_MORPH_FAC=0.01;

float RENDER_MORPH_FAC_MIN=0.0015;
float RENDER_MORPH_FAC_MAX=0.0080;

float RENDER_CHANGE_MORPH_FAC_CHANCE = 0.02;

boolean SAVE_IMAGES = true;



int CYCLES_PER_FRAME = 50000;


double A=0, B=0, C=0;
double AMorph=0, BMorph=0, CMorph=0;
double x = 0.0, y = 0.0;
double xn = 0.0, yn = 0.0;
double lastStartX = 0.0, lastStartY = 0.0;

long cyclesDrawn = 0;

int MODE_TRY = 0;
int MODE_RENDER = 1;
int MODE = MODE_TRY;

int col;
GraphicUtil gu;
int renderFrame=0;
long totalCycles=0;

void setup() {
  size(640,480,P3D);
  background(0,0,0);
  col=(int)random(0x202020,0xFFFFFF);
  mousePressed();
  gu = new GraphicUtil();
  gu.setColorMode(COLOR_MODE);  
}

void initXYAndScreenABit(double _x, double _y, float p) {
  x = _x;
  y = _y;
  lastStartX = x;
  lastStartY = y;
  //background(0,0,0);
  for (int sx=0;sx<width;sx++) for (int sy=0;sy<height;sy++) {
    gu.softSet(sx,sy,0x000000,p);
  }
  println("A: "+A);
  println("B: "+B);
  println("C: "+C);
}

void initXYAndScreen(double _x, double _y) {
  x = _x;
  y = _y;
  lastStartX = x;
  lastStartY = y;
  background(0,0,0);
  println("A: "+A);
  println("B: "+B);
  println("C: "+C);
}

void mousePressed() {
  if (MODE==MODE_TRY) {
    if (mouseButton==LEFT) {
      A = sq(random(1.0))*random(-150,150);
      B = sq(random(1.0))*random(-150,150);
      C = sq(random(1.0))*random(-150,150);
    }
    cyclesDrawn = 0;
    initXYAndScreen(mouseX-width/2.0,mouseY-height/2.0);  
  }
}

void morphColor(float p) {
  col = gu.morphColor(col,p);
  col = gu.restrainColor(col,0x404040,0xFFFFFF);
  println("color: "+(col>>16&0xFF)+"|"+(col>>8&0xFF)+"|"+(col&0xFF));
}

void makeABCMorphFac() {
  float r = RENDER_MORPH_FAC_MAX-RENDER_MORPH_FAC_MIN;
  AMorph = A * (RENDER_MORPH_FAC_MIN+r*Math.random()) * sign(random(-1.0,1.0));
  BMorph = B * (RENDER_MORPH_FAC_MIN+r*Math.random()) * sign(random(-1.0,1.0));
  CMorph = C * (RENDER_MORPH_FAC_MIN+r*Math.random()) * sign(random(-1.0,1.0));
  println("New Morph Factors: ");
  println("AMorph: "+AMorph);
  println("BMorph: "+BMorph);
  println("CMorph: "+CMorph);
}

void keyPressed() {
  if (MODE==MODE_TRY) {
    if (key=='2') {
      MODE=MODE_RENDER;
      renderFrame=0;
      makeABCMorphFac();
      initXYAndScreen(lastStartX,lastStartY);
    }
    if (key=='c') {
      // COLOR_MORPH:
      morphColor(0.05);
    } 
    else if (key=='r') {
      // RESTART:
      cyclesDrawn = 0;
      initXYAndScreen(lastStartX,lastStartY);
    }
  } 
  else if (MODE==MODE_RENDER) {
    if (key=='1') {
      MODE = MODE_TRY;
      cyclesDrawn = 0;
      initXYAndScreen(lastStartX,lastStartY);
    }
  }
}

void renderCycle(long n,float p) {
  for (long i=0;i<n;i++) {
    xn = x;
    yn = y;
    x = yn - sign(xn)*Math.pow(Math.abs(B*xn-C),0.5);
    y = A - xn;
    gu.floatSet((float)(x+width/2.0),(float)(y+height/2.0),col,p);
  }
}

double sign(double d) {
  return (d<0?-1:1);
}



void draw() {

  if (MODE==MODE_TRY) {
    renderCycle(CYCLES_PER_FRAME,0.03);
    cyclesDrawn += CYCLES_PER_FRAME;
  } 

  else if (MODE==MODE_RENDER) {

    if (totalCycles==0) {
      println("new frame: "+nf(renderFrame,6)+"( "+cyclesDrawn+" cycles per image @ "+CYCLES_PER_FRAME+" cycles per frame)");
      if (random(1.0)<RENDER_CHANGE_MORPH_FAC_CHANCE) makeABCMorphFac();
      initXYAndScreenABit(lastStartX,lastStartY,RENDER_CLEAR_INTENSITY);
    }

    renderCycle(CYCLES_PER_FRAME,RENDER_DRAW_INTENSITY);
    totalCycles+=CYCLES_PER_FRAME;
    
    if (totalCycles>=cyclesDrawn) {
      totalCycles=0;
      gu.blurScreen(RENDER_BLUR_INTENSITY);

      A += AMorph;
      B += BMorph;
      C += CMorph;
      morphColor(RENDER_COLOR_MORPH_FAC);

      if (SAVE_IMAGES) {
        println("...saving frame");
        println();
        saveFrame("_Wallpaper-"+nf(renderFrame,6)+".tga");
        renderFrame++;
      }
    }



  }
}
