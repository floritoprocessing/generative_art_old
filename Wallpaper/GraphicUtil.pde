
class GraphicUtil {

  public static final int COLOR_MODE_NORMAL = 0;
  public static final int COLOR_MODE_ADD = 1;
  private int COLOR_MODE = COLOR_MODE_NORMAL;

  GraphicUtil() {
  }
  
  public void setColorMode(int m) {
    COLOR_MODE = m;
  }

  public int colorMix(int c0, int c1, float p0) {
    float p1 = 1.0 - p0;
    int r0 = c0>>16&0xFF;
    int g0 = c0>>8&0xFF;
    int b0 = c0&0xFF;
    int r1 = c1>>16&0xFF;
    int g1 = c1>>8&0xFF;
    int b1 = c1&0xFF;
    int r=0, g=0, b=0;
    if (COLOR_MODE==COLOR_MODE_NORMAL) {
      r = int(r0*p0+r1*p1);  
      g = int(g0*p0+g1*p1);
      b = int(b0*p0+b1*p1);
    } else if (COLOR_MODE==COLOR_MODE_ADD) {
      r = r1 + int(r0*p0);  
      if (r>255) r=255;
      if (r<0) r=0;
      g = g1 + int(g0*p0);
      if (g>255) g=255;
      if (g<0) g=0;
      b = b1 + int(b0*p0);
      if (b>255) b=255;
      if (b<0) b=0;
    }
    
    return (r<<16|g<<8|b);
  }

  public void softSet(int x, int y, int c, float p) {
    set(x,y,colorMix(c,get(x,y),p));  
  }
  
  public void floatSet(float x, float y, int c, float p) {
    int x0 = int(floor(x)), x1 = x0 + 1;
    int y0 = int(floor(y)), y1 = y0 + 1;
    float px1 = x - x0, px0 = 1.0 - px1;
    float py1 = y - y0, py0 = 1.0 - py1;
    float p00 = p * px0 * py0, p10 = p * px1* py0;
    float p01 = p * px0 * py1, p11 = p * px1* py1;
    softSet(x0,y0,c,p00);
    softSet(x1,y0,c,p10);
    softSet(x0,y1,c,p01);
    softSet(x1,y1,c,p11);
  }
  
  public int morphColor(int c, float p) {
    int r = c>>16&0xFF;
    r += int(255*random(-p,p));
    if (r<0) r=0;
    if (r>255) r=255;
    
    int g = c>>8&0xFF;
    g += int(255*random(-p,p));
    if (g<0) g=0;
    if (g>255) g=255;
    
    int b = c&0xFF;
    b += int(255*random(-p,p));
    if (b<0) b=0;
    if (b>255) b=255;
    
    return (r<<16|g<<8|b);
  }
  
  public int restrainColor(int c, int c0, int c1) {
    int r = c>>16&0xFF;
    int g = c>>8&0xFF;
    int b = c&0xFF;
    int r0 = c0>>16&0xFF;
    int g0 = c0>>8&0xFF;
    int b0 = c0&0xFF;
    int r1 = c1>>16&0xFF;
    int g1 = c1>>8&0xFF;
    int b1 = c1&0xFF;
    if (r<r0) r=r0; else if (r>r1) r=r1;
    if (g<g0) g=b0; else if (g>g1) g=g1;
    if (b<b0) g=b0; else if (b>b1) b=b1;
    return (r<<16|g<<8|b);
  }
  
  public void blurScreen(float intensity) {
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      float r=0,g=0,b=0;
      for (int xo=-1;xo<=1;xo++) for (int yo=-1;yo<=1;yo++) {
        int sx=x+xo, sy=y+yo;
        if (sx<0) sx=0; else if (sx>=width) sx=width-1;
        if (sy<0) sy=0; else if (sy>=height) sy=height-1;
        int c=get(sx,sy);
        r += (c>>16&0xFF);
        g += (c>>8&0xFF);
        b += c&0xFF;
      }
      r/=9.0;
      g/=9.0;
      b/=9.0;
      int blurCol = int(r)<<16|int(g)<<8|int(b);
      softSet(x,y,blurCol,intensity);
    }
  }

}




