class QImage {
  
  public int width, height;
  private QColor[][] img;
  private PImage pimage;
  
  QImage(int w, int h) {
    width = w;
    height = h;
    img = new QColor[w][h];
    pimage = new PImage(w,h);
    for (int x=0;x<w;x++) for (int y=0;y<h;y++) {
      img[x][y] = new QColor(0,0,0);
    }
  }
  
  public void set(int x, int y, QColor c) {
    img[x][y].set(c);
  }
  
  public QColor get(int x, int y) {
    return img[x][y];
  }
  
  public void updatePImage() {
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      pimage.set(x,y,img[x][y].asColor());
    }
  }
  
  public PImage asPImage() {
    return pimage;
  }
  
}
