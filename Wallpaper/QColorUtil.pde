class QColorUtil {
  
  //public static final int BLEND_MODE_NORMAL = 0;
  //private int BLEND_MODE = BLEND_MODE_NORMAL;
  
  //public void setBlendMode(int m) {
  //  BLEND_MODE = m;
  //}
  
  QColorUtil() {
  }
  
  public QColor colorMix(QColor c0, QColor c1, double p0) {
    double p1 = 1.0 - p0;
    int r = (int)(c0.red()*p0+c1.red()*p1);  
    int g = (int)(c0.green()*p0+c1.green()*p1);
    int b = (int)(c0.blue()*p0+c1.blue()*p1);
    return new QColor(r,g,b);
  }
  
  public void softSet(QImage img, int x, int y, QColor c, float p) {
    QColor outColor = colorMix(c,img.get(x,y),p);
    img.set(x,y,outColor);
  }
  
  public void floatSet(QImage img, float x, float y, QColor c, float p) {
    int x0 = int(floor(x)), x1 = x0 + 1;
    int y0 = int(floor(y)), y1 = y0 + 1;
    float px1 = x - x0, px0 = 1.0 - px1;
    float py1 = y - y0, py0 = 1.0 - py1;
    float p00 = p * px0 * py0, p10 = p * px1* py0;
    float p01 = p * px0 * py1, p11 = p * px1* py1;
    softSet(img,x0,y0,c,p00);
    softSet(img,x1,y0,c,p10);
    softSet(img,x0,y1,c,p01);
    softSet(img,x1,y1,c,p11);
  }
  
}
