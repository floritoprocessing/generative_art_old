class QColor  {
  
  private int r, g, b;
  
  QColor(int _r, int _g, int _b) {
    r = _r;
    g = _g;
    b = _b;
  }
  
  public void set(QColor c) {
    r = c.red();
    g = c.green();
    b = c.blue();
  }
  
  public void set(int _r, int _g, int _b) {
    r = _r;
    g = _g;
    b = _b;
  }
  
  public int red() {
    return r;
  }
  public int green() {
    return g;
  }
  public int blue() {
    return b;
  }
  
  public color asColor() {
    return color(r>>16&0xFF,g>>16&0xFF,b>>16&0xFF);
  }
  
}
