int nrOfAttractors; 
Attractor[] att;
int nrOfDotties; 
Dotty[] dot;
color[] drawColor = {#FF1A06, #FC1800, #FF3602, #FF3C00, #FE5802, #FF7003, #FD6B04, #EF8201, #D17C0F, #97550B, #563610, #463F11, #1F371D, #929705, #878A00, #72700C, #696118, #697B29, #518322, #266F26};
int nrOfColors=drawColor.length;
float speedBase, speedRange;

void setup() {
  size(600, 300);//,P3D); 
  go();
}

void go() {
  int limx=600, limy=300;
  ellipseMode(CENTER);//CENTER_DIAMETER);
  nrOfAttractors=6; 
  att=new Attractor[nrOfAttractors]; 
  createAttractors();
  nrOfDotties=1500; 
  dot=new Dotty[nrOfDotties]; 
  createDotties();
  background(26, 84, 85);

  speedRange=random(10);
  speedBase=random(1)*speedRange;
}

void keyPressed() {
  go();
}

void draw() {
  for (int i=0; i<100; i++) {
    updateAttractors();
    updateDotties();
  }
}



void createDotties() {
  for (int i=0; i<nrOfDotties; i++) {
    dot[i]=new Dotty();
  }
}

void updateDotties() {
  for (int i=0; i<nrOfDotties; i++) {
    dot[i].update();
  }
}

class Dotty {
  Vec pos, mov;
  float ang, spd, behave;
  color col;
  Dotty() {
    int i=(int)random(nrOfAttractors);
    pos=new Vec(att[i].pos.x+random(-4, 4), att[i].pos.y+random(-4, 4));    // new position
    ang=random(-PI, PI); 
    spd=random(speedBase, speedRange);        // new angle
    mov=new Vec(spd*cos(ang), spd*sin(ang));
    int pick=(int)random(nrOfColors);             // new color
    col=drawColor[pick];              
    behave=pick/(float)nrOfColors;  // 0..1
  }

  void update() {
    for (int i=0; i<nrOfAttractors; i++) {
      float cd=diffColor(col, att[i].col);
      Vec diff=VecSub(att[i].pos, pos);
      mov=VecAdd(mov, VecMul(cd*0.001, diff));
    }
    mov=VecRot(behave*ang*0.01, mov);
    mov=VecMul(0.999, mov);
    pos=VecAdd(pos, mov);
    int x=int(pos.x), y=int(pos.y);
    if (x>=1&&x<=width&&y>1&&y<=height) {
      set(x, y, fadeColor(col, get(x, y), 0.05));
    }
  }
}



void createAttractors() {
  for (int i=0; i<nrOfAttractors; i++) {
    att[i]=new Attractor();
  }
}

void updateAttractors() {
  for (int i=0; i<nrOfAttractors; i++) {
    att[i].update();
  }
}

class Attractor {
  Vec tpos, pos, mov;
  color col;
  boolean jumped;

  Attractor() {
    init();
    int pick=(int)random(nrOfColors);
    col=drawColor[pick];
    jumped=((random(1)<0.5)?true:false);
  }

  void update() {
    if (!jumped) {
      if (random(1)<0.0001) {
        init();
        jumped=true;
        createDotties();
      }
    }
    mov=VecMul(0.999, mov);
    tpos=VecAdd(pos, mov);
    if (tpos.x>width||tpos.x<1) {
      mov=new Vec(-mov.x, mov.y);
    }
    if (tpos.y>height||tpos.y<1) {
      mov=new Vec(mov.x, -mov.y);
    }
    pos=VecAdd(pos, mov);
    //noStroke(); fill(col);
    //ellipse(pos.x,pos.y,10,10);
  }

  void init() {
    pos=new Vec(random(width), random(height));
    mov=new Vec(random(-1, 1), random(-1, 1));
    mov=VecMul(0.5, mov);
  }
}





color fadeColor(color a, color b, float percA) {
  float percB=1-percA;
  int rr=int(percA*red(a)+percB*red(b));
  int gg=int(percA*green(a)+percB*green(b));
  int bb=int(percA*blue(a)+percB*blue(b));
  return color(rr, gg, bb);
}

float diffColor(color a, color b) {
  float dr=abs(red(a)-red(b))/(3*255.0);
  float dg=abs(green(a)-green(b))/(3*255.0);
  float db=abs(blue(a)-blue(b))/(3*255.0);
  return (dr+dg+db);
}



// VECTOR CLASS AND MATHEMATICS --------------------------------
// -------------------------------------------------------------
class Vec {
  float x, y;
  Vec(float x, float y) {
    this.x=x; 
    this.y=y;
  }
}

Vec VecAdd(Vec a, Vec b) {
  Vec z=new Vec(0, 0);
  z.x=a.x+b.x; 
  z.y=a.y+b.y;
  return z;
}

Vec VecSub(Vec a, Vec b) {
  Vec z=new Vec(0, 0);
  z.x=a.x-b.x; 
  z.y=a.y-b.y;
  return z;
}

Vec VecMul(float a, Vec b) {
  Vec z=new Vec(0, 0);
  z.x=a*b.x; 
  z.y=a*b.y;
  return z;
}


Vec VecRot(float arc, Vec a) {
  Vec z=new Vec(0, 0);
  float Si=sin(arc), Co=cos(arc); 
  z.x=a.x*Co-a.y*Si; 
  z.y=a.y*Co+a.x*Si;
  return z;
}  


float VecLen(Vec a) {
  float z=sqrt(a.x*a.x+a.y*a.y);
  return z;
}

Vec VecInScreen(Vec a) {
  Vec z=a;
  while (z.x>width) {
    z.x-=width;
  } 
  while (z.x<1) {
    z.x+=width;
  }
  while (z.y>height) {
    z.y-=height;
  } 
  while (z.y<1) {
    z.y+=height;
  }
  return z;
}
// -------------------------------------------------------------
// (end vector class and mathematics) --------------------------
