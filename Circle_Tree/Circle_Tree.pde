//int gW=1600, gH=400;

int nrOfCircles=50;
float gRadLo=0.01, gRadHi=0.03;
int gColRLo=0, gColRHi=128;
int gColGLo=20, gColGHi=100;
int gColBLo=0, gColBHi=25;
float gMov=0.001; 
float gRadMorph=0.0005;
float gRotMorph=0.08;
float gAlpMin=2, gAlpMax=15;
float gColMorphRange=5.0;

float mRotY=0;//=HALF_PI;
float mRotYInc;
float mRotYIncMax=0.002;
float mRotYIncMin=0.0003;


boolean saved=false;


void setup() {
  size(1600,400,P3D);
  background(255,255,255);
  createCircles();
  mRotY = HALF_PI;
}

void draw() {
  if (mRotY>0) {
    colorMode(RGB,255.0);
//    mRotY:     HALF_PI     -> 0.0
//    mRotYInc:  mRotYIncMax -> mRotYIncMin

    float p=1.0-mRotY/HALF_PI;
    mRotYInc=mRotYIncMax+p*(mRotYIncMin-mRotYIncMax);
    
    translate(-width/2.0,0);
    rotateY(mRotY-=mRotYInc);
    translate(width/2.0,0);

    //scale(0.5);
    updateCircles();
//    lightenScreen(50,0,0);
//    Pointillize(5);
    
    if (frameCount%100==0) {println("deg: "+degrees(mRotY)); println("inc: "+degrees(mRotYInc)); }
  } else {
    if (!saved) {
      //Curves();
      saved=true;
      //save("CircleTree"+random(9999)+".tga");
      println("DONE!");
    }
  }
}

void keyPressed() {
  Curves();
}


Circle[] circle;

void createCircles() {
  circle=new Circle[nrOfCircles];
  for (int i=0;i<nrOfCircles;i++) {
    circle[i]=new Circle();
  }
}

void updateCircles() {
  for (int i=0;i<nrOfCircles;i++) {
    circle[i].update();
  }
}

boolean allCirclesDead() {
  int n=0;
  for (int i=0;i<nrOfCircles;i++) { if (circle[i].dead) {n++;} }
  if (frameCount%25==0) {println("Dead circles: "+n+" (of "+nrOfCircles+")");}
  return (n==nrOfCircles);
}

class Circle {
  float x=0, y=0, xMax=0, yMax=0;
  float sScale=0;
  float radius=0;
  float xRot=0, yRot=0;
  color col;
  float alp=0;
  boolean dead=false;
  
  Circle() {
    sScale=width>height?width:height;
    if (width>height) { xMax=1.0; yMax=height/(float)width; } else { yMax=1.0; xMax=width/(float)height; }
    randomize();
  }
  
  void randomize() {
    x=random(xMax); y=random(yMax);
    radius=random(gRadLo,gRadHi);
    //xRot=random(TWO_PI); yRot=random(TWO_PI);
    col=color(random(gColRLo,gColRHi),random(gColGLo,gColGHi),random(gColBLo,gColBHi));
  }
  
  void update() {
    if (!dead) {
      // die:
//      if (random(1)<0.002) {dead=true;}
      
      // randomize all:
      if (random(1)<0.01) {randomize();}
      
      // morph position:
      x+=random(-1,1)*gMov; y+=random(-1,1)*gMov;
      
      // morph radius:
      float newRadius=-1;
      while (newRadius<gRadLo||newRadius>2*gRadHi) {
        newRadius=radius+gRadMorph*random(-1,1);
      }
      radius=newRadius;
      
      // morph rotation:
      xRot+=random(-gRotMorph,gRotMorph); yRot+=random(-gRotMorph,gRotMorph);
      
      // morph alpha:
      alp=random(gAlpMin,gAlpMax);
      
      // morph color:
      float r=-1, g=-1, b=-1;
      while (r<gColRLo||r>gColRHi) {r=red(col)+random(-gColMorphRange,gColMorphRange);}
      while (g<gColGLo||g>gColGHi) {g=green(col)+random(-gColMorphRange,gColMorphRange);}
      while (b<gColBLo||b>gColBHi) {b=blue(col)+random(-gColMorphRange,gColMorphRange);}
      col=color(r,g,b);
      
      // draw ellipse
      
      ellipseMode(CENTER);
      colorMode(RGB,255.0);
      noFill(); 
      stroke(red(col),green(col),blue(col),alp * ( 0.05 + 0.95* x/xMax * (1-mRotY/HALF_PI)));
      float sr=radius*sScale;
      pushMatrix();
      scale(0.8,0.8,0.8);
      translate(width*0.1,height*0.1);
      translate(x*sScale,y*sScale);
      rotateX(xRot); rotateY(yRot);
      ellipse(0,0,sr,sr);
      popMatrix();
    }
  }
}





void lightenScreen(float _ease1, float _ease2, float _ease3) {
  imageMode(CORNERS);
  colorMode(RGB,255.0);
  float easeTotal=_ease1+_ease2+_ease3;
  loadPixels();
  for (int i=0;i<pixels.length;i++) {
    float R=constrain((_ease1*red(pixels[i])+_ease2*green(pixels[i])+255.0*_ease3)/easeTotal,0,255);
    float G=constrain((_ease1*green(pixels[i])+_ease2*blue(pixels[i])+255.0*_ease3)/easeTotal,0,255);
    float B=constrain((_ease1*blue(pixels[i])+_ease2*red(pixels[i])+255.0*_ease3)/easeTotal,0,255);
    pixels[i]=color(R,G,B);
  }
  updatePixels();
}

void Pointillize(float _alpha) {
  colorMode(RGB,255.0);
  loadPixels();
  float r=random(dist(0,0,width,height)/20.0);
  float x=random(width), y=random(height);
  color c=get(int(x),int(y));
  noStroke(); fill(red(c),green(c),blue(c),_alpha); ellipse(x,y,r,r);
}

void Curves() {
  colorMode(HSB,1.0);
  loadPixels();
  for (int i=0;i<pixels.length;i++) {
    color c=pixels[i];
    float f1=0.5+0.5*sin(brightness(c)*PI+0.75*TWO_PI);
    float f2=brightness(c);
    float f=0.5*f1+0.5*f2;
    pixels[i]=color(hue(c),saturation(c),f);
  }
  updatePixels();
}
