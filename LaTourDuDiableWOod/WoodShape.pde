class WoodShape
{

  final int RED = color(255, 0, 0);
  final int GREEN = color(0, 255, 0);

  final float baseRadius;
  final float radiusDeviation;
  float maxCreatedRadius;

  boolean searching = true;
  float x, y;
  ArrayList<PVector> points;



  public WoodShape() {
    
    baseRadius = randomBaseRadius();
    radiusDeviation = random(SHAPE_RADIUS_MIN_DEVIATION, SHAPE_RADIUS_MAX_DEVIATION);
    create();
  }
  
  public WoodShape(WoodShape shape, float morphFactor) {

    float newBaseRadius = randomBaseRadius();
    float newRadiusDeviation = random(SHAPE_RADIUS_MIN_DEVIATION, SHAPE_RADIUS_MAX_DEVIATION);
    float shapeBaseRadius = shape.baseRadius;
    float shapeRadiusDeviation = shape.radiusDeviation;

    baseRadius = map(morphFactor, 0, 1, shapeBaseRadius, newBaseRadius);
    radiusDeviation = map(morphFactor, 0, 1, shapeRadiusDeviation, newRadiusDeviation);
    create();
  }

  float randomBaseRadius() {
    //float a0 = sq(SHAPE_MIN_RADIUS);
    //float a1 = sq(SHAPE_MAX_RADIUS);
    //float a = random(a0,a1);
    //float r = sqrt(a);
    //return r;
    return random(SHAPE_MIN_RADIUS, SHAPE_MAX_RADIUS);
  }


  void create() {

    // create simple outline

    points = new ArrayList<PVector>();


    maxCreatedRadius = 0;
    float radius0 = baseRadius*(1.0-radiusDeviation);
    float radius1 = baseRadius*(1.0+radiusDeviation);
    float radiusDev = radius1-radius0;

    float radius = random(radius0, radius1);
    for (float degree=0; degree<360; degree+=SHAPE_DEGREE_STEP) {

      float rd = radians(degree);

      maxCreatedRadius = max(maxCreatedRadius, radius);
      PVector point = new PVector(
        radius * cos(rd), radius * sin(rd));
      radius += 0.1*random(-radiusDev, radiusDev);
      radius = max(radius0, min(radius1, radius));

      points.add(point);
    }
  }




  boolean doesOverlap() {

    loadPixels();

    // check area around
    int x0 = (int)floor(x - maxCreatedRadius);
    int x1 = (int)ceil(x + maxCreatedRadius);
    int y0 = (int)floor(y - maxCreatedRadius);
    int y1 = (int)ceil(y + maxCreatedRadius);

    for (int y=y0; y<=y1; y++) {
      int sy = max(0, min(height-1, y));
      for (int x=x0; x<=x1; x++) {
        int sx = max(0, min(width-1, x));

        int c = get(sx, sy);
        if ((int)red(c)==255 && (int)green(c)==255) {
          return true;
        }
      }
    }

    return false;
  }




  public void draw(boolean searchMode) {

    if (searchMode) blendMode(ADD);

    int col = searching ? GREEN : RED;
    if (!searchMode) {
      col = color(#CB9640);
    }

    fill(col);
    if (searchMode) {
      noStroke();
    } else {
      stroke(0);
    }
    //stroke(col);

    pushMatrix();
    translate(x, y);
    beginShape(POLYGON);
    for (PVector point : points) {
      vertex(point.x, point.y);
    }
    endShape(CLOSE);
    popMatrix();
  }
}



void createRandom() {
  WoodShape shape = new WoodShape();
  shape.x = random(width);
  shape.y = random(height);
  shapes.add(shape);
}

void createMorph(WoodShape shape) {
  WoodShape s = new WoodShape(shape, random(SHAPE_MAX_MORPH_FACTOR));
  s.x = random(width);
  s.y = random(height);
  shapes.add(s);
}