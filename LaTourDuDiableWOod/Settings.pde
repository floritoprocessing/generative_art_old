final int BACKGROUND_TEST_OVERLAP = color(0);

final float SHAPE_DEGREE_STEP = 5;
final float SHAPE_MIN_RADIUS = 1;
final float SHAPE_MAX_RADIUS = 12;  
final float SHAPE_RADIUS_MIN_DEVIATION = 0.25;
final float SHAPE_RADIUS_MAX_DEVIATION = 0.55;

final float SHAPE_MAX_MORPH_FACTOR = 0.2;

final float NEW_SHAPE_RANDOM_POS_CHANCE = 0.15;