
ArrayList<WoodShape> shapes;
WoodShape searchShape;

int morphBaseIndex = 0;

void setup() {
  size(800, 450);

  noSmooth();

  shapes = new ArrayList<WoodShape>();

  // put first shape:
  morphBaseIndex = 0;
  createRandom();
  shapes.get(0).y = height - shapes.get(0).maxCreatedRadius;
  shapes.get(0).searching = false;

  // new random shape
  newSearchShape();
}

void newSearchShape() {
  
  searchIterations = 0;
  searchRd = random(TWO_PI);
  //TODO: to morph or not to morph..

  // NEW RANDOM:
  float chanceForRandom = NEW_SHAPE_RANDOM_POS_CHANCE;
  boolean ok = false;
  WoodShape rnd = null;
  if (random(1.0)<chanceForRandom) { //random(1.0)<chanceForRandom
    print("trying for random...");
    int tries = 0;
    do {
      createRandom();
      rnd = shapes.get(shapes.size()-1);
      rnd.y = rnd.maxCreatedRadius;
      ok = !rnd.doesOverlap();
      tries++;
      if (!ok) shapes.remove(shapes.size()-1);
      else {
        rnd.searching = false;
      }
    } while (!ok && tries<30);
    
    if (ok) {
      println("FOUND in "+tries);
    } else {
      println("nope.");
    }
  }

  if (ok) {
    
    do {
      //println("aaa.."+rnd.y);
      rnd.y += 1;
      rnd.draw(true);
    } while (!rnd.doesOverlap()&&rnd.y<height-rnd.maxCreatedRadius);
    rnd.y -= 1;
    println(rnd.x+" / "+rnd.y);
    
    morphBaseIndex = shapes.size()-1;
  }
  // MORPH:
  WoodShape morphBase = shapes.get(morphBaseIndex);
  createMorph( morphBase );
  searchShape = shapes.get(shapes.size()-1);
  searchShape.x = morphBase.x;
  searchShape.y = morphBase.y;
}


int searchIterations = 0;
float searchRd;

void draw() {
  
  
  //int ti = millis();
  //int endTi = ti + 20;
  //for (int f=0; f<50; f++) {
  int f=0, ti=0;
  for (ti=millis()+20; millis()<ti; f++) {
    background(BACKGROUND_TEST_OVERLAP);

    for (WoodShape shape : shapes) {
      shape.draw(true);
    }

    if (searchShape!=null) {

      boolean overlap = searchShape.doesOverlap();
      if (overlap) {
        // DRUNK:
        //float rd = TWO_PI * noise(
        //  searchShape.x/width*0.1, searchShape.y/height*0.1, millis()*0.0001);//random(TWO_PI);
        //float rd1 = random(TWO_PI);
        //rd = 0.15*rd + 0.85*rd1;
        
        searchRd += radians(random(-15,15));
        
        float x = 1*cos(searchRd);
        float y = 1*sin(searchRd);
        searchShape.x += x;
        searchShape.y += y;
        if (searchShape.x<0 || searchShape.x>width || searchShape.y<0 || searchShape.y>height) {
          searchShape.x -= x;
          searchShape.y -= y;
        }
        searchIterations++;
        if (searchIterations>1000) {
          //println("Can't find, retry");
          shapes.remove(searchShape);
          newSearchShape();
        }
        
      } else {
        //println("Found a spot in "+searchIterations);
        searchShape.searching = false;
        newSearchShape();
      }
    }
    //createMorph( shapes.get(morphBaseIndex) );
  }
  //println(f+" iterations per frame");
  
  
  if (!showBackend) {
    background(#EDE1CB);
    blendMode(NORMAL);
    for (WoodShape shape : shapes) {
      shape.draw(false);
    }
  }
  
  if (saveIt) {
    saveIt = false;
    String name = "LaTour_"+nf(year(),4)+nf(month()+1,2)+nf(day(),2)+"_"+nf(hour(),2)+nf(minute(),2)+nf(second(),2)+".bmp";
    println("Saved as "+name);
    save(name);
  }
}

boolean showBackend = false;
boolean saveIt = false;
void keyPressed() {
  if (key=='S') saveIt = true;
  if (key=='b') showBackend = !showBackend;
}