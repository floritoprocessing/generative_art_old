int maxpix;
ActiveScreen screen;

void setup() {
  size(400,300); maxpix=width*height;
  screen=new ActiveScreen(width/2,height/2);
  screen.clear();
}

void draw() {
  background(0);
  screen.newDots(maxpix/100,128,255);
  screen.fade();
  
  screen.toMonitor(maxpix);
}

class ActiveScreen {
  int w,h,len;
  float maxWH, mx, my;
  int[] pix;
  ActiveScreen(int w, int h) {
    this.w=w; this.h=h; len=w*h;
    maxWH=sqrt(sq(w)+sq(h)); mx=w/2.0; my=h/2.0;
    pix=new int[len];
  }
  
  void clear() {
    for (int i=0;i<len;i++) {pix[i]=0;}
  }
  
  void newDots(int n, int c1, int c2) {
    for (int i=0;i<n;i++) {
      int c=(int)random(c1,c2);
      pix[(int)random(len)]=c<<16|c<<8|c;
    }
  }
  
  void fade() {
    mx+=(mouseX-mx)*0.15; my+=(mouseY-my)*0.15;
    float mx2=width-mx, my2=height-my;
    float SI=0.005+0.005*sin(0.1*millis()/1000.0*TWO_PI);
    for (int i=0;i<len;i++) {
      int y=i/w; int x=i-(y*w);
      float d=SI+dist(x,y,mx,my)/maxWH+dist(x,y,mx2,my2)/maxWH;
      int rr=int((pix[i]>>16&255)/d);
      int gg=int((pix[i]>>8&255)/d);
      int bb=int((pix[i]&255)/d);
      pix[i]=255<<24|rr<<16|gg<<8|bb;
    }
  }
  
  void toMonitor(int top) {
    loadPixels();
    for (int i=0;i<top;i++) {
      pixels[i]=screen.pix[i%len];
    }
    updatePixels();
  }
}
