Sketcherr[] sk; int nrOfSk=100000, actualSk;
int drawColor=0;
int drawPhase;

// variables used for calculating average center
float br, avx, avy, div, tot_avx, tot_avy;
int tot_x=0,tot_y=0;

void setup() {
  size(800,600);
  background(255); //framerate(50);
  sk=new Sketcherr[nrOfSk];
  initSketchers();
  drawPhase=1;
}

void initSketchers() {
  for (int i=0;i<nrOfSk;i++) { sk[i]=new Sketcherr(width,height); sk[i].init(); }
  actualSk=0;
}

void draw() {
  // DRAW SKETCHERS -------------------------------------------------------
  for (int ii=0;ii<50;ii++) {  // (50x drawing per frame)
  
  
  if (actualSk<nrOfSk-1) {
    
    // change drawcolor:
    if (random(1)<0.0004) {
      drawColor=0xffffff-drawColor;
      if (brightness(drawColor)<80) {
        int rr=(drawColor>>16&255)+(int)random(1); if (rr<255) {rr=255;}
        int gg=(drawColor>>8&255)+(int)random(1); if (gg>255) {gg=255;}
        int bb=(drawColor&255)+(int)random(1); if (bb>255) {bb=255;}
        drawColor=(rr<<16|gg<<8|bb);
      }
      println(actualSk+"/"+nrOfSk+" color: "+drawColor);
    }

    // new sketcher once in so many time
    if (random(1)<0.08) {
      actualSk++; 
      float rand=random(1);
      if (rand<0.001) {        // 0.1%  // JUMP TO NEW POSITION
        sk[actualSk].init();
      } else if (rand<0.01) { // 0.9%   // JUMP TO NEW POSITION RELATED TO ANY PREVIOUS SKETCHER
        sk[actualSk].morph(sk[(int)random(actualSk)]);
      } else {                // 99%    // JUMP TO NEW POSITION RELATED TO LAST SKETCHER
        sk[actualSk].morph(sk[actualSk-1]);
      }        
    }
    
    // draw actual sketcher
    sk[actualSk].makeLine(drawColor);
  }
  
  }
}


class Sketcherr {
  int w,h;    //max width/height;
  Vec cen=new Vec(), rad=new Vec();
  float rd1,rd2,rdr;
  
  Sketcherr(int limX, int limY) {
    w=limX; h=limY;
  }
  
  void init() {
     cen.init(random(w),random(h));
     rad.init(random(w/80.0,w/12.0),random(h/80.0,h/12.0));
     rd1=random(TWO_PI); rd2=rd1+random(PI/16.0,PI); rdr=abs(rd1-rd2);
  }
  
  void morph(Sketcherr isk) {
    float rand=random(1);
    float cxoff=0,cyoff=0,rxoff=0,ryoff=0,rd1off=0,rd2off=0;

    if (rand<.2) {
      cxoff=random(-5,5); cyoff=random(-5,5);
    } else if (rand<.6) {
      rxoff=random(-w/128.0,w/128.0); ryoff=random(-h/128.0,h/128.0);
    } else {
      rd1off=random(-PI/16.0,PI/16.0); rd2off=random(PI/16.0,PI);
    }
    
    cen.init(isk.cen.x+cxoff,isk.cen.y+cyoff);
    rad.init(isk.rad.x+rxoff,isk.rad.y+ryoff);
    rd1=isk.rd1+rd1off; rd2=rd1+rd2off; rdr=abs(rd1-rd2);
  }
  
  void makeLine(int c) {
    float trd1=rd1+random(-rdr/4.0,rdr/4.0);
    float trd2=rd2+random(-rdr/4.0,rdr/4.0);
    float tcx=cen.x+random(-w/150.0,w/150.0), tcy=cen.y+random(-h/150.0,h/150.0);
    for (float rd=trd1;rd<=trd2;rd+=TWO_PI/360.0) {
      float sx=tcx+rad.x*cos(rd), sy=tcy+rad.y*sin(rd);
      softset(int(sx),int(sy),c,0.01);
    }
  }
}


class Vec {
  float x,y;
  Vec() {}
  void init(float ix, float iy) {x=ix;y=iy;}
}

void softset(int x, int y, int c1, float p1) {
  float p2=1.0-p1;
  int c2=get(x,y);
  int r=int(p1*(c1>>16&255)+p2*(c2>>16&255));
  int g=int(p1*(c1>>8&255)+p2*(c2>>8&255));
  int b=int(p1*(c1&255)+p2*(c2&255));
  set(x,y,(255<<24|r<<16|g<<8|b));
}
