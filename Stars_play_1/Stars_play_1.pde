float[] x, y, z;
int[] col;
float sf=3.0;
int nrOfStars=10000;
float sideLen;
float mx, my, mz;
float rz;

void setup() {
  size(720,576,P3D);
  initArray();
  sideLen=pow(nrOfStars,1/3.0);
  mx=random(100); my=random(100); mz=random(100);
  rz=0;
}

void draw() {
  background(0);
  
  updateArray(mx,my,mz-=0.008);
  rz+=0.003;
  
  
  for (int i=0;i<nrOfStars;i++) {
    float tx=x[i]*sf, ty=y[i]*sf, tz=z[i]*sf;
    pushMatrix();
    translate(width/2.0+60,height/2.0);
    rotateZ(rz);
    stroke(255,0,0,col[i]);
    point(tx,ty,tz);
    popMatrix();
    pushMatrix();
    translate(width/2.0-60,height/2.0);
    rotateZ(rz);
    stroke(0,255,0,col[i]);
    point(tx,ty,tz);
    popMatrix();
  }
  
  //saveFrame("c:/temp4/Stars_play_1_####.tga");
}

void initArray() {
  x=new float[nrOfStars]; y=new float[nrOfStars]; z=new float[nrOfStars];
  col=new int[nrOfStars];
  for (int i=0;i<nrOfStars;i++) { x[i]=0;y[i]=0;z[i]=0; }
}

void updateArray(float xo, float yo, float zo) {
  float noiseFac=10;
  float xi,yi,zi;
  for (int i=0;i<nrOfStars;i++) {
    xi=random(1); yi=random(1); zi=random(1);
    if (noise((xi+xo)*noiseFac,(yi+yo)*noiseFac,(zi+zo)*noiseFac)<0.35) {
      x[i]=(xi-0.5)*1000; y[i]=(yi-0.5)*1000; 
      z[i]=-1000+(zi)*1000; col[i]=180+int(zi*32);
    }
  }
}
