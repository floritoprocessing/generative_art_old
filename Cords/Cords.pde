import java.util.Vector;

Vector nodes;
Springs springs;
Vec gravity = new Vec();

void setup() {
  size(640,480,P3D);
  nodes = new Vector();
  springs = new Springs();
  for (int i=0;i<50;i++) nodes.add(new Node(new Vec(random(width),random(height))));

  // make random connections:
  /*
  for (int i=0;i<200;i++) {
    boolean alreadyConnected = true;
    while (alreadyConnected) {
      int i1 = (int)random(nodes.size());
      int i2 = i1;
      while (i1==i2) i2 = (int)random(nodes.size());
      Node n1 = (Node)nodes.elementAt(i1);
      Node n2 = (Node)nodes.elementAt(i2);
      if (!springs.existsConnectionBetween(n1,n2)) {
        springs.add(new Spring(n1,n2));
        alreadyConnected=false;
      }
    }
  }
  */



  // connect all:
  for (int i=0;i<nodes.size()-1;i++) for (int j=i+1;j<nodes.size();j++) 
    springs.add(new Spring((Node)nodes.elementAt(i),((Node)nodes.elementAt(j))));

  //if (i<19) springs.add(new Spring((Node)nodes.elementAt(i),(Node)nodes.elementAt(i+1)));
  //else springs.add(new Spring((Node)nodes.elementAt(i),(Node)nodes.elementAt(0)));
  
  background(0);
}

void draw() {
  //background(0);
  if (random(1.0)<0.02) {
    gravity = new Vec(0,0.2);
    gravity.rotZ(random(TWO_PI));
  }
  for (int i=0;i<nodes.size();i++) ((Node)nodes.elementAt(i)).createNewPosition(gravity);

  if (mousePressed) {
    ((Node)nodes.elementAt(0)).newPOSITION.x = mouseX;
    ((Node)nodes.elementAt(0)).newPOSITION.y = mouseY;
  }
  for (int i=0;i<nodes.size();i++) ((Node)nodes.elementAt(i)).updateToNewPosition();
  springs.updateForces();
  //springs.draw();

  for (int i=0;i<nodes.size();i++) ((Node)nodes.elementAt(i)).draw();
}
