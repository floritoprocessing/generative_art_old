class Node {
  
  Vec POSITION, newPOSITION;
  Vec MOVEMENT;
  Vector ATTACHED_SPRINGS = new Vector();
  float WALL_DRAG = 0.9;
  
  Node(Vec p) {
    POSITION = new Vec(p);
    newPOSITION = new Vec(p);
    MOVEMENT = new Vec();
  }
  
  void createNewPosition(Vec gravity) {
    Vec ACC = new Vec();
    
    for (int i=0;i<ATTACHED_SPRINGS.size();i++) {
      Node otherNode = ((Spring)ATTACHED_SPRINGS.elementAt(i)).getOtherNode(this);
      Vec dir = vecSub(otherNode.POSITION,POSITION);
      dir.normalize();
      ACC.add(vecMul(dir,((Spring)ATTACHED_SPRINGS.elementAt(i)).FORCE));
    }
    
    ACC.add(gravity);
    MOVEMENT.add(ACC);
    MOVEMENT.mul(0.99);
    //if (MOVEMENT.len()>10.0) println(MOVEMENT.len());
    if (MOVEMENT.len()>10.0) MOVEMENT.setLen(10.0);
    newPOSITION.add(MOVEMENT);
    
    
    if (newPOSITION.x<0) {
      newPOSITION.x=-WALL_DRAG*newPOSITION.x;
      MOVEMENT.x*=-WALL_DRAG;
    }
    else if (newPOSITION.x>width) {
      newPOSITION.x=2*width-(((newPOSITION.x-width)*WALL_DRAG)+width);
      MOVEMENT.x*=-WALL_DRAG;
    }
    if (newPOSITION.y<0) {
      newPOSITION.y=-WALL_DRAG*newPOSITION.y;
      MOVEMENT.y*=-WALL_DRAG;
    }
    else if (newPOSITION.y>height) {
      newPOSITION.y=2*height-(((newPOSITION.y-height)*WALL_DRAG)+height);
      MOVEMENT.y*=-WALL_DRAG;
    }
    //MOVEMENT.mul(0.3);
    
    
  }
  
  void updateToNewPosition() {
    stroke(255,0,0,15);
    line((float)POSITION.x,(float)POSITION.y,(float)newPOSITION.x,(float)newPOSITION.y);
    POSITION.setVec(newPOSITION);
  }
  
  void draw() {
    stroke(255,255,255,3);
    noFill();
    ellipseMode(RADIUS);
    ellipse((float)POSITION.x,(float)POSITION.y,5,5);
  }
  
}
