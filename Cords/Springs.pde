class Springs extends Vector {
  
  Springs() {
    super();
  }
  
  Spring springAt(int i) {
    return ((Spring)elementAt(i));
  }
  
  void updateForces() {
    for (int i=0;i<size();i++) springAt(i).updateForce();
  }
  
  void draw() {
    for (int i=0;i<size();i++) springAt(i).draw();
  }
  
  boolean existsConnectionBetween(Node n1, Node n2) {
    for (int i=0;i<springs.size();i++) {
      if (springAt(i).consistsOfNodes(n1,n2)) return true;
    }
    return false;
  }
  
}
