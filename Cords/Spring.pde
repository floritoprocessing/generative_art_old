class Spring {
  
  float NATURAL_LENGTH = 20;
  float FORCE = 0;
  float CURRENT_LENGTH;
  
  Node[] ATTACHED_NODES = new Node[2];
  
  Spring(Node n1, Node n2) {
    attachTo(n1,n2);
    updateLength();
  }
  
  boolean consistsOfNodes(Node n1, Node n2) {
    if (ATTACHED_NODES[0].equals(n1)&&ATTACHED_NODES[1].equals(n2)) return true;
    if (ATTACHED_NODES[1].equals(n1)&&ATTACHED_NODES[0].equals(n2)) return true;
    return false;
  }
  
  void updateLength() {
    Vec p1 = ATTACHED_NODES[0].POSITION;
    Vec p2 = ATTACHED_NODES[1].POSITION;
    CURRENT_LENGTH = (float)(vecSub(p1,p2).len());
  }
  
  void updateForce() {
    
    updateLength();
    
    if (CURRENT_LENGTH>NATURAL_LENGTH) {
      // ATTRACTION
      //FORCE = C1 * log(NATURAL_LENGTH/C3);
      if (CURRENT_LENGTH>2*NATURAL_LENGTH) CURRENT_LENGTH=2*NATURAL_LENGTH;
      if (CURRENT_LENGTH<0.01*NATURAL_LENGTH) CURRENT_LENGTH=0.01*NATURAL_LENGTH;
      FORCE = 0.005*(CURRENT_LENGTH/NATURAL_LENGTH);
      
    }
    
    else if (CURRENT_LENGTH<NATURAL_LENGTH) {
      // REPULSION
      //FORCE = C3 / sqrt(CURRENT_LENGTH);
      //FORCE = -sq(NATURAL_LENGTH/CURRENT_LENGTH);
      if (NATURAL_LENGTH/CURRENT_LENGTH<0.01) CURRENT_LENGTH=0.01*NATURAL_LENGTH;
      FORCE = -0.1*sq(NATURAL_LENGTH/CURRENT_LENGTH);
    }
  }
  
  Node getOtherNode(Node n) {
    if (n.equals(ATTACHED_NODES[0])) {
      return ATTACHED_NODES[1];
    } else {
      return ATTACHED_NODES[0];
    }
  }
  
  void attachTo(Node n1, Node n2) {
    ATTACHED_NODES[0] = n1;
    ATTACHED_NODES[1] = n2;
    n1.ATTACHED_SPRINGS.add(this);
    n2.ATTACHED_SPRINGS.add(this);
  }
  
  void draw() {
    stroke(255,0,0);
    line((float)ATTACHED_NODES[0].POSITION.x,(float)ATTACHED_NODES[0].POSITION.y,(float)ATTACHED_NODES[1].POSITION.x,(float)ATTACHED_NODES[1].POSITION.y);
  }
  
}
