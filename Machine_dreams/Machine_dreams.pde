color white, black;
int[] base_color=new int[2];
int col_index;
int[] buf;

int circ_x, circ_y, circ_r;

void setup() {
  white=color(255,255,255); black=color(0,0,0); 
  base_color[0]=12<<16|12<<8|255; 
  base_color[1]=255<<16|255<<8|255;
  col_index=0;
  
  // screen setup:
  size(600,400); background(white);

  buf=new int[width*height];

  circleSetup();
}

void circleSetup() {
  circ_x = int(0.5*width+random(-0.1,0.1)*width);
  circ_y = int(0.5*height+random(-0.1,0.1)*height);
  circ_r = int(random(0.15,0.3)*(width+height)/2.0);
}

void draw() {
  if (random(1)<0.001) {circleSetup();}

  if (col_index==0) {if (random(1)<0.01) {col_index=1;}} else {if (random(1)<0.04) {col_index=0;}}
  drawCircle();

  if (random(1)<0.05) {
    loadPixels();
    buf=moveBuf(4,4);
    buf=blurBuf(buf);
    drawBuf(buf);
  }
}




void drawCircle() {
  float xo=random(0.025)*width;
  float yo=random(0.025)*height;
  for (float rd=0.0;rd<TWO_PI;rd+=0.005) {
    int x=int(circ_x+xo+circ_r*cos(rd));
    int y=int(circ_y+yo+circ_r*sin(rd));
    softset(x,y,colorVar(base_color[col_index],60),0.2);
    if (random(1)<0.0001) {
      float r1=random(0.5*circ_r,circ_r);
      float r2=r1+2*random(circ_r);
      drawLine(circ_x+xo,circ_y+yo,x,y,r1,r2,rd,base_color[1-col_index],0.5);
    }
    if (random(1)<0.05) {
      float r1=random(0.95*circ_r,circ_r);
      float r2=random(circ_r,1.05*circ_r);
      drawLine(circ_x+xo,circ_y+yo,x,y,r1,r2,rd,base_color[col_index],0.1);
    }
  }
}

void drawLine(float x1, float y1, float x2, float y2, float r1, float r2, float rd, int c, float intensity) {
  for (float r=r1;r<r2;r+=0.5) {
    int x=int(x1+r*cos(rd));
    int y=int(y1+r*sin(rd));
    softset(x,y,c,intensity);
  }
}





int[] moveBuf(int xm, int ym) {
  //loadPìxels();
  int[] o=new int[width*height];
  for (int y=0;y<height;y++) {
    for (int x=0;x<width;x++) {
      int i=y*width+x;
      if (y>=ym&&x>=xm) {
        int si=(y-ym)*width+(x-xm);
        o[i]=pixels[si];
      } else {
        o[i]=base_color[1];
      }
    }
  }
  return o;
}

int[] blurBuf(int[] b_in) {
  int[] b_out=new int[width*height];
  float v=1/9.0;
  float[][] k={ {v,v,v}, {v,v,v}, {v,v,v} };
  for (int y=0;y<height;y++) {
    for (int x=0;x<width;x++) {
    
      float rr=0, gg=0, bb=0;
      for (int yo=-1;yo<=1;yo++) {
        for (int xo=-1;xo<=1;xo++) {
          int xp=x+xo, yp=y+yo;
          if (xp>=0&&xp<width&&yp>=0&&yp<height) {
            int i=yp*width+xp;
            rr+=v*(b_in[i]>>16&255);
            gg+=v*(b_in[i]>>8&255);
            bb+=v*(b_in[i]&255);
          } else {
            rr+=v*255.0; gg+=v*255.0; bb+=v*255.0;
          }
        }
      }
      
      b_out[y*width+x]=int(rr)<<16|int(gg)<<8|int(bb);
      
    }
  }
  return b_out;
}


void drawBuf(int[] b) {
  for (int y=0;y<height;y++) {
    for (int x=0;x<width;x++) {
      int i=y*width+x;
      //set(x,y,b[i]);
      softset(x,y,b[i],0.5);
    }
  }
}









int colorVar(int inC,int q) {
  int rr=inC>>16&255, gg=inC>>8&255, bb=inC&255;
  int off;
  do {off=(int)random(-q,q);} while (rr+off<0||rr+off>255); rr+=off;
  do {off=(int)random(-q,q);} while (gg+off<0||gg+off>255); gg+=off;
  do {off=(int)random(-q,q);} while (bb+off<0||bb+off>255); bb+=off;
  return (rr<<16|gg<<8|bb);
}

void softset(int x, int y, int c1, float p1) {
  if (x>=0&&x<width&&y>=0&&y<height) {
    float p2=1.0-p1;
    int c2=get(x,y);
    int r=int(p1*(c1>>16&255)+p2*(c2>>16&255));
    int g=int(p1*(c1>>8&255)+p2*(c2>>8&255));
    int b=int(p1*(c1&255)+p2*(c2&255));
    set(x,y,(255<<24|r<<16|g<<8|b));
  }
}
