// grower

Moisture[] moisture; int nrOfMoist; color moistColor;

Grower[] grower; int nrOfGrow, maxGrow;
boolean nextFrameInit;
float gDiag;

void setup() {

  size(640,480); background(255); gDiag=sqrt(sq(width)+sq(height));
  nextFrameInit=true;
}

void initMoistures() {
  moistColor=color(80,120,255);
  nrOfMoist=10; moisture=new Moisture[nrOfMoist]; for (int i=0;i<nrOfMoist;i++) { moisture[i]=new Moisture(); }
  nrOfGrow=1; maxGrow=5000; grower=new Grower[maxGrow]; grower[0]=new Grower(); grower[0].setPos(width/2,height/2);
}

void mousePressed()  {
  nextFrameInit=true;
}

void draw() {
  if (nextFrameInit) {
    nextFrameInit=false;
    background(255);
    initMoistures();
    for (int i=0;i<nrOfMoist;i++) { moisture[i].drawMe(); }
  } else {
  
    for (int i=0;i<nrOfGrow;i++) {
      grower[i].update();
    }
    
    int newNrOfGrow=nrOfGrow;
    for (int i=0;i<nrOfGrow;i++) {
      if (grower[i].createChild&&newNrOfGrow<maxGrow-1) {
        println("CHILD! at frame "+frameCount);
        grower[newNrOfGrow]=new Grower();
        grower[newNrOfGrow].setPos(grower[i].x,grower[i].y);
        grower[newNrOfGrow].life=(1+grower[i].life)/2.0;
        newNrOfGrow++;
      }
    }
    nrOfGrow=newNrOfGrow;
  }
}

class Grower {
  float x,y;  // position
  float dir;  // direction
  float life;  // 1..0
  float bS=2;  // basic speed;
  float bT=0.5;// basic turn;
  int nrOfChild=0, maxChild=2;
  
  boolean createChild=false;;
  
  Grower() {
    x=random(width); y=random(height); dir=random(TWO_PI); life=1;
  }
  
  void setPos(float x, float y) {
    this.x=x; this.y=y;
  }
  
  void update() {
    createChild=false;
    if (life>0) {
      life-=0.002;;
      //dir+=bT*(noise(life,x,y)-0.5);
      dir+=bT*random(-0.5,0.5);
      float xm=bS*life*cos(dir), ym=bS*life*sin(dir);
      x+=xm; y+=ym;
      if (get(int(x),int(y))==moistColor) { 
        bT=(10*bT+2)/11.0;
        life=(100*life+1)/101.0;
      } else {
        bT=(10*bT+0.5)/11.0;
      }
      
      //set(int(x),int(y),0);   
      softset(int(x),int(y),0,0.1);
      if (random(1)<0.005&&nrOfChild<maxChild) {createChild=true; nrOfChild++;}
    }
    //updatePixels();
  }
  
  
}

class Moisture {
  float x,y,r;
  Pos2D[] pixel=new Pos2D[width*height];
  float[] frq=new float[10], amp=new float[10], phs=new float[10];
  
  Moisture() {
    init();
  }
  
  void init() {
    x=random(width); y=random(height); r=gDiag*random(0.005,0.05);
    for (int i=0;i<10;i++) {
      frq[i]=int(random(1,20)); amp[i]=random(0.01,0.1)*r; phs[i]=random(TWO_PI);
    }
  }
  
  void drawMe() {
    for (float mscl=0;mscl<=1;mscl+=1/(r*2)) {
      for (float rd=0;rd<TWO_PI;rd+=TWO_PI/500.0) {
        float ra=r;
        for (int i=0;i<10;i++) {ra+=amp[i]*cos(frq[i]*rd+phs[i]); }
        set(int(x+mscl*ra*cos(rd)),int(y+mscl*ra*sin(rd)),moistColor);
      }
    }
  }
}


class Pos2D {
  float x,y;
  Pos2D(float inx, float iny) {
    x=inx; y=iny;
  }
}


void softset(int x, int y, color c1, float p1) {
  float p2=1.0-p1;
  color c2=get(x,y);
  int r=int(p1*(c1>>16&255)+p2*(c2>>16&255));
  int g=int(p1*(c1>>8&255)+p2*(c2>>8&255));
  int b=int(p1*(c1&255)+p2*(c2&255));
  set(x,y,(255<<24|r<<16|g<<8|b));
}
