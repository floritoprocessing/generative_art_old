Sketcher[] sk; int nrOfSk=1000, actualSk;
int drawColor=0;
int drawPhase;

// variables used for calculating average center
float br, avx, avy, div, tot_avx, tot_avy;
int tot_x=0,tot_y=0;

void setup() {
  size(800,600);
  background(255); //framerate(50);
  sk=new Sketcher[nrOfSk];
  initSketchers();
  drawPhase=1;
}

void initSketchers() {
  for (int i=0;i<nrOfSk;i++) { sk[i]=new Sketcher(width,height); sk[i].init(); }
  actualSk=0;
}

void draw() {
  if (drawPhase==1) {
    // DRAW SKETCHERS -------------------------------------------------------
    for (int ii=0;ii<50;ii++) {
    if (random(1)<0.001) {drawColor=0xffffff-drawColor;println(drawColor);}
      if (actualSk<nrOfSk-1) {
        if (random(1)<0.07) {
          actualSk++; 
          float rand=random(1);
          if (rand<0.001) {        // 0.1%
            sk[actualSk].init();
          } else if (rand<0.01) { // 0.9%
            sk[actualSk].morph(sk[(int)random(actualSk)]);
          } else {                // 99%
            sk[actualSk].morph(sk[actualSk-1]);
          }        
        }
        sk[actualSk].makeLine(drawColor);
      } else {drawPhase=2;}
    }
  } else if (drawPhase==2) {
  
    
    // GET CENTER OF IMAGE
    println("get center of image");
    
    // get average x-position:
    tot_avx=0;
    for (int y=0;y<height;y++) {
      avx=0; div=0;
      for (int x=-width/2;x<width/2;x++) {
        br=255-brightness(get(x+width/2,y));
        if (br!=255) {avx+=br*x; div+=br;}
      }
      if (div!=0) {avx/=div;}
      set(int(avx)+width/2,y,255);
      tot_avx+=avx; if (avx!=0) {tot_y++;}
    }
    tot_avx=tot_avx/(float)tot_y+width/2;
    stroke(255,0,0);fill(255,0,0); line(tot_avx,0,tot_avx,height);
    println("tot_avx: "+tot_avx);
    
    // get average y-position:
    tot_avy=0;
    for (int x=0;x<width;x++) {
      avy=0; div=0;
      for (int y=-height/2;y<height/2;y++) {
        br=255-brightness(get(x,y+height/2));
        if (br!=255) {avy+=br*y; div+=br;}
      }
      if (div!=0) {avy/=div;}
      set(x,int(avy)+height/2,255);
      tot_avy+=avy; if (avy!=0) {tot_x++;}
    }
    tot_avy=tot_avy/(float)tot_x+height/2;
    stroke(255,0,0);fill(255,0,0); line(0,tot_avy,width,tot_avy);
    println("tot_avy: "+tot_avy);
    
    drawPhase=3;
  } else if (drawPhase==3) {
    // FADE OUT AWAY FROM AVERAGE CENTER
    println("Fade out from center");
    float maxD=sqrt(width*width+height*height);
    for (int x=0;x<width;x++) {
      for (int y=0;y<height;y++) {
        float d=dist(x,y,tot_avx,tot_avy)/maxD;
        softset(int(x),int(y),0x000000,d);
      }
    }
    drawPhase=4;
  } else if (drawPhase==4) {
//    initSketchers();
//    drawPhase=5;
  }
}

class Sketcher {
  int w,h;    //max width/height;
  Vec cen=new Vec(), rad=new Vec();
  float rd1,rd2,rdr;
  
  Sketcher(int limX, int limY) {
    w=limX; h=limY;
  }
  
  void init() {
     cen.init(random(w),random(h));
     rad.init(random(w/64.0,w/8.0),random(h/64.0,h/8.0));
     rd1=random(TWO_PI); rd2=rd1+random(PI/16.0,PI); rdr=abs(rd1-rd2);
  }
  
  void morph(Sketcher isk) {
    float rand=random(1);
    float cxoff=0,cyoff=0,rxoff=0,ryoff=0,rd1off=0,rd2off=0;
    
    if (rand<.2) {
      cxoff=random(-5,5); cyoff=random(-5,5);
    } else if (rand<.6) {
      rxoff=random(-w/128.0,w/128.0); ryoff=random(-h/128.0,h/128.0);
    } else {
      rd1off=random(-PI/10.0,PI/10.0); rd2off=random(PI/16.0,PI);
    }
    
    cen.init(isk.cen.x+cxoff,isk.cen.y+cyoff);
    rad.init(isk.rad.x+rxoff,isk.rad.y+ryoff);
    rd1=isk.rd1+rd1off; rd2=rd1+rd2off; rdr=abs(rd1-rd2);
  }
  
  void makeLine(int c) {
    float trd1=rd1+random(-rdr/4.0,rdr/4.0);
    float trd2=rd2+random(-rdr/4.0,rdr/4.0);
    float tcx=cen.x+random(-w/150.0,w/150.0), tcy=cen.y+random(-h/150.0,h/150.0);
    for (float rd=trd1;rd<=trd2;rd+=TWO_PI/720.0) {
      float sx=tcx+rad.x*cos(rd), sy=tcy+rad.y*sin(rd);
      softset(int(sx),int(sy),c,0.01);
    }
  }
}


class Vec {
  float x,y;
  Vec() {}
  void init(float ix, float iy) {x=ix;y=iy;}
}

void softset(int x, int y, int c1, float p1) {
  float p2=1.0-p1;
  int c2=get(x,y);
  int r=int(p1*(c1>>16&255)+p2*(c2>>16&255));
  int g=int(p1*(c1>>8&255)+p2*(c2>>8&255));
  int b=int(p1*(c1&255)+p2*(c2&255));
  set(x,y,(255<<24|r<<16|g<<8|b));
}
