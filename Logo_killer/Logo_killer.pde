PImage[] logo;
String[] logoName;
int nrOfLogos, logoNr;
int mode, fc;
color bg;

void setup() {
  size(640,480,P3D); background(255); imageMode(CENTER); //framerate(50);
  bg=color(102,102,102);
  
  nrOfLogos=5; logoNr=(int)random(nrOfLogos);
  logoName=new String[nrOfLogos];
  logoName[0]="cocacola_320x240.jpg";
  logoName[1]="win95_320x240.jpg";
  logoName[2]="mtv.jpg";
  logoName[3]="playstation.jpg";
  logoName[4]="bartsimpson.jpg";
  
  logo=new PImage[nrOfLogos];
  for (int i=0;i<nrOfLogos;i++) {
    logo[i]=loadImage(logoName[i]);
  }
  mode=1;
  fc=0;
}

void draw() {
  if (mode==1) {
    background(bg);
    logoNr++; if (logoNr==nrOfLogos) {logoNr=0;}
    image(logo[logoNr],width/2,height/2);
    mode=2; fc=0;
  }
  if (mode==2) {
    fc++;
    if (fc>250) {nextMode();}
  }
  if (mode==3) {
    for (int i=0;i<=800;i++) {
      // take a sample from the original image
      float x1=random(logo[logoNr].width); 
      float y1=random(logo[logoNr].height);
      float x2=x1+random(logo[logoNr].width-x1)/12.0;
      float y2=y1+random(logo[logoNr].height-y1)/12.0;
      float xh=x2/2.0, yh=y2/2.0;
      
      float sx=random(width);
      float sy=random(height);
      
      pushMatrix();
      translate(sx,sy);
      
      rotateX(random(TWO_PI));
      rotateY(random(TWO_PI));//
      //rotateZ(random(TWO_PI));
      scale(pow(2,random(-3,6)));
      for (float x=x1;x<x2;x++) {
        for (float y=y1;y<y2;y++) {
          color pick=logo[logoNr].pixels[int(y)*logo[logoNr].width+int(x)];
          stroke(pick);
          point((x-x1)-xh,(y-y1)-yh);
        }
      }
      popMatrix();
    }
  }
}

void mousePressed() {
  nextMode();
}

void nextMode() {
  //background(bg);
  mode++; if (mode==4) {mode=1;}
}
