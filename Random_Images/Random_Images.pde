float w,h;  // main width/height
Attractor[] att; int nrOfAtt; // attractor
Lub[] lub; int nrOfLub;       // Lubs

void setup() {
  // screensetup
  size(800,600,P3D); w=width; h=height;
  startAgain();
}

void startAgain() {
  background(255);
  // attractor setup
  float cx=random(.1,.9), cy=random(.1,.9);
  nrOfAtt=8; att=new Attractor[nrOfAtt]; for (int i=0;i<nrOfAtt;i++) {att[i]=new Attractor(cx,cy);}
  // lub setup
  nrOfLub=1000; lub=new Lub[nrOfLub]; for (int i=0;i<nrOfLub;i++) {lub[i]=new Lub(att);}
}

void draw() {
  //background(255);
  for (int f=0;f<20;f++) {
  for (int i=0;i<nrOfAtt;i++) {att[i].update();}
  for (int i=0;i<nrOfLub;i++) {lub[i].update();}
  }
}


class Attractor {
  float sx,sy,x,y,d;
  float mrd,mrd_m, mr, mr_m;
  color c; float r,g,b;
  Attractor(float cx, float cy) {
    sx=cx; sy=cy;
    mrd=random(TWO_PI); mrd_m=random(0.005,0.05); if (random(1)<0.5) {mrd_m*=-1;} 
    mr=random(0.5); mr_m=random(-0.001,0.001);
    x=sx+mr*cos(mrd); y=sy+mr*sin(mrd); d=0.01; 
    c=color(random(256),random(256),random(256)); r=red(c); g=green(c); b=blue(c);
  }
  void update() {
    mrd_m*=0.9995;
    mrd+=mrd_m;
    
    mr_m*=0.9995;
    mr+=mr_m;
    
    x=sx+mr*cos(mrd); y=sy+mr*sin(mrd);
    int tx=int(x*w), ty=int(y*h);
//    set(tx,ty,colorFade(c,get(tx,ty),0.9));
//    stroke(0); fill(c);
//    ellipseMode(CENTER_DIAMETER);
//    ellipse(x*w,y*h,d*w,d*w);
  }
}

class Lub {
  float x,y,xm,ym; color c; float r,g,b;
  Attractor[] at;
  int type;
  float damp=0.99;
  Lub(Attractor[] in_att) {
    x=random(1); y=random(1); xm=random(-0.005,0.005); ym=random(-0.005,0.005);
    type=(int)random(3);
    c=color(random(256),random(256),random(256)); r=red(c); g=green(c); b=blue(c);
    at=in_att;
  }
  void update() {
    for (int i=0;i<nrOfAtt;i++) {
      if (type==0) {
        float rfac=abs((r-at[i].r)/255.0);
        xm+=0.0001*rfac*(att[i].x-x);
        ym+=0.0001*rfac*(att[i].y-y);
      } else if (type==1) {
        float gfac=abs((g-at[i].g)/255.0);
        xm+=0.00001*gfac*(att[i].x-x);
        ym+=0.00001*gfac*(att[i].y-y);
      } else {
        float bfac=abs((b-at[i].b)/255.0);
        xm+=0.00001*bfac*(att[i].x-x);
        ym+=0.00001*bfac*(att[i].y-y);
      }
      xm+=random(-0.00001,0.00001);
      ym+=random(-0.00001,0.00001);
    }
    xm*=damp; ym*=damp;
    x+=xm; y+=ym; 
    if (x<0||x>1) {xm*=-1;}
    if (y<0||y>1) {ym*=-1;}
    int sx=int(x*w), sy=int(y*h);
    set(sx,sy,colorFade(c,get(sx,sy),0.05));
  }
}

color colorFade(color a, color b, float perc1) {
  float perc2=1.0-perc1;
  float rr=perc1*red(a)+perc2*red(b), gg=perc1*green(a)+perc2*green(b), bb=perc1*blue(a)+perc2*blue(b);
  return color(rr,gg,bb);
}
