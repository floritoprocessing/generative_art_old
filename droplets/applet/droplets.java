import processing.core.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class droplets extends PApplet {// droplets

Vector droplets=new Vector();

public void setup() {
  size(800,600);
  background(212,212,224);
  /*
  for (int i=0;i<5;i++) {
    Droplet d=new Droplet();
    if (d.mov.y>0) {d.mov.negY();}
    droplets.addElement(d);
  }
  */
}

public void mousePressed() {
  Droplet d=new Droplet();
  if (d.mov.y>0) {d.mov.negY();}
  d.pos.x=mouseX/(float)width;
  d.pos.y=mouseY/(float)height;
  droplets.addElement(d);
}

public void draw() {
  //background(212,212,224);
  float frameTime=millis();
  
  do { 
    for (int i=0;i<droplets.size();i++) {
      Droplet d=(Droplet)droplets.elementAt(i);
      d.update();
      if (d.giveBirth) {
        droplets.addElement(new Droplet(d.Child()));
      }
      d.toScreen(width,height);
    }
  } while (millis()-frameTime<0.04f);
  
}



class Droplet {
  Vec pos=new Vec(Math.random(),0.8f+0.2f*Math.random(),0);  // position
  Vec mov=new Vec(Math.PI/2.0f,0.001f+0.001f*Math.random());  // movement
  double m=1.0f;    // mass ; maximum is 1
  double r=0;      // radius for drawing; depends on mass
  double friction=0.999f;
  int c=color(158,108,71,8);  // color
  boolean giveBirth=false;
  double birthRate=0.05f;

  Droplet(Vec _pos, Vec _mov, double _m, int _c) {
    pos=_pos;
    mov=_mov;
    m=_m;
    c=_c;
    recalcRadius();
  }

  Droplet(Droplet _d) {
    pos=_d.pos;
    mov=_d.mov;
    m=_d.m;
    recalcRadius();
  }

  Droplet() {
    recalcRadius();
  }

  public Droplet Child() {
    double cm=m*Math.random();
    m-=cm;
    recalcRadius();
    Vec cpos=new Vec(pos);
    Vec cmov=new Vec(mov);
    double splitDegree=-Math.PI/6.0f+Math.PI/3.0f*Math.random();
    cmov.rotZ(splitDegree);
    cmov.mul(Math.cos(splitDegree));
    int cc=c;
    return (new Droplet(cpos,cmov,cm,cc));
  }

  public void recalcRadius() {
    r=20*Math.sqrt(m/Math.PI);
  }

  public void randomDir() {
    if (Math.random()<0.5f) {
      mov.negX();
    }
    if (Math.random()<0.5f) {
      mov.negY();
    }
    if (Math.random()<0.5f) {
      mov.negZ();
    }
  }

  public void update() {
    giveBirth=false;
    if (Math.random()<birthRate&&m>0.001f) {
      giveBirth=true;
    }

    mov.mul(friction);
    
    if (pos.x+mov.x>1||pos.x+mov.x<0) {
      mov.negX();
    }
    if (pos.y+mov.y>1||pos.y+mov.y<0) {
      mov.negY();
    }
    pos.add(mov);
  }

  public void toScreen(int _w, int _h) {
    fill(c); noStroke();
    ellipseMode(CENTER_RADIUS);
    ellipse((float)(pos.x*_w),(float)(pos.y*_h),(float)r,(float)r);
  }

}


/*****************************
 *  Vec class and functions  *
 *****************************/

class Vec {
  double x=0,y=0,z=0;

  Vec() {
  }

  Vec(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  Vec(double rd, double r) {
    x=r*Math.cos(rd);
    y=r*Math.sin(rd);
    z=0;
  }

  Vec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }

  public void add(Vec _v) {
    x+=_v.x;
    y+=_v.y;
    z+=_v.z;
  }

  public void sub(Vec _v) {
    x-=_v.x;
    y-=_v.y;
    z-=_v.z;
  }

  public void mul(double p) {
    x*=p;
    y*=p;
    z*=p;
  }

  public void negX() {
    x=-x;
  }

  public void negY() {
    y=-y;
  }

  public void negZ() {
    z=-z;
  }

  public void neg() {
    negX();
    negY();
    negZ();
  }

  public void rotX(double rd) {
    double SIN=Math.sin(rd); 
    double COS=Math.cos(rd);
    double yn=y*COS-z*SIN;
    double zn=z*COS+y*SIN;
    y=yn;
    z=zn;
  }

  public void rotY(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-z*SIN; 
    double zn=z*COS+x*SIN;
    x=xn;
    z=zn;
  }

  public void rotZ(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-y*SIN; 
    double yn=y*COS+x*SIN;
    x=xn;
    y=yn;
  }

  public void rot(double xrd, double yrd, double zrd) {
    rotX(xrd);
    rotY(yrd);
    rotZ(zrd);
  }
}
static public void main(String args[]) {   PApplet.main(new String[] { "droplets" });}}