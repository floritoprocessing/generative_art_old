// droplets
import java.util.Vector;

Vector droplets=new Vector();

void setup() {
  size(800,600);
  background(212,212,224);
  /*
  for (int i=0;i<5;i++) {
    Droplet d=new Droplet();
    if (d.mov.y>0) {d.mov.negY();}
    droplets.addElement(d);
  }
  */
}

void mousePressed() {
  Droplet d=new Droplet();
  if (d.mov.y>0) {d.mov.negY();}
  d.pos.x=mouseX/(float)width;
  d.pos.y=mouseY/(float)height;
  droplets.addElement(d);
}

void draw() {
  //background(212,212,224);
  float frameTime=millis();
  
  do { 
    for (int i=0;i<droplets.size();i++) {
      Droplet d=(Droplet)droplets.elementAt(i);
      d.update();
      if (d.giveBirth) {
        droplets.addElement(new Droplet(d.Child()));
      }
      d.toScreen(width,height);
    }
  } while (millis()-frameTime<0.04);
  
}



class Droplet {
  Vec pos=new Vec(Math.random(),0.8+0.2*Math.random(),0);  // position
  Vec mov=new Vec(Math.PI/2.0,0.001+0.001*Math.random());  // movement
  double m=1.0;    // mass ; maximum is 1
  double r=0;      // radius for drawing; depends on mass
  double friction=0.999;
  color c=color(158,108,71,8);  // color
  boolean giveBirth=false;
  double birthRate=0.05;

  Droplet(Vec _pos, Vec _mov, double _m, color _c) {
    pos=_pos;
    mov=_mov;
    m=_m;
    c=_c;
    recalcRadius();
  }

  Droplet(Droplet _d) {
    pos=_d.pos;
    mov=_d.mov;
    m=_d.m;
    recalcRadius();
  }

  Droplet() {
    recalcRadius();
  }

  Droplet Child() {
    double cm=m*Math.random();
    m-=cm;
    recalcRadius();
    Vec cpos=new Vec(pos);
    Vec cmov=new Vec(mov);
    double splitDegree=-Math.PI/6.0+Math.PI/3.0*Math.random();
    cmov.rotZ(splitDegree);
    cmov.mul(Math.cos(splitDegree));
    color cc=c;
    return (new Droplet(cpos,cmov,cm,cc));
  }

  void recalcRadius() {
    r=20*Math.sqrt(m/Math.PI);
  }

  void randomDir() {
    if (Math.random()<0.5) {
      mov.negX();
    }
    if (Math.random()<0.5) {
      mov.negY();
    }
    if (Math.random()<0.5) {
      mov.negZ();
    }
  }

  void update() {
    giveBirth=false;
    if (Math.random()<birthRate&&m>0.001) {
      giveBirth=true;
    }

    mov.mul(friction);
    
    if (pos.x+mov.x>1||pos.x+mov.x<0) {
      mov.negX();
    }
    if (pos.y+mov.y>1||pos.y+mov.y<0) {
      mov.negY();
    }
    pos.add(mov);
  }

  void toScreen(int _w, int _h) {
    fill(c); noStroke();
    ellipseMode(RADIUS);
    ellipse((float)(pos.x*_w),(float)(pos.y*_h),(float)r,(float)r);
  }

}


/*****************************
 *  Vec class and functions  *
 *****************************/

class Vec {
  double x=0,y=0,z=0;

  Vec() {
  }

  Vec(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  Vec(double rd, double r) {
    x=r*Math.cos(rd);
    y=r*Math.sin(rd);
    z=0;
  }

  Vec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }

  void add(Vec _v) {
    x+=_v.x;
    y+=_v.y;
    z+=_v.z;
  }

  void sub(Vec _v) {
    x-=_v.x;
    y-=_v.y;
    z-=_v.z;
  }

  void mul(double p) {
    x*=p;
    y*=p;
    z*=p;
  }

  void negX() {
    x=-x;
  }

  void negY() {
    y=-y;
  }

  void negZ() {
    z=-z;
  }

  void neg() {
    negX();
    negY();
    negZ();
  }

  void rotX(double rd) {
    double SIN=Math.sin(rd); 
    double COS=Math.cos(rd);
    double yn=y*COS-z*SIN;
    double zn=z*COS+y*SIN;
    y=yn;
    z=zn;
  }

  void rotY(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-z*SIN; 
    double zn=z*COS+x*SIN;
    x=xn;
    z=zn;
  }

  void rotZ(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-y*SIN; 
    double yn=y*COS+x*SIN;
    x=xn;
    y=yn;
  }

  void rot(double xrd, double yrd, double zrd) {
    rotX(xrd);
    rotY(yrd);
    rotZ(zrd);
  }
}
