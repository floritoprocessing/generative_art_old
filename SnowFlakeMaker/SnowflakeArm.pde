class SnowflakeArm {
  
  color DRAW_COLOR = color(255,255,255,24);
  
  float cenX = 0;
  float cenY = 0;
  float armGrow = 0;
  float rot = 0;
  int arms = 0;
  float childFac = 1.0;
  int depth = 0;
  float armLen = 0;
  float armLenMax = 0;
  
  boolean childrenCreated = false;
  SnowflakeArm[] child;
  
  SnowflakeArm(float _armGrow, float _armLenMax, float _rot,int _arms, float _childFac,int _depth) {
//    cenX = cx;
//    cenY = cy;
    armGrow = _armGrow;
    armLenMax = _armLenMax;
    rot = _rot;
    arms = _arms;
    childFac = _childFac;
    depth = _depth;
    child = new SnowflakeArm[arms];
  }
  
  
  void update() {
    
    pushMatrix();
    rotateZ(rot);
    stroke(DRAW_COLOR);
    line(0,0,0,armLen);
    if (armLen<armLenMax) {
      armLen+=armGrow;
    } else if (depth>0) {
      if (!childrenCreated) {
        // create child
        for (int i=0;i<child.length;i++) child[i] = new SnowflakeArm(armGrow*childFac,armLenMax*childFac,2*PI*i/float(arms),arms,childFac,depth-1);
        childrenCreated = true;
      } else {
        // update child
        for (int i=0;i<child.length;i++) {
          pushMatrix();
          translate(0,armLen);
          rotateZ(rot);
          child[i].update();
          popMatrix();
        }
      }
      
    }
    
    popMatrix();
  }
  
  
}
