Snowflake snowflake;

void setup() {
  size(500,500,P3D);
  //Snowflake(float armGrow, float maxArm, int arms, float childFac, int depth)
  snowflake = new Snowflake(random(20,100),random(20,100),(int)random(3,11),random(0.1,1.1),(int)random(1,7));
}

void keyPressed() {
  if (key==' ') setup();
}

void mousePressed() {
  setup();
}


void draw() {
  background(31,100,159);
  translate(width/2.0,height/2.0);
  snowflake.update();
}
