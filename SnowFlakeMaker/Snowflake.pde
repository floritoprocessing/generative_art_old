class Snowflake {
  
  SnowflakeArm[] arm;
  
  float armGrow, maxArm, childFac;
  int arms, depth;
  
  Snowflake(float _armGrow, float _maxArm, int _arms, float _childFac, int _depth) {
    armGrow = _armGrow;
    maxArm = _maxArm;
    arms = _arms;
    childFac = _childFac;
    depth = _depth;
    arm = new SnowflakeArm[arms];
    for (int i=0;i<arm.length;i++) arm[i] = new SnowflakeArm(armGrow,maxArm,2*PI*i/float(arms),arms,childFac,depth-1);
  }
  

  void update() {
    for (int i=0;i<arm.length;i++) arm[i].update();
  }
  
}
