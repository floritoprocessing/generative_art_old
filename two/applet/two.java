import processing.core.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class two extends PApplet {public void setup() {
  size(400,400); 
  colorMode(HSB,255); background(0,0,0);
  //smooth();
  framerate(25);
  initGravity();
  setupParticles();
}

public void draw() {
  background(0,0,0);
  updateGravity();
  updateParticles();
  drawParticles();
}



int nrOfGravities=3;
public void initGravity() { 
  grav=new Vec[nrOfGravities];
  for (int i=0;i<nrOfGravities;i++) {
    grav[i]=new Vec(random(TWO_PI),0.0002f); 
  }
}
public void updateGravity() { 
  for (int i=0;i<nrOfGravities;i++) {
    if (random(1)<0.1f) { grav[i].setDir(random(TWO_PI)); } 
  }
}

Vec[] grav;
class Vec {
  float rd=0, st=0, x=0, y=0;
  Vec(float _rd, float _st) { rd=_rd; st=_st; createXY(); }
  public void setDir(float _rd) {rd=_rd; createXY();}
  public void setStren(float _st) {st=_st; createXY(); }
  public void createXY() { x=st*cos(rd); y=st*sin(rd); } 
}



/*
PARTICLE CLASS
*/

int nrOfParticles=75;
Particle[] particle;

public void setupParticles() {
  particle=new Particle[nrOfParticles];
  for (int i=0;i<nrOfParticles;i++) {
    particle[i]=new Particle(width,height);
  }
}

public void updateParticles() { for (int i=0;i<nrOfParticles;i++) { particle[i].update(); } }
public void drawParticles() { for (int i=0;i<nrOfParticles;i++) { particle[i].toScreen(); } }

class Particle {
  int type=0;
  float damp=0.98f;
  float gravDevFreq=random(0.5f,5);
  float x=random(1), y=random(1);
  float[] tr_x, tr_y, tr_h, tr_s, tr_b; int traceLength=40;
  float xm=0,ym=0;
  float hAdd=random(12), hm=0.1f;
  float h=0, s=255, b=0;
  float xscl,yscl;
  
  Particle(float _xscl, float _yscl) {
    xscl=_xscl; yscl=_yscl;
    tr_x=new float[traceLength]; tr_y=new float[traceLength];
    tr_h=new float[traceLength]; tr_s=new float[traceLength]; tr_b=new float[traceLength];
    initTrace();
  }
  
  public void initTrace() { 
    for (int i=0;i<traceLength;i++) {
      tr_x[i]=x; tr_y[i]=y; 
      tr_h[i]=0; tr_s[i]=0; tr_b[i]=0;
    } 
  }
  
  public void updateTrace(float _x, float _y, float _h, float _s, float _b) {
    for (int i=traceLength-1;i>0;i--) {
      tr_x[i]=tr_x[i-1]; tr_y[i]=tr_y[i-1];
      tr_h[i]=tr_h[i-1]; tr_s[i]=tr_s[i-1]; tr_b[i]=tr_b[i-1];
    }
    tr_x[0]=_x; tr_y[0]=_y;
    tr_h[0]=_h; tr_s[0]=_s; tr_b[0]=_b;
  }
  
  public void drawTrace() {
    for (int i=0;i<traceLength-1;i++) {
      if (tr_b[i]>0) {
        int c=color(tr_h[i],tr_s[i],tr_b[i]);
        stroke(c);
        line(tr_x[i]*xscl,tr_y[i]*yscl,tr_x[i+1]*xscl,tr_y[i+1]*yscl);
      }
    }
  }
  
  public void update() {
    if (random(1)<0.001f) { type=(int)random(nrOfGravities); }
  
    hAdd+=hm;
//    float i=sqrt(sq(xm)+sq(ym))/0.005;
    float i=(sq(xm)+sq(ym))/0.000025f;
    h=i*50+hAdd+(type/(float)nrOfGravities)*255.0f; if (h>255) {h-=255;}
    b=255-i*300; 

    float gravDev=radians(15)*sin(millis()/(1000.0f*gravDevFreq));
    Vec tgrav=new Vec(grav[type].rd+gravDev,grav[type].st);
    xm+=tgrav.x; ym+=tgrav.y;
    xm*=damp; ym*=damp;
    x+=xm; if (x>1||x<0) { xm*=-1; }
    y+=ym; if (y>1||y<0) { ym*=-1; }
    
    updateTrace(x,y,h,s,b);
    drawTrace();
  }
  
  public void toScreen() {
  }
}
}