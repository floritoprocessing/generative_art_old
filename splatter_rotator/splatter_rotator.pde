

void setup() {
  size(800,400,P3D); background(0xf0f0f0);
  createRotator();
  createSplats();
}

void draw() {
  background(0xf0f0f0);
  rotator.update();
  rotator.toScreen();
  updateAllSplats();
  showAllSplats();
}



///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Rotator Class ////////////////////////////////////////

Rotator rotator;

void createRotator() { rotator=new Rotator(); }

class Rotator {
  Vec pos=new Vec(0,0,100);
  int nrOfSprayers=3; Sprayer[] sprayer;
  float rot=0, rotSpeed=radians(1);
  
  Rotator() {
    sprayer=new Sprayer[nrOfSprayers];
    for (int i=0;i<nrOfSprayers;i++) {sprayer[i]=new Sprayer();}
  }
  
  void update() {
    pos.x=mouseX; pos.y=mouseY;
    rot+=rotSpeed;
    
    for (int i=0;i<nrOfSprayers;i++) {
      sprayer[i].pos=pos;
      sprayer[i].rotZ=rot+radians(360*i/(float)nrOfSprayers);
      sprayer[i].update();
    }
  }
   
  void toScreen() {
    for (int i=0;i<nrOfSprayers;i++) {
      sprayer[i].toScreen();
    }
  }
}

//////////////////////////////////////// end Rotator Class ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////





class Sprayer {
  float radius=30;
  float sprayIntensity=0.1;
  float rotZ;
  Vec pos;
  
  Sprayer() {}
  
  void update() {
    if (random(1)<sprayIntensity) {
      int index=getNextAvailableSplat();
      if (index!=-1) {
        splat[index].activate(pos,rotZ); print(index+" ");
      }
    }
  }
  
  void toScreen() { 
    pushMatrix(); stroke(0); translate(pos.x,pos.y,pos.z); rotateZ(rotZ); line(0,0,0,-radius); popMatrix();
    pushMatrix(); stroke(0,0,0,30); translate(pos.x,pos.y,0); rotateZ(rotZ); line(0,0,0,-radius); popMatrix();
  }
  
}






int nrOfSplats=200;
Splat[] splat;

void createSplats() {
  splat=new Splat[nrOfSplats];
  for (int i=0;i<nrOfSplats;i++) {
    splat[i]=new Splat();
  }
}

int getNextAvailableSplat() {
  int output=-1,i=0;
  boolean found=false;
  do {
    if (!splat[i].active) { found=true;output=i;}
    i++;
  } while (!found&&i<nrOfSplats);
  return output;
}

void updateAllSplats() {
  for (int i=0;i<nrOfSplats;i++) {
    if (splat[i].active) {splat[i].update();}
  }
}

void showAllSplats() {
  for (int i=0;i<nrOfSplats;i++) {
    if (splat[i].active) {splat[i].toScreen();}
  }
}

class Splat {
  color col=0xffff0000;
  float mass=10;
  boolean active=false;
  Vec pos, mov;
  
  Splat() {}
  
  void activate(Vec _pos, float _rotZ) {
    active=true;
    pos=new Vec(); mov=new Vec();
    pos.y-=1;
    pos.rotZ(_rotZ);  
    pos.Add(_pos);
  }
  
  void update() {
    pos.Add(mov);
  }
  
  void toScreen() {
    pushMatrix();
    translate(pos.x,pos.y,pos.z);
    noStroke();fill(col);
    ellipseMode(DIAMETER); ellipse(0,0,5,5);
    popMatrix();
  }
}








class Vec {
  float x=0,y=0,z=0;
  Vec() {}
  Vec(Vec a) {x=a.x;y=a.y;z=a.z;}
  Vec(float _x, float _y, float _z) {x=_x;y=_y;z=_z;}
  void Add(Vec b) { x+=b.x; y+=b.y; z+=b.z;}
  void Mul(float n) { x*=n; y*=n; z*=n; }
  void rotZ(float rd) { float SIN=sin(rd),COS=cos(rd), xn=x*COS-y*SIN, yn=y*COS+x*SIN; x=xn; y=yn; }
}
