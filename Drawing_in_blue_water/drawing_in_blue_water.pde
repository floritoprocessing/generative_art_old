PImage img;
int maxDots=120001;
int nrOfDots;
Dot[] dot=new Dot[maxDots+1];
float lmx=0.0, lmy=0.0, mxmov=0.0, mymov=0.0;

void setup() {
  size(400,300);
  colorMode(RGB, 255);
  img = loadImage("water400x300.jpg"); 

  int i=0;
  for (int y=1;y<=height;y++) {
    for (int x=1;x<=width;x++) {
      color c=img.pixels[int(y-1)*width+int(x-1)];
      if (random(1)<0.15) {
        dot[i]=new Dot(x,y,c);
        i++;
      }
    }
  }
  nrOfDots=i-1;
}

void draw() {
  background(0);
  for (int i=1;i<=nrOfDots;i++) {
    dot[i].update();
  }
}

void mousePressed() {
  lmx=mouseX; lmy=mouseY;
}
void mouseDragged() {
  mxmov=mouseX-lmx;
  mymov=mouseY-lmy;
  lmx=mouseX; lmy=mouseY;
}

class Dot {
  float x,y,sx,sy;
  float d,dm=20000.0;
  float xmov=0,ymov=0;
  float movability;
  //int nrOfNeighbours;
  color col=color(255,255,255);
  
  Dot(int x, int y, color c) {
    col=c;
    sx=x;sy=y;
    this.x=x; this.y=y;
    xmov=random(-1,1); ymov=random(-1,1);
    movability=1-dist(x,y,width/2.0,height/2.0)/sqrt(pow(width/2.0,2)+pow(height/2.0,2));    //  0..
  }
  
  void update() {
    if (mousePressed) {
      dm=dist(x,y,mouseX,mouseY);
    } else {
      dm=(3*dm+2000000)/4;
    }
    d=pow(1/(5+dm),0.7);
    xmov+=movability*0.7*mxmov*d;
    ymov+=movability*0.7*mymov*d;
    
    xmov+=movability*0.0045*(sx-x);
    ymov+=movability*0.0045*(sy-y);
    
    float damp=0.99;//-getNrOfNeighbours(x,y)*0.002;
    xmov*=damp;
    ymov*=damp;
    
    if (x+xmov<1||x+xmov>width) {xmov*=-0.8;}
    if (y+ymov<1||y+ymov>height) {ymov*=-0.8;}
    
    x+=xmov;y+=ymov;
    
    set(int(x),int(y),col);
  }
  /*
  float getNrOfNeighbours(float xp,float yp) {
    int x=int(xp), y=int(yp);
    return 8;
  }
  */
}



