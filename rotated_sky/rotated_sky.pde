// ROTATED SKY

PImage img1, img2, bgBuf, tempBuf;
float rd;
float deg=135;
boolean finished;

void setup() {
  size(911, 911, P3D); 
  colorMode(RGB, 255.0);
  img1=loadImage("sky1.jpg");  // no text
  img2=loadImage("sky2.jpg");  //
  bgBuf=loadImage("empty911.jpg");
  tempBuf=loadImage("empty911.jpg");
  img1.loadPixels();
  img2.loadPixels();
  bgBuf.loadPixels();
  tempBuf.loadPixels();
  background(0);
  imageMode(DIAMETER);
}

void draw() {
  loadPixels();
  if (rd<radians(deg)) {
    copyImageToBgBuf();
    drawNewImage(rd+=radians(0.1));
    copyImageToTempBuf();
    image(bgBuf, 455.5, 455.5);
    for (int i=0; i<pixels.length; i++) { 
      pixels[i]=lighten(tempBuf.pixels[i], bgBuf.pixels[i]);
    }
    
    updatePixels();
  } else if (!finished) {
    finished=true;
    save("RotatedSky "+(""+(int(10*24*deg/360.0)/10.0))+" hours v03.tga");
  }
}

void copyImageToBgBuf() {
  loadPixels();
  for (int i=0; i<bgBuf.pixels.length; i++) { 
    bgBuf.pixels[i]=pixels[i];
  }
  bgBuf.updatePixels();
}
void copyImageToTempBuf() { 
  loadPixels();
  for (int i=0; i<tempBuf.pixels.length; i++) { 
    tempBuf.pixels[i]=pixels[i];
  }
  tempBuf.updatePixels();
}

void drawNewImage(float _rd) {
  background(0);
  pushMatrix();
  translate(455.5, 455.5); 
  rotate(_rd);
  translate(-455.5, -455.5, 0); 
  image(img1, 455.5, 455.5);
  popMatrix();
}




////////// screening modes:

int ch_red(int c) { 
  return (c>>16&255);
}
int ch_grn(int c) { 
  return (c>>8&255);
}
int ch_blu(int c) { 
  return (c&255);
}

int fade(int a, float p) {
  int rr=int(ch_red(a)*p);
  int gg=int(ch_grn(a)*p);
  int bb=int(ch_blu(a)*p);
  return (rr<<16|gg<<8|bb);
}

int multiply(int a, int b) {
  int rr=(ch_red(a)*ch_red(b))>>8;
  int gg=(ch_grn(a)*ch_grn(b))>>8;
  int bb=(ch_blu(a)*ch_blu(b))>>8;
  return (rr<<16|gg<<8|bb);
}

int screen(int a, int b) {
  int rr=255 - ((255-ch_red(a)) * (255-ch_red(b))>>8);
  int gg=255 - ((255-ch_grn(a)) * (255-ch_grn(b))>>8);
  int bb=255 - ((255-ch_blu(a)) * (255-ch_blu(b))>>8);
  return (rr<<16|gg<<8|bb);
}

int lighten(int a, int b) {
  int rr=ch_red(a)>ch_red(b)?ch_red(a):ch_red(b);
  int gg=ch_grn(a)>ch_grn(b)?ch_grn(a):ch_grn(b);
  int bb=ch_blu(a)>ch_blu(b)?ch_blu(a):ch_blu(b);
  return (rr<<16|gg<<8|bb);
}
