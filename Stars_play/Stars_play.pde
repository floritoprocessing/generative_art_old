float[] x, y, z;
int[] col;
float sf=1.0;
int nrOfStars=250000;
float sideLen;
float mx, my, mz;

//PImage forest;

void setup() {
  size(720,576,P3D);
  initArray();
  sideLen=pow(nrOfStars,1/3.0);
  mx=0; my=0; mz=0;
  //forest=loadImage("forest_720x576.jpg");
}

void draw() {
  background(0);
//  push();
//  translate(0,0,-1000);
//  imageMode(CENTER_DIAMETER);
//  image(forest,width/2.0,height/2.0,720*3,576*3);
//  pop();
  
  updateArray(mx,my,mz-=0.001);
  translate(width/2.0,height/2.0);
  for (int i=0;i<nrOfStars;i++) {
    stroke(255,255,255,col[i]);
    point(x[i]*sf,y[i]*sf,z[i]*sf);
  }
  
  //saveFrame("c:/temp4/Stars_play_1_####.tga");
}

//void keyPressed() {
//  if (key=='q') {sf*=1.01;}
//  if (key=='a') {sf/=1.01;}
//  if (key=='Q') {sf*=1.1;}
//  if (key=='A') {sf/=1.1;}
//}





void initArray() {
  x=new float[nrOfStars]; y=new float[nrOfStars]; z=new float[nrOfStars];
  col=new int[nrOfStars];
  for (int i=0;i<nrOfStars;i++) { x[i]=0;y[i]=0;z[i]=0; }
}

void updateArray(float xo, float yo, float zo) {
  float noiseFac=2;
  float xi,yi,zi;
  for (int i=0;i<nrOfStars;i++) {
//    xi=(i%sideLen)/sideLen;
//    yi=((i/sideLen)%sideLen)/sideLen;
//    zi=((i/(sideLen*sideLen))%sideLen)/sideLen;
    xi=random(1); yi=random(1); zi=random(1);
    if (noise((xi+xo)*noiseFac,(yi+yo)*noiseFac,(zi+zo)*noiseFac)<0.4&&i<nrOfStars-1) {
      x[i]=(xi-0.5)*1000; y[i]=(yi-0.5)*1000; 
      z[i]=-1000+(zi)*1000; col[i]=64+int(zi*64);
    }
  }
}
