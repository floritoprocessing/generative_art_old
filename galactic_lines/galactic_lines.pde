// galactic lines to stars
PImage lines, stars;
int display;
float diag2;

void setup() {
  size(800,800); diag2=dist(0,0,width/2,height/2);
  loadGraphics();
//  createStars(1,20,1.0,0xffffff,0.8);  //(int nrOfStars, float scatter, float distribution, int col, float softness)
  createStars(15,50,4.8,0xccddff,0.2);
  createStars(5,40,4.8,0xffddff,0.2);
  createStars(15,40,4.9,0xdad8ff,0.2);
  display=0;
}

void draw() {
  switch (display) {
    case 0: image(lines,0,0); break;
    case 1: image(stars,0,0); break;
  }
}

void mousePressed() {
  display++; if (display==2) {display=0;}
}

void loadGraphics() {
  lines=loadImage("just_lines_noalias_800_2.jpg");
  stars=loadImage("black_800.jpg");  
}

void createStars(int nrOfStars, float scatter, float distribution, int col, float softness) {
  ellipseMode(RADIUS);
  for (int x=0;x<width;x++) {
    for (int y=0;y<height;y++) {
      if (brightness(lines.pixels[y*width+x])>128) {
        float softCen=1-0.5*dist(x,y,width/2,height/2)/diag2;
        for (int n=0;n<nrOfStars;n++) {
          float r=scatter*pow(random(1),distribution)/2.0, rd=random(TWO_PI);
          r+=(scatter/2.0)*noise(100*x/(float)width,100*y/(float)height);
          float circX=r*cos(rd), circY=r*sin(rd);
          int sx=x+int(circX); if (sx>=width) {sx=width-1;} if (sx<0) {sx=0;}
          int sy=y+int(circY); if (sy>=height) {sy=height-1;} if (sy<0) {sy=0;}
          float softRand=noise(50*x/(float)width,50*y/(float)height);
          softsetOnStars(sx,sy,col,softness*softRand*softCen);
        }
      }
    }
  }
}

void softsetOnStars (int x, int y, int c1, float p1) {
  int c2=stars.get(x,y); float p2=1.0-p1;
  int r = int(p1*(c1>>16&255)+p2*(c2>>16&255));
  int g = int(p1*(c1>>8&255)+p2*(c2>>8&255));
  int b = int(p1*(c1&255)+p2*(c2&255));
  int i=y*width+x;
  stars.pixels[i]=(r<<16|g<<8|b);;
}
