color mainCol[][] = { {0x483526, 0x201718, 0x382720, 0x4A3530,
0x2F221C, 0x261E1B, 0x22130E, 0x35241D,
0x4A3A2D, 0x3F2C2E, 0x352621, 0x573F35,
0x362420, 0x35231F, 0x433228, 0x422F29,
0x4C352D, 0x43302A, 0x372920, 0x1F1919,
 },
{ 0x513124, 0x3E251E, 0x4B2F24, 0x59392E,
0x532E25, 0x61453A, 0x4D2F27, 0x4F322C,
0x69493E, 0x755347, 0x38251F, 0x4F3328,
0x5E433C, 0x704938, 0x5F4137, 0x2F1F12,
0x4E3129, 0x422E27, 0x523128, 0x422A20,
 },
{ 0xBC9C85, 0xCDA987, 0xCAA789, 0xDDBB9F,
0xC8A587, 0xC9A58B, 0xCFB195, 0xDBB699,
0xDCB99B, 0xDAB99A, 0xDDBA9A, 0xD5BA9D,
0xDDB89B, 0xD9BA9D, 0xC8A585, 0xC6A07B,
0xD5B096, 0xCCAA85, 0xCFAB89, 0xCEAA86,
 },
{ 0xB68660, 0xD7A685, 0xCE9872, 0xAD784E,
0xD09C77, 0xD5A47C, 0xDCAA87, 0xBC8C68,
0xC6977D, 0x9A6948, 0xC99371, 0xDAAD8C,
0xC5956F, 0xD09E79, 0xC0946D, 0xD0A47D,
0xDCB896, 0xD2A385, 0xC69874, 0xD9A87F,
 },
{ 0x83472D, 0x945D3E, 0xA06547, 0x965C34,
0x814C2D, 0x94573A, 0xA1674F, 0xAA6C45,
0xAB7958, 0x8C5335, 0x9D6041, 0xA86B4F,
0x874934, 0x9C5D3A, 0x9D5E3B, 0xA87153,
0x8E502B, 0xA25E3B, 0x8C4A2A, 0x8C4D2E,
 },
{ 0x683A2A, 0x623925, 0x6E412A, 0x51291D,
0x5E3523, 0x451F14, 0x673E2A, 0x53291D,
0x74453B, 0x5E3227, 0x5E362A, 0x5B322E,
0x503023, 0x583026, 0x52342C, 0x613729,
0x472219, 0x49291E, 0x3A2218, 0x513124,
 } };

float w,h;  // main width/height
Attractor[] att; int nrOfAtt; // attractor
Lub[] lub; int nrOfLub;       // Lubs
int mainHair[]=new int[3]; 
int nrOfColorSamples=20;
PImage canvas;
boolean restart;

void setup() {
  size(600,450,P3D); 
  // load image:
  //canvas=loadImage("paper600x450.jpg");
  // screensetup
  restart=false;
  initAll();
}

void initAll() {
  background(255);
  w=width; h=height;
  // setup canvas
  canvas=loadImage("paper600x450.jpg");
  canvas.loadPixels();
  //image(canvas,0,0);
  // hair setup; choose 3 from 6
  for (int i=0;i<3;i++) {mainHair[i]=(int)random(6);}
  // attractor setup
  float cx=random(.1,.9);
  float cy=random(.1,.9);
  nrOfAtt=8; 
  att=new Attractor[nrOfAtt]; 
  for (int i=0;i<nrOfAtt;i++) {
    att[i]=new Attractor(cx,cy);
  }
  // lub setup
  nrOfLub=1000; lub=new Lub[nrOfLub]; for (int i=0;i<nrOfLub;i++) {lub[i]=new Lub(canvas,att);}
  restart=false;
}

void mousePressed() {
  restart=true;
}

void draw() {
  if (restart) {initAll();}
  for (int f=0;f<10;f++) {
    for (int i=0;i<nrOfAtt;i++) {att[i].update();}
    for (int i=0;i<nrOfLub;i++) {lub[i].update();}
  }
  canvas.updatePixels();
  
  background(0);
  image(canvas,0,0);
  
  ellipseMode(RADIUS);
  noFill();
  for (int i=0;i<nrOfAtt;i++) {
    //stroke(att[i].r,att[i].g,att[i].b);
    //ellipse(att[i].x*width,att[i].y*height,5,5);
  }
  
  //PImage img = new PImage(width,height);
  //img.copy(this.g,0,0,width,height,0,0,width,height);
  //image(img,0,0);
}

class Attractor {
  float sx,sy,x,y,d;
  float mrd,mrd_m, mr, mr_m;
  color c; float r,g,b;
  Attractor(float cx, float cy) {
    sx=cx; sy=cy;
    mrd=random(TWO_PI); mrd_m=random(0.005,0.05); if (random(1)<0.5) {mrd_m*=-1;} 
    mr=random(0.5); mr_m=random(-0.001,0.001);
    x=sx+mr*cos(mrd); y=sy+mr*sin(mrd); d=0.01; 
    c=color(random(256),random(256),random(256)); r=red(c); g=green(c); b=blue(c);
  }
  void update() {
    mrd_m*=0.9995; mrd+=mrd_m;
    mr_m*=0.9995; mr+=mr_m;
    x=sx+mr*cos(mrd); y=sy+mr*sin(mrd);
  }
}

class Lub {
  float x,y,xm,ym,averagespeed,moverandom; color c; float r,g,b;
  boolean fadeout;
  Attractor[] at;
  int type; color drawcolor; float colorFade;
  float damp=0.99;
  Lub(PImage canvas, Attractor[] in_att) {
    x=random(1); y=random(1); xm=random(-0.005,0.005); ym=random(-0.005,0.005);
    type=(int)random(3); 
    moverandom=0.00001; colorFade=0.05; averagespeed=1000; fadeout=false;
    drawcolor=mainCol[mainHair[type]][(int)random(nrOfColorSamples)];
    c=color(random(256),random(256),random(256)); r=red(c); g=green(c); b=blue(c);
    at=in_att;
  }
  
  void update() {
    for (int i=0;i<nrOfAtt;i++) {
      if (type==0) {
        float rfac=abs((r-at[i].r)/255.0);
        xm+=0.0001*rfac*(att[i].x-x);
        ym+=0.0001*rfac*(att[i].y-y);
      } else if (type==1) {
        float gfac=abs((g-at[i].g)/255.0);
        xm+=0.00001*gfac*(att[i].x-x);
        ym+=0.00001*gfac*(att[i].y-y);
      } else {
        float bfac=abs((b-at[i].b)/255.0);
        xm+=0.00001*bfac*(att[i].x-x);
        ym+=0.00001*bfac*(att[i].y-y);
      }
      xm+=random(-moverandom,moverandom);
      ym+=random(-moverandom,moverandom);
    }
    xm*=damp; ym*=damp;
    // fade out when start curling:
    averagespeed=(50*averagespeed+sqrt(xm*xm+ym*ym))/51.0;
    if (averagespeed<0.0001) {fadeout=true;}
    if (fadeout) {colorFade*=0.9;}

    x+=xm; y+=ym; 
    if (x<0||x>1) {xm*=-1;}
    if (y<0||y>1) {ym*=-1;}
    int sx=int(x*w), sy=int(y*h);
    canvas.set(sx,sy,colorFade(drawcolor,canvas.get(sx,sy),colorFade));
  }
}

color colorFade(color a, color b, float perc1) {
  float perc2=1.0-perc1;
  float rr=perc1*red(a)+perc2*red(b), gg=perc1*green(a)+perc2*green(b), bb=perc1*blue(a)+perc2*blue(b);
  return color(rr,gg,bb);
}
