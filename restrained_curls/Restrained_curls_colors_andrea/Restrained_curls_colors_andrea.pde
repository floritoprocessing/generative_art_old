float w,h;  // main width/height
Attractor[] att; int nrOfAtt; // attractor
Lub[] lub; int nrOfLub;       // Lubs
boolean restart;

color[] mainColor = { 0x4DF7F7, 0xFFA633, 0xFFFFFF, 0xE0E0E0};

void setup() {
  size(600,450,P3D); 
  restart=false;
  initAll();
}

void initAll() {
  background(mainColor[3]);
  
  w=width; h=height;
  // attractor setup
  float cx,cy; cx=random(.1,.9);cy=random(.1,.9);
  nrOfAtt=8; att=new Attractor[nrOfAtt]; for (int i=0;i<nrOfAtt;i++) {att[i]=new Attractor(cx,cy);}
  // lub setup
  nrOfLub=1000; lub=new Lub[nrOfLub]; for (int i=0;i<nrOfLub;i++) {lub[i]=new Lub(att);}
  restart=false;
}

void keyPressed() {
  if (key==' ') { restart=true; stopIt=false;}
  if (key=='1') { selectColor=0; initColorPicker(hue(mainColor[0]),saturation(mainColor[0]),brightness(mainColor[0])); stopIt=true; }
  if (key=='2') { selectColor=1; initColorPicker(hue(mainColor[1]),saturation(mainColor[1]),brightness(mainColor[1])); stopIt=true; }
  if (key=='3') { selectColor=2; initColorPicker(hue(mainColor[2]),saturation(mainColor[2]),brightness(mainColor[2])); stopIt=true; }
  if (key=='4') { selectColor=3; initColorPicker(hue(mainColor[3]),saturation(mainColor[3]),brightness(mainColor[3])); stopIt=true; }
}

void draw() {
  if (stopIt) {
    updateColorPicker();
  } else {
    if (restart) {initAll();}
    for (int i=0;i<nrOfAtt;i++) {att[i].update();}
    for (int i=0;i<nrOfLub;i++) {lub[i].update();}
  }
}

boolean stopIt=false;
int pickW=256, pickH=256;
int selectColor;

void initColorPicker(float h, float s, float b) {
  background(255);
  colorMode(HSB,255);
  for (int x=0;x<pickW;x++) { for (int y=0;y<pickH;y++) {
    color c=color(h,x,pickH-y);
    set(100+x,100+y,c);
  } }
  noFill(); stroke(128); rectMode(CORNERS); rect(100,100,100+pickW,100+pickH);
  
  noFill(); stroke(128);
  ellipseMode(DIAMETER);
  ellipse(100+s,100+pickH-b,30,30);

  for (int y=0;y<pickH;y++) {
    color c=color(y,255,255);
    for (int x=0;x<=20;x++) {
      set(100+20+pickW+x,100+pickH-y,c);
    }
  }
  noFill(); stroke(128);
  rectMode(DIAMETER); rect(100+20+pickW+10,100+pickH-h,24,10);
  
  for (int x=0;x<50;x++) { for (int y=0;y<50;y++) {
    color c=color(h,s,b);
    set(100+20+pickW+40+x,100+128+y,c);
  } }
  noFill(); stroke(128);
  noFill(); stroke(128); rectMode(CORNERS); rect(100+20+40+pickW,100+128,100+20+40+pickW+50,100+128+50);
 
  colorMode(RGB,255);
}

void updateColorPicker() {
  // HUE SELECT:
  if (mouseX>=100+20+pickW&&mouseX<=100+20+pickW+20) {
    if (mouseY>=100&&mouseY<=100+pickH) {
      if (mousePressed) {
        colorMode(HSB,255);
        float h=100+pickH-mouseY;
        float s=saturation(mainColor[selectColor]);
        float b=brightness(mainColor[selectColor]);
        mainColor[selectColor]=color(h,s,b);
        initColorPicker(h,s,b);
      }
    }
  }
  if (mouseX>=100&&mouseX<=100+pickW&&mouseY>=100&&mouseY<=100+pickH) {
    if (mousePressed) {
      colorMode(HSB,255);
      float h=hue(mainColor[selectColor]);
      float s=mouseX-100;
      float b=100+pickH-mouseY;
      mainColor[selectColor]=color(h,s,b);
      initColorPicker(h,s,b);
    }
  }
}


class Attractor {
  float sx,sy,x,y,d;
  float mrd,mrd_m, mr, mr_m;
  color c; float r,g,b;
  Attractor(float cx, float cy) {
    sx=cx; sy=cy;
    mrd=random(TWO_PI); mrd_m=random(0.005,0.05); if (random(1)<0.5) {mrd_m*=-1;} 
    mr=random(0.5); mr_m=random(-0.001,0.001);
    x=sx+mr*cos(mrd); y=sy+mr*sin(mrd); d=0.01; 
    c=color(random(256),random(256),random(256)); r=red(c); g=green(c); b=blue(c);
  }
  void update() {
    mrd_m*=0.9995; mrd+=mrd_m;
    mr_m*=0.9995; mr+=mr_m;
    x=sx+mr*cos(mrd); y=sy+mr*sin(mrd);
  }
}

class Lub {
  float x,y,xm,ym,averagespeed,moverandom; color c; float r,g,b;
  boolean fadeout;
  Attractor[] at;
  int type; color drawcolor; float colorFade;
  float damp=0.99;
  Lub(Attractor[] in_att) {
    x=random(1); y=random(1); xm=random(-0.005,0.005); ym=random(-0.005,0.005);
    type=(int)random(3); 
    moverandom=0.00001; colorFade=0.05; averagespeed=1000; fadeout=false;
    drawcolor=mainColor[type];
    c=color(random(256),random(256),random(256)); r=red(c); g=green(c); b=blue(c);
    at=in_att;
  }
  void update() {
    for (int i=0;i<nrOfAtt;i++) {
      if (type==0) {
        float rfac=abs((r-at[i].r)/255.0);
        xm+=0.0001*rfac*(att[i].x-x);
        ym+=0.0001*rfac*(att[i].y-y);
      } else if (type==1) {
        float gfac=abs((g-at[i].g)/255.0);
        xm+=0.00001*gfac*(att[i].x-x);
        ym+=0.00001*gfac*(att[i].y-y);
      } else {
        float bfac=abs((b-at[i].b)/255.0);
        xm+=0.00001*bfac*(att[i].x-x);
        ym+=0.00001*bfac*(att[i].y-y);
      }
      xm+=random(-moverandom,moverandom);
      ym+=random(-moverandom,moverandom);
    }
    xm*=damp; ym*=damp;
    // fade out when start curling:
    averagespeed=(50*averagespeed+sqrt(xm*xm+ym*ym))/51.0;
    if (averagespeed<0.0001) {fadeout=true;}
    if (fadeout) {colorFade*=0.9;}

    x+=xm; y+=ym; 
    if (x<0||x>1) {xm*=-1;}
    if (y<0||y>1) {ym*=-1;}
    int sx=int(x*w), sy=int(y*h);
    set(sx,sy,colorFade(drawcolor,get(sx,sy),colorFade));
  }
}

color colorFade(color a, color b, float perc1) {
  float perc2=1.0-perc1;
  float rr=perc1*red(a)+perc2*red(b), gg=perc1*green(a)+perc2*green(b), bb=perc1*blue(a)+perc2*blue(b);
  return color(rr,gg,bb);
}
