color[] mainColor=new color[12];
color actualColor; 
boolean colorPicking; 
int colorNum;

// drawings vars
int maxDrawings, actualDrawing; Drawing[] drawing;



void setup() {
  size(600,300); background(255); //framerate(100);
  maxDrawings=5000; drawing=new Drawing[maxDrawings]; actualDrawing=0;
  initColorPicker();
}

void draw() {
  drawHuman();
  activateNewDrawer();
  drawComputer();
  drawColorPicker(colorNum);
}




void drawHuman() {
  if (mousePressed&&!colorPicking) {
    int mx=mouseX, my=mouseY;
    drawing[actualDrawing].addPoint(mx,my);
    softline(pmouseX,pmouseY,mx,my,actualColor,0.1);
  }
}
  
  
void activateNewDrawer() {
  if (actualDrawing>0&&random(1)<0.08) {
    int i=(int)random(actualDrawing);
    if (!drawing[i].active) {
      drawing[i].activateNew();
    }
  }
}


void drawComputer() {  
  if (actualDrawing>0) {
    for (int i=0;i<actualDrawing;i++) {
      if (drawing[i].active) {drawing[i].drawNextPoint();}
    }
  }
}


void mousePressed() {
  int mx=mouseX, my=mouseY;
  if (mx>=5&&mx<=125&&my>=5&&my<=15) { colorPicking=true; } else {
    colorPicking=false;
    if (actualDrawing<maxDrawings) {
      drawing[actualDrawing]=new Drawing(actualColor);
    }
  }
}
void mouseReleased() {
  if (!colorPicking) {
    drawing[actualDrawing].getCenter(); actualDrawing++;
  } else {
    // color picking:
    int mx=mouseX, my=mouseY;
    if (mx>=5&&mx<125&&my>=5&&my<=15) {
      colorNum=(int)floor((mx-5)/10.0);
      actualColor=mainColor[colorNum];
    }
  }
}



void keyPressed() {
//  if (key=='c'|key=='C') {initColorPicker();}
}




class Drawing {
  int maxFrames=2000, topFrame=0, i=0;
  int[] x=new int[maxFrames], y=new int[maxFrames]; int av_x=0, av_y=0;
  color c;
  
  Drawing(color in_c) {
    c=in_c;
  }
  
  // setting:
  void addPoint(int in_x, int in_y) {
    if (i<maxFrames) {
      x[i]=in_x; y[i]=in_y;
      av_x+=in_x; av_y+=in_y;
      i++; topFrame++;
    }
  }
  void getCenter() {
    av_x/=(topFrame-1);
    av_y/=(topFrame-1);
    for (i=0;i<topFrame;i++) {
      x[i]-=av_x; y[i]-=av_y;
    }
  }
  
  // drawing:
  int drawIndex=1;
  boolean active=false;
  int xoff, yoff;        // positional variation
  float rot, COS, SIN;   // rotation variation
  float scl, xscl, yscl; // scale variation
  color coff;            // color variation
  void activateNew() {
    active=true; drawIndex=1;
    xoff=av_x+(int)random(width/6); yoff=av_y+(int)random(height/6);
    rot=random(TWO_PI); COS=cos(rot); SIN=sin(rot);
    scl=pow(2,random(-2,1)); xscl=random(0.8,1.2)*scl; yscl=random(0.8,1.2)*scl;
    int rr=c>>16&255, gg=c>>8&255, bb=c&255;
    int q=32; int rro,ggo,bbo;
    do {rro=(int)random(-q,q);} while (rr+rro<0||rr+rro>255);
    do {ggo=(int)random(-q,q);} while (gg+ggo<0||gg+ggo>255);
    do {bbo=(int)random(-q,q);} while (bb+bbo<0||bb+bbo>255);
    coff=rr<<16|gg<<8|bb;
  }
  
  void drawNextPoint() {
    if (drawIndex<topFrame) {
      active=true;
      float x1=xscl*x[drawIndex-1], x2=xscl*x[drawIndex], y1=yscl*y[drawIndex-1], y2=yscl*y[drawIndex];
      float nx1=x1*COS+y1*SIN, ny1=y1*COS-x1*SIN;
      float nx2=x2*COS+y2*SIN, ny2=y2*COS-x2*SIN;
      nx1+=xoff; ny1+=yoff; nx2+=xoff; ny2+=yoff;
      softline(int(nx1),int(ny1),int(nx2),int(ny2),coff,0.1);
      drawIndex++;
    } else {
      active=false;
    }
  }
}


// COLOR PICKER FUNCTIONS:  ---------------------------------------------------
void initColorPicker() {
  for (int i=0;i<12;i++) {
    mainColor[i]=color((int)random(255),(int)random(255),(int)random(255));
  }
  actualColor=mainColor[0]; colorNum=0;
  colorPicking=false;
}
void drawColorPicker(int nr) {
  for (int i=0;i<12;i++) {
    stroke(0); fill(mainColor[i]); rectMode(DIAMETER);
    rect(10+i*10,10,10,10);
  }
  stroke(0); fill(mainColor[nr]); rect(140,10,10,10);
}







// PIXEL SETTING FUNCTIONS: ----------------------------------------------------
void softline(int x1, int y1, int x2, int y2, int c1, float p1) {
  float d=dist(x1,y1,x2,y2);
  int dx=x2-x1, dy=y2-y1;
  if (d!=0) {
    for (int i=0;i<d;i++) {
      float p=i/d;
      softset(int(x1+p*dx),int(y1+p*dy),c1,p1);
    }
  }
}

void softset(int x, int y, int c1, float p1) {
  if (x>=0&&x<width&&y>=0&&y<height) {
    float p2=1.0-p1;
    int c2=get(x,y);
    int r=int(p1*(c1>>16&255)+p2*(c2>>16&255));
    int g=int(p1*(c1>>8&255)+p2*(c2>>8&255));
    int b=int(p1*(c1&255)+p2*(c2&255));
    set(x,y,(255<<24|r<<16|g<<8|b));
  }
}
