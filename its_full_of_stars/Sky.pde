class Sky {
  
  float RADIUS=5000;
  float USTEP=0.05, VSTEP=0.1;
  
  PImage img;
  
  
  
  // start Sky constructor
  Sky(String bitmapName) {
    img=loadImage(bitmapName);
    textureMode(NORMAL);
  }
  // end Sky constructor
  
  
  
  // start setDivision(int numberOfDivisions)
  // sets detail level of sphere
  void setDivisions(int d) {
    USTEP=0.5/(float)d;
    VSTEP=1.0/(float)d;
  }
  // end setDivision
  
  
  
  // start show();
  void show() {
    fill(0,0,0);
    noStroke();
    for (float baseU=0;baseU<1.0;baseU+=USTEP) {
      for (float baseV=0;baseV<1.0;baseV+=VSTEP) {
        beginShape();
        for (int i=0;i<4;i++) {
          texture(img);
          float offV=(i<2?0:VSTEP);
          float offU=((i==0||i==3)?0:USTEP);
          float u=baseU+offU;
          float v=baseV+offV;
          float lon=(u<1.0?u:0.0)*TWO_PI; // to avoid strange thing on edge
          float lat=-PI/2.0 + v*PI;
          Vec dot=new Vec(0,0,RADIUS);
          dot.rotX(lat);
          dot.rotY(lon);
          vecTextureVertex(dot,u,v);
        }
        endShape();       
      }
    }
  }
  // end show()
  
}
