class Camera {
  float LIM_XROT=0.97*PI/4.0;
  float MOUSE_SPEED=0.5;
  float FOV=60;
  
  Vec center=new Vec(0,0,0.0001);
  Vec pointVec=new Vec(0,0,-1000);
  Vec upaxis=new Vec(0,1,0);
  float xRot=0, yRot=0;
   
  Camera() {
    setFOV(FOV);
  }
  
  void setPosition(Vec v) {
    center.setVec(v);
  }
  
  void setDirection(Vec v) {
    pointVec.setVec(v);
  }
  
  void setFOV(float fovDeg) {
    FOV=fovDeg;
    float fov=radians(FOV);
    float cameraZ = (height/2.0) / tan(PI * fov / 360.0); 
    perspective(fov, float(width)/float(height), cameraZ/10.0, cameraZ*10.0); 
  }
  
  void makeFromMouse() {
    if (mousePressed) {
      yRot-=MOUSE_SPEED*TWO_PI*(pmouseX-mouseX)/(float)width;
      if (yRot>=TWO_PI) {yRot-=TWO_PI;}
      if (yRot<0) {yRot+=TWO_PI;}
      xRot-=MOUSE_SPEED*PI*(pmouseY-mouseY)/(float)height;
      if (xRot>LIM_XROT) {xRot=LIM_XROT;}
      if (xRot<-LIM_XROT) {xRot=-LIM_XROT;}
    }
    pointVec=new Vec(0,0,-1000);
    pointVec.rotX(xRot);
    pointVec.rotY(yRot);
  }
  
  void make() {  
    vecCamera(center,vecAdd(center,pointVec),upaxis);
  }
  
  
}
