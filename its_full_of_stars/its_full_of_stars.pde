Sky sky;
Camera cam;
ScreenCapture capRed, capGreen;

Vec[] starPos=new Vec[10000];

Vec leftCamPos=new Vec(-10,0,0.00001);
Vec rightCamPos=new Vec(10,0,0.00001);
Vec camDir=new Vec(0,0,-1000);

float LIM_XROT=0.97*PI/4.0;
float MOUSE_SPEED=0.5;

float xRot=0, yRot=0;

void setup() {
  size(640,480,P3D);
  cam=new Camera();
  cam.setFOV(45);
  sky=new Sky("StarsMap_2500x1250.jpg");
  sky.setDivisions(20);
  capRed=new ScreenCapture();
  capGreen=new ScreenCapture();
  makeAFewStars();
}

void draw() {

  if (mousePressed) {
    yRot-=MOUSE_SPEED*TWO_PI*(pmouseX-mouseX)/(float)width;
    if (yRot>=TWO_PI) {yRot-=TWO_PI;}
    if (yRot<0) {yRot+=TWO_PI;}
    xRot-=MOUSE_SPEED*PI*(pmouseY-mouseY)/(float)height;
    if (xRot>LIM_XROT) {xRot=LIM_XROT;}
    if (xRot<-LIM_XROT) {xRot=-LIM_XROT;}
  }
  Vec newLeftCamPos=vecRotY(leftCamPos,yRot);
  Vec newRightCamPos=vecRotY(rightCamPos,yRot);
  Vec newCamDir=vecRotX(camDir,xRot);
  newCamDir.rotY(yRot);

  background(0,0,0);
  cam.setPosition(newLeftCamPos);
  cam.setDirection(newCamDir);
  cam.make();
  sky.show();
  drawBox();
  drawAFewStars();
  capRed.captureBrightness();

  background(0,0,0);
  cam.setPosition(newRightCamPos);
  cam.setDirection(newCamDir);
  cam.make();
  sky.show();
  drawBox();
  drawAFewStars();
  capGreen.captureBrightness();
      
  background(0,0,0);
  drawScreenBrightnessRedGreen(capRed,capGreen);
//  capRed.drawBrightnessRed();
}

void drawBox() {
  noFill();
  stroke(255,255,255);
  pushMatrix();
  translate(0,0,-200);
  box(50);
  popMatrix();
}



void makeAFewStars() {
  for (int i=0;i<starPos.length;i++) {
    starPos[i]=new Vec(pmRandom(100,4000),pmRandom(100,4000),pmRandom(100,4000));
  }
}

void drawAFewStars() {
  stroke(255,255,255);
  for (int i=0;i<starPos.length;i++) {
    vecPoint(starPos[i]);
  }
}

float pmRandom(float mi, float ma) {
  return (random(1)<0.5?random(mi,ma):random(-ma,-mi));
}
