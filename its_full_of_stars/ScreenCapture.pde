class ScreenCapture {
  int wid, hei;
  color[] pix;
  float[] pixBright;
  
  ScreenCapture() {
    wid=width;
    hei=height;
    pix=new color[wid*hei];
    pixBright=new float[wid*hei];
  }
  
  void capture() {
    colorMode(RGB,255);
    for (int x=0;x<wid;x++) {
      for (int y=0;y<hei;y++) {
        pix[y*wid+x]=get(x,y);
      }
    }
  }
  
  void captureBrightness() {
    colorMode(RGB,255);
    for (int x=0;x<wid;x++) {
      for (int y=0;y<hei;y++) {
        pixBright[y*wid+x]=brightness(get(x,y));
      }
    }
  }
  
  void drawBrightnessRed() {
    colorMode(RGB,255);
    for (int x=0;x<wid;x++) {
      for (int y=0;y<hei;y++) {
        set(x,y,color(pixBright[y*wid+x],0,0));
      }
    }
  }
  
}

void drawScreenBrightnessRedGreen(ScreenCapture r, ScreenCapture g) {
  int wid=r.wid;
  int hei=r.hei;
  for (int x=0;x<wid;x++) {
    for (int y=0;y<hei;y++) {
      set(x,y,color(r.pixBright[y*wid+x],g.pixBright[y*wid+x],0));
    }
  }
}
