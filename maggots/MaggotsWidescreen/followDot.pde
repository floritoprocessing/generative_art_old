class followDot {
  float x=0.0,y=0.0;
  float agA, agB; //easing
  followDot(float x, float y, float agil) {
    this.x=x; this.y=y;
    agA=agil; agB=agA+1.0;
  }
  void update(float tx,float ty) {
    x=(agA*x+tx)/agB;
    y=(agA*y+ty)/agB;
    pointScreen(x,y,false,0);
  }
}
