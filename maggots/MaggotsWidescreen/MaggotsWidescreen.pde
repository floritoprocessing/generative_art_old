import processing.video.*;

int nrOfWorms=500;
Wormtype[] worm=new Wormtype[nrOfWorms+1];

//MovieMaker mov;
boolean saveOutput = false;

//color[][] traces=new color[1025][577];
PImage traces;

float bgcR=238, bgcG=229, bgcB=191;
color backGroundColor;

void setup() {
  size(1024,576,P3D);
  traces = new PImage(width,height);
  
  if (saveOutput) {
    //mov = new MovieMaker(this, width, height, "MaggotsWidescreen00.mov", 25, MovieMaker.JPEG, MovieMaker.HIGH);
  }

  backGroundColor=color(bgcR,bgcG,bgcB);

  noFill();
  frameRate(100);
  ellipseMode(RADIUS);
  for (int i=0;i<traces.pixels.length;i++) {
    traces.pixels[i] = backGroundColor;
  }
  traces.updatePixels();
  
  for (int i=1;i<=nrOfWorms;i++) {
    worm[i]=new Wormtype(i);
  }
  background(backGroundColor);
  //registerDispose(this);
}


void dispose() {
  if (saveOutput) {
    //mov.finish();
  }
}

void draw() {
  for (int f=0;f<25;f++) {
    //background(backGroundColor);
    
    // draw worm traces:
//    for (int x=1;x<=width;x++) { 
//      for (int y=1;y<=height;y++) {
//        set(x,y,traces[x][y]);
//      } 
//    }

    image(traces,0,0);
    
    // draw worms:
    for (int i=1;i<=nrOfWorms;i++) {
      worm[i].update();
    }
    traces.updatePixels();

    if (saveOutput) {
      //mov.addFrame();
    }

  }
}






void pointScreen(float x,float y,boolean trace, color wormColor) {
  color tCol;
  while (x<1) {
    x+=width;
  } 
  while (x>width) {
    x-=width;
  }
  while (y<1) {
    y+=height;
  } 
  while (y>height) {
    y-=height;
  }
  point(x,y);

  if (trace) {
    int ix = (int)constrain(x,0,width-1);
    int iy = (int)constrain(y,0,height-1);
    int i = iy*width+ix;
    
    //tCol=traces[int(x)][int(y)];
    tCol = traces.pixels[i];
    
    tCol=color((15*red(tCol)+red(wormColor))/16.0,(15*green(tCol)+green(wormColor))/16.0,(15*blue(tCol)+blue(wormColor))/16.0);
    //traces[int(x)][int(y)]=tCol;
    traces.pixels[i]=tCol;
  }
}

float warpedX(float input) {
  while (input<1) {
    input+=width;
  } 
  while (input>width) {
    input-=width;
  }
  return input;
}
float warpedY(float input) {
  while (input<1) {
    input+=height;
  } 
  while (input>height) {
    input-=height;
  }
  return input;
}
