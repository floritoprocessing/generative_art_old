// galactic lines to stars
PImage lines;
boolean anotherLevel;
float maxR;
int lev=0;

void setup() {
  size(800,800,JAVA2D); maxR=dist(0,0,400,400);
  loadGraphics();
  background(0); smooth();
  colorMode(RGB,255);
  anotherLevel=false;
}

void draw() {
  if (lev==0) image(lines,0,0);
  if (anotherLevel) {
    if (lev==0) background(0);
    lev++;
    anotherLevel=false;
    createAllStars();
  }
}

void mousePressed() {
  anotherLevel=true;
}

void loadGraphics() {
  lines=loadImage("just_lines_noalias_800_2.jpg");
}

void createAllStars() {
  createStars(20000,      1.8,        0.4,      40,  0xddddff,  0x111100,   0x2f);
  createStars(20000,      1.5,        0.3,      40,  0xeeeeff,  0x111100,   0xef);
  createStars(90000,      1.5,        0.3,      12,  0xffddff,  0x112200,   0x6f);
  createStars(120000,       1,        0.9,      20,  0xaaaaff,  0x111100,   0x80);
  createStars(120000,       1,        0.9,      40,  0xa8a8ff,  0x222211,   0x80);
  createStars(120000,       1,        0.9,      20,  0xffffff,  0x222211,   0x80);
  createStars(500000,       1,        0.3,      50,  0xffffff,  0x222211,   0x80);
}

void createStars(int nrOfTimes, float radius, float radvar, float locvar, int col, int colvar, int tt) {
  ellipseMode(RADIUS);
  for (int i=0;i<nrOfTimes;i++) {
    int rr=constrain((col>>16&255)+int(random(-(colvar>>16&255),colvar>>16&255)),0,255);
    int gg=constrain((col>>8&255)+int(random(-(colvar>>8&255),colvar>>8&255)),0,255);
    int bb=constrain((col&255)+int(random(-(colvar&255),colvar&255)),0,255);
    noStroke(); 
    fill(rr,gg,bb,tt);
    int x=(int)random(width), y=(int)random(height);
    if (brightness(lines.pixels[y*width+x])>128) {
      float r=radius+random(-radvar,radvar);
      float locr=locvar*noise(100*x/(float)width,100*y/(float)height), locrd=random(TWO_PI);
      ellipse(x+locr*cos(locrd),y+locr*sin(locrd),r,r);
    }
    
  }
}
