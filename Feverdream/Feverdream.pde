Sketcher[] sk; int nrOfSk=50000, actualSk;
int drawColor=0;
int drawPhase;
float[] character;
boolean paused;
int timeOnMouseDown=0;

// variables used for calculating average center
float br, avx, avy, div, tot_avx, tot_avy;
int tot_x=0,tot_y=0;

void setup() {
  size(600,400);
  background(0); 
  //framerate(50);
  
  character=new float[10];
  character[0]=random(0.02,0.5); // how intense
  character[1]=random(0.0001,0.001);  // how often new Sketcher
  character[2]=character[1]+random(0.005,0.01);    // how often morph Sketcher from any existing one
  character[3]=random(0.005,0.5);  // how often morph of color each frame
  character[4]=random(0.1*PI,PI);  // general roundness of sketchers
  
  sk=new Sketcher[nrOfSk];
  initSketchers();
  drawPhase=1;
  paused=false;
}

void initSketchers() {
  for (int i=0;i<nrOfSk;i++) { sk[i]=new Sketcher(width,height); sk[i].init(); }
  actualSk=0;
}

void draw() { if (!paused) {
  if (drawPhase==1) {
    // DRAW SKETCHERS -------------------------------------------------------
    // variate color:
    if (random(1)<character[3]) { drawColor=varColor(drawColor); }
    boolean mp=mousePressed;  
    // 100 times each frame:
    for (int ii=0;ii<100;ii++) {
  
      if (random(1)<0.001) {
        if (mp) {
          drawColor=mixColor(drawColor,getUnderMouseColor(),0.8);
        } else {
          drawColor=0xffffff-drawColor;
        }
      }
      
      if (actualSk<nrOfSk-1) {
        if (random(1)<character[0]) {
          actualSk++; 
          float rand=random(1);
          if (rand<character[1]) {        // 0.1%
            sk[actualSk].init();
          } else if (rand<character[2]) { // 0.9%
            sk[actualSk].morph(sk[(int)random(actualSk)]);
          } else {                // 99%
            sk[actualSk].morph(sk[actualSk-1]);
          }        
        }
        sk[actualSk].makeLine(drawColor);
      } else {drawPhase=2;}
    }
    println(actualSk);
  } else { 
    initSketchers();
    drawPhase=1;
  }
  
} }

void keyPressed() {
  paused=!paused;
  println("Paused: "+paused);
}

void mousePressed() {
  if (millis()-timeOnMouseDown<450) {
    // doubleclick!
    initSketchers();
  }
  timeOnMouseDown=millis();
}

class Sketcher {
  int w,h;    //max width/height;
  Vec cen=new Vec(), rad=new Vec();
  float rd1,rd2,rdr;
  
  Sketcher(int limX, int limY) {
    w=limX; h=limY;
  }
  
  void init() {
     cen.init(random(w),random(h));
     rad.init(random(w/64.0,w/8.0),random(h/64.0,h/8.0));
     rd1=random(TWO_PI); rd2=rd1+random(PI/16.0,PI/16.0+character[4]); rdr=abs(rd1-rd2);
  }
  
  void morph(Sketcher isk) {
    float rand=random(1);
    float cxoff=0,cyoff=0,rxoff=0,ryoff=0,rd1off=0,rd2off=0;
    
    if (rand<.2) {
      cxoff=random(-5,5); cyoff=random(-5,5);
    } else if (rand<.6) {
      rxoff=random(-w/128.0,w/128.0); ryoff=random(-h/128.0,h/128.0);
    } else {
      rd1off=random(-PI/10.0,PI/10.0); rd2off=random(PI/16.0,PI);
    }
    
    cen.init(isk.cen.x+cxoff,isk.cen.y+cyoff);
    rad.init(isk.rad.x+rxoff,isk.rad.y+ryoff);
    rd1=isk.rd1+rd1off; rd2=rd1+rd2off; rdr=abs(rd1-rd2);
  }
  
  void makeLine(int c) {
    float trd1=rd1+random(-rdr/4.0,rdr/4.0);
    float trd2=rd2+random(-rdr/4.0,rdr/4.0);
    float tcx=cen.x+random(-w/150.0,w/150.0), tcy=cen.y+random(-h/150.0,h/150.0);
    for (float rd=trd1;rd<=trd2;rd+=TWO_PI/720.0) {
      float sx=tcx+rad.x*cos(rd), sy=tcy+rad.y*sin(rd);
      softset(int(sx),int(sy),c,0.01);
    }
  }
}


class Vec {
  float x,y;
  Vec() {}
  void init(float ix, float iy) {x=ix;y=iy;}
}

void softset(int x, int y, int c1, float p1) {
  float p2=1.0-p1;
  int c2=get(x,y);
  int r=int(p1*(c1>>16&255)+p2*(c2>>16&255));
  int g=int(p1*(c1>>8&255)+p2*(c2>>8&255));
  int b=int(p1*(c1&255)+p2*(c2&255));
  set(x,y,(255<<24|r<<16|g<<8|b));
}

int varColor(int c) {
  int q=2;
  int rr=constrain((c>>16&255)+(int)random(-q,q),0,255);
  int gg=constrain((c>>8&255)+(int)random(-q,q),0,255);
  int bb=constrain((c&255)+(int)random(-q,q),0,255);
  return (rr<<16|gg<<8|bb);
}

float bright(int c) {
  // gets brightness from int color. Result: 0..1
  float b=0;
  b+=(c>>16&255)/255.0;
  b+=(c>>8&255)/255.0;
  b+=(c&255)/255.0;
  b/=3.0;
  return b;
}

int getUnderMouseColor() {
  int o;
  color c=get(mouseX,mouseY);
  //o=int(red(c))<<16|int(green(c))<<8|int(blue(c));
  return c;
}

int mixColor(int c1, int c2, float p1) {
  float p2=1.0-p1;
  int rr=int(constrain((p1*(c1>>16&255)+p2*(c2>>16&255)),0,255));
  int gg=int(constrain((p1*(c1>>8&255)+p2*(c2>>8&255)),0,255));
  int bb=int(constrain((p1*(c1&255)+p2*(c2&255)),0,255));
  return (rr<<16|gg<<8|bb);
}
