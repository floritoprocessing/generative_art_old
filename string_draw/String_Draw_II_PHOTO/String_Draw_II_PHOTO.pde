color[] mainColors;

int nrOfAttractors=12;  // was 12
Attractor[] atr = new Attractor[nrOfAttractors+1];
float[] at_xmov=new float[nrOfAttractors+1];
float[] at_ymov=new float[nrOfAttractors+1];

int nrOfTimesToDraw=100;  // per frame
//int nrOfFramesToDraw=40000; // for 5600x2800
int nrOfFramesToDraw=12000; // for 800x400
int f;

float WID=800, HEI=400;
float mainScale=8;

String uniqueName="c:/String Draw "+String(nrOfFramesToDraw)+" [ ";
int startTime;

void setup() {
  // orig size is 800x400
  //size(5600,2800);
  size(800,400);
  
  mainColors=new color[4];
  mainColors[0]=color(128,124,32);  //0x807C20, 0xCCD67A,
  mainColors[1]=color(255,200,32);  //0xFFC820, 0x6C882A,
  mainColors[2]=color(77,50,47);    //0x4D322F, 0xFDA230,
  mainColors[3]=color(128,40,60);   //0x80283C, 0xAD011E,
  
  initAll();
  startTime=millis();
}

void initAll() {
  f=0;
  background(mainColors[0]);
  colorMode(RGB,255);
  
  for (int i=1;i<=nrOfAttractors;i++) {
    atr[i]=new Attractor(i);
    at_xmov[i]=random(-3.2,3.2);
    at_ymov[i]=random(-3.2,3.2);
    char Cap; String Voc;
    Cap=char(65+int(25*atr[i].x/(float)WID));
    uniqueName+=Cap;
    float tmp=int(4*atr[i].y/(float)HEI);
    if (tmp==0) {Voc="a";} else if (tmp==1) {Voc="e";} else if (tmp==2) {Voc="i";} else if (tmp==3) {Voc="o";} else  {Voc="o";}
    uniqueName+=Voc;
  }
  
  uniqueName+=" ].tga";
  drawFrames();
}




void loop() {
  // normal activity
  drawFrames();
  updateAttractors();
}




void drawFrames() {
  if (f<nrOfFramesToDraw) {
    f++; if (f%100==0) {print("frame: "+f+" - time: "+int((millis()-startTime)/1000)+"...  ");}
    for (int i=1;i<=nrOfAttractors*nrOfDots;i++) {
      dot[i]=new Dotty();
    }
    for (int f=1;f<=nrOfTimesToDraw;f++) {
      for (int i=1;i<=nrOfDots;i++) {
        dot[i].move();
      }
    }
  } else if (f==nrOfFramesToDraw) {
    f++;
    save(uniqueName);
    println("SAVED! "+uniqueName+" render time: "+int((millis()-startTime)/1000)+"sec");
  } 
}



// Dotty CLASS -----------------------------------------------------------
// -----------------------------------------------------------------------

int nrOfDots=50;  // was 10
Dotty[] dot = new Dotty[nrOfAttractors*nrOfDots+1];

class Dotty {
  int type;
  int subtype;
  float rr,gg,bb;
  float tr,tg,tb;
  color col;
  color bgCol;
  
  float x=0,y=0;
  float xmov,ymov,ang,speed;
  
  Dotty() {
    init();
  }
  void init() {
    subtype=1;
    type=int(1+random(nrOfAttractors));
    int off=40;
    do { rr=atr[type].rr+random(-off,off); } while (rr<0||rr>255); 
    do { gg=atr[type].gg+random(-off,off); } while (gg<0||gg>255);
    do { bb=atr[type].bb+random(-off,off); } while (bb<0||bb>255);
    col=color(rr,gg,bb);
    for (int i=1;i<=nrOfAttractors;i++) {
      if (i!=type) {
        x+=atr[type].x+random(-1,1); 
        y+=atr[type].y+random(-1,1);
      }
    }
    
    ang=atr[type].ang;
    speed=type;
  }
  
  void move() {
    if (brightness(col)<128) {
      ang+=5.0/dist(x,y,atr[type].x,atr[type].y);
    } else {
      ang-=5.0/dist(x,y,atr[type].x,atr[type].y);
    }
    xmov=speed*cos(ang);
    ymov=speed*sin(ang);
    x+=xmov; if (x>WID) {x-=WID;} if (x<0) {x+=WID;}
    y+=ymov; if (y>HEI) {y-=HEI;} if (y<0) {y+=HEI;}


    bgCol=get(int(x*mainScale),int(y*mainScale));
    tr=(5.0*red(bgCol)+rr)/6.0;
    tg=(5.0*green(bgCol)+gg)/6.0;
    tb=(5.0*blue(bgCol)+bb)/6.0;
    col=color(tr,tg,tb);
    
    set(int(x*mainScale),int(y*mainScale),col);   
  }
}

// Atractor CLASS -----------------------------------------------------------
// --------------------------------------------------------------------------

void updateAttractors() {
  for (int i=1;i<=nrOfAttractors;i++) {
    at_ymov[i]+=0.1;
    at_xmov[i]*=0.75+brightness(atr[i].col)/1024.0;
    at_ymov[i]*=0.75+brightness(atr[i].col)/1024.0;
    if (atr[i].y+at_ymov[i]>HEI) {at_ymov[i]*=-0.9;}
    atr[i].x+=at_xmov[i];
    atr[i].y+=at_ymov[i];
  }
}

class Attractor {
  int nr;
  float x,y;
  float rr,gg,bb, avRGB;
  color col;
  float ang;
  Attractor(int n) {
    nr=n;
    init();
  }
  void init() {
    ang=random(-PI,PI);
    if (nr%3==0) {
      rr=red(mainColors[1]);
      gg=green(mainColors[1]);
      bb=blue(mainColors[1]);
    } else if (nr%3==1) {
      rr=red(mainColors[2]);
      gg=green(mainColors[2]);
      bb=blue(mainColors[2]);
    } else {
      rr=red(mainColors[3]);
      gg=green(mainColors[3]);
      bb=blue(mainColors[3]);
    }
    col=color(rr,gg,bb);
    avRGB=rr+gg+bb/3.0;
    x=random(WID);
    y=random(HEI);
  }
  void show() {
    noStroke();
    fill(rr,gg,bb);
    ellipse(x,y,20,20);
  }
}

