// Dotty CLASS -----------------------------------------------------------
// -----------------------------------------------------------------------

class Dotty {
  int type;
  int subtype;
  float rr,gg,bb;
  float tr,tg,tb;
  color col;
  color bgCol;
  
  float x=0,y=0;
  float xmov,ymov,ang,speed;
  
  Dotty() {
    init();
  }
  void init() {
    subtype=1;
    type=int(1+random(nrOfAttractors));
    rr=atr[type].rr+random(atr[type].r1,atr[type].r2); 
    gg=atr[type].gg+random(atr[type].g1,atr[type].g2);
    bb=atr[type].bb+random(atr[type].b1,atr[type].b2);
    col=color(rr,gg,bb);
    for (int i=1;i<=nrOfAttractors;i++) {
      if (i!=type) {
        x+=atr[type].x+random(-1,1); 
        y+=atr[type].y+random(-1,1);
      }
    }
    
    ang=atr[type].ang;//random(-0.02,0.02);
    speed=type;//random(0.4,2.2);
  }
  
  void move() {
    if (brightness(col)<128) {
      ang+=5/dist(x,y,atr[type].x,atr[type].y);
    } else {
      ang-=5/dist(x,y,atr[type].x,atr[type].y);
    }
    xmov=speed*cos(ang);
    ymov=speed*sin(ang);
    x+=xmov; if (x>width) {x-=width;} if (x<1) {x+=width;}
    y+=ymov; if (y>height) {y-=height;} if (y<1) {y+=height;}

    bgCol=get(int(x),int(y));
    tr=(5.0*red(bgCol)+rr)/6.0;
    tg=(5.0*green(bgCol)+gg)/6.0;
    tb=(5.0*blue(bgCol)+bb)/6.0;
    col=color(tr,tg,tb);
    
    set(int(x),int(y),col);   
  }
}
