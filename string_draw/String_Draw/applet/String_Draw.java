import processing.core.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class String_Draw extends PApplet {int nrOfAttractors=12;
Attractor[] atr = new Attractor[nrOfAttractors+1];
float[] at_xmov=new float[nrOfAttractors+1];
float[] at_ymov=new float[nrOfAttractors+1];

int nrOfDots=10;
Dotty[] dot = new Dotty[nrOfAttractors*nrOfDots+1];

int nrOfTimesToDraw=100;  // per frame
int nrOfFramesToDraw=3000; //
int f;//=0;
int frameTime;// = millis();

String uniqueName="c:/String Draw "+nrOfFramesToDraw+" [ ";

public void setup() {
  char Cap; 
  String Voc;
  size(800,400,P3D);
  background(128,124,32);
  colorMode(RGB,255);
  ellipseMode(CENTER_RADIUS);
  //framerate(1000);
  for (int i=1;i<=nrOfAttractors;i++) {
    atr[i]=new Attractor(i);
    at_xmov[i]=random(-3.2f,3.2f);
    at_ymov[i]=random(-3.2f,3.2f);
    Cap=PApplet.parseChar(65+PApplet.parseInt(25*atr[i].x/(float)width));
    uniqueName+=Cap;
    float tmp=PApplet.parseInt(4*atr[i].y/(float)height);
    if (tmp==0) {
      Voc="a";
    } 
    else if (tmp==1) {
      Voc="e";
    } 
    else if (tmp==2) {
      Voc="i";
    } 
    else if (tmp==3) {
      Voc="o";
    } 
    else  {
      Voc="o";
    }
    uniqueName+=Voc;
  }
  uniqueName+=" ].tga";
  f=0;
  frameTime=millis();
  drawFrames();
}

public void mousePressed() {
  setup();
}

public void draw() {
  while (millis()-frameTime<40) {
    drawFrames();
    for (int i=1;i<=nrOfAttractors;i++) {
      at_ymov[i]+=0.1f;
      at_xmov[i]*=0.75f+brightness(atr[i].col)/1024.0f;
      at_ymov[i]*=0.75f+brightness(atr[i].col)/1024.0f;
      if (atr[i].y+at_ymov[i]>height) {
        at_ymov[i]*=-0.9f;
      }
      atr[i].x+=at_xmov[i];
      atr[i].y+=at_ymov[i];
    }
  }
  frameTime=millis();
}

public void drawFrames() {
  if (f<nrOfFramesToDraw) {
    f++;
    for (int i=1;i<=nrOfAttractors*nrOfDots;i++) {
      dot[i]=new Dotty();
    }
    for (int f=1;f<=nrOfTimesToDraw;f++) {
      for (int i=1;i<=nrOfDots;i++) {
        dot[i].move();
      }
    }
  } 
  /*
  else if (f==nrOfFramesToDraw) {
      f++; invertPicture();
      delay(500);
    } else if (f==nrOfFramesToDraw+1) { 
      f++; invertPicture();
      save(uniqueName);
    }
    */
}

public void invertPicture() {
  for (int x=1;x<=width;x++) {
    for (int y=1;y<=height;y++) {
      int pc=get(x,y);
      set(x,y,color(255-red(pc),255-green(pc),255-blue(pc)));
    }
  }
}





// Atractor CLASS -----------------------------------------------------------
// --------------------------------------------------------------------------

class Attractor {
  int nr;
  float x,y;
  float rr,gg,bb, avRGB;
  float r1,r2,g1,g2,b1,b2;
  int col;
  float ang;
  Attractor(int n) {
    nr=n;
    init();
    //show();
  }
  public void init() {
    ang=random(0,TWO_PI);
    if (nr%3==0) {
      rr=255; r1=-40; r2=0;
      gg=200; g1=-12; g2=0;
      bb=32;  b1=0; b2=40;
    } else if (nr%3==1) {
      rr=77; r1=-60; r2=0;
      gg=50; g1=0; g2=12;
      bb=47; b1=-20; b2=0;
    } else {
      rr=128;r1=-10;r2=22;
      gg=40;r1=0;r2=40;
      bb=60;b1=-32;b2=13;
    }
    col=color(rr,gg,bb);
    avRGB=rr+gg+bb/3.0f;
    x=random(width);//+width*0.1;
    y=random(height);//+height*0.1;
  }
  public void show() {
    noStroke();
    fill(rr,gg,bb);
    ellipse(x,y,10,10);
  }
}
// Dotty CLASS -----------------------------------------------------------
// -----------------------------------------------------------------------

class Dotty {
  int type;
  int subtype;
  float rr,gg,bb;
  float tr,tg,tb;
  int col;
  int bgCol;
  
  float x=0,y=0;
  float xmov,ymov,ang,speed;
  
  Dotty() {
    init();
  }
  public void init() {
    subtype=1;
    type=PApplet.parseInt(1+random(nrOfAttractors));
    rr=atr[type].rr+random(atr[type].r1,atr[type].r2); 
    gg=atr[type].gg+random(atr[type].g1,atr[type].g2);
    bb=atr[type].bb+random(atr[type].b1,atr[type].b2);
    col=color(rr,gg,bb);
    for (int i=1;i<=nrOfAttractors;i++) {
      if (i!=type) {
        x+=atr[type].x+random(-1,1); 
        y+=atr[type].y+random(-1,1);
      }
    }
    
    ang=atr[type].ang;//random(-0.02,0.02);
    speed=type;//random(0.4,2.2);
  }
  
  public void move() {
    if (brightness(col)<128) {
      ang+=5/dist(x,y,atr[type].x,atr[type].y);
    } else {
      ang-=5/dist(x,y,atr[type].x,atr[type].y);
    }
    xmov=speed*cos(ang);
    ymov=speed*sin(ang);
    x+=xmov; if (x>width) {x-=width;} if (x<1) {x+=width;}
    y+=ymov; if (y>height) {y-=height;} if (y<1) {y+=height;}

    bgCol=get(PApplet.parseInt(x),PApplet.parseInt(y));
    tr=(5.0f*red(bgCol)+rr)/6.0f;
    tg=(5.0f*green(bgCol)+gg)/6.0f;
    tb=(5.0f*blue(bgCol)+bb)/6.0f;
    col=color(tr,tg,tb);
    
    set(PApplet.parseInt(x),PApplet.parseInt(y),col);   
  }
}
static public void main(String args[]) {   PApplet.main(new String[] { "String_Draw" });}}