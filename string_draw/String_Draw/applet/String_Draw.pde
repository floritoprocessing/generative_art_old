int nrOfAttractors=12;
Attractor[] atr = new Attractor[nrOfAttractors+1];
float[] at_xmov=new float[nrOfAttractors+1];
float[] at_ymov=new float[nrOfAttractors+1];

int nrOfDots=10;
Dotty[] dot = new Dotty[nrOfAttractors*nrOfDots+1];

int nrOfTimesToDraw=100;  // per frame
int nrOfFramesToDraw=3000; //
int f;//=0;
int frameTime;// = millis();

String uniqueName="c:/String Draw "+nrOfFramesToDraw+" [ ";

void setup() {
  char Cap; 
  String Voc;
  size(800,400,P3D);
  background(128,124,32);
  colorMode(RGB,255);
  ellipseMode(CENTER_RADIUS);
  //framerate(1000);
  for (int i=1;i<=nrOfAttractors;i++) {
    atr[i]=new Attractor(i);
    at_xmov[i]=random(-3.2,3.2);
    at_ymov[i]=random(-3.2,3.2);
    Cap=char(65+int(25*atr[i].x/(float)width));
    uniqueName+=Cap;
    float tmp=int(4*atr[i].y/(float)height);
    if (tmp==0) {
      Voc="a";
    } 
    else if (tmp==1) {
      Voc="e";
    } 
    else if (tmp==2) {
      Voc="i";
    } 
    else if (tmp==3) {
      Voc="o";
    } 
    else  {
      Voc="o";
    }
    uniqueName+=Voc;
  }
  uniqueName+=" ].tga";
  f=0;
  frameTime=millis();
  drawFrames();
}

void mousePressed() {
  setup();
}

void draw() {
  while (millis()-frameTime<40) {
    drawFrames();
    for (int i=1;i<=nrOfAttractors;i++) {
      at_ymov[i]+=0.1;
      at_xmov[i]*=0.75+brightness(atr[i].col)/1024.0;
      at_ymov[i]*=0.75+brightness(atr[i].col)/1024.0;
      if (atr[i].y+at_ymov[i]>height) {
        at_ymov[i]*=-0.9;
      }
      atr[i].x+=at_xmov[i];
      atr[i].y+=at_ymov[i];
    }
  }
  frameTime=millis();
}

void drawFrames() {
  if (f<nrOfFramesToDraw) {
    f++;
    for (int i=1;i<=nrOfAttractors*nrOfDots;i++) {
      dot[i]=new Dotty();
    }
    for (int f=1;f<=nrOfTimesToDraw;f++) {
      for (int i=1;i<=nrOfDots;i++) {
        dot[i].move();
      }
    }
  } 
  /*
  else if (f==nrOfFramesToDraw) {
      f++; invertPicture();
      delay(500);
    } else if (f==nrOfFramesToDraw+1) { 
      f++; invertPicture();
      save(uniqueName);
    }
    */
}

void invertPicture() {
  for (int x=1;x<=width;x++) {
    for (int y=1;y<=height;y++) {
      color pc=get(x,y);
      set(x,y,color(255-red(pc),255-green(pc),255-blue(pc)));
    }
  }
}




