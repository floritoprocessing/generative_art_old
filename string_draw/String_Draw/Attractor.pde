// Atractor CLASS -----------------------------------------------------------
// --------------------------------------------------------------------------

class Attractor {
  int nr;
  float x,y;
  float rr,gg,bb, avRGB;
  float r1,r2,g1,g2,b1,b2;
  color col;
  float ang;
  Attractor(int n) {
    nr=n;
    init();
    //show();
  }
  void init() {
    ang=random(0,TWO_PI);
    if (nr%3==0) {
      rr=255; r1=-40; r2=0;
      gg=200; g1=-12; g2=0;
      bb=32;  b1=0; b2=40;
    } else if (nr%3==1) {
      rr=77; r1=-60; r2=0;
      gg=50; g1=0; g2=12;
      bb=47; b1=-20; b2=0;
    } else {
      rr=128;r1=-10;r2=22;
      gg=40;r1=0;r2=40;
      bb=60;b1=-32;b2=13;
    }
    col=color(rr,gg,bb);
    avRGB=rr+gg+bb/3.0;
    x=random(width);//+width*0.1;
    y=random(height);//+height*0.1;
  }
  void show() {
    noStroke();
    fill(rr,gg,bb);
    ellipse(x,y,10,10);
  }
}
