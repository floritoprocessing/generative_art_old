int nrOfAttractors=12;
Attractor[] atr = new Attractor[nrOfAttractors+1];
float[] at_xmov=new float[nrOfAttractors+1];
float[] at_ymov=new float[nrOfAttractors+1];

int nrOfDots=10;
Dotty[] dot = new Dotty[nrOfAttractors*nrOfDots+1];

int nrOfTimesToDraw=100;  // per frame
int nrOfFramesToDraw=12000; //8 minutes
int f=0;

float maxWH;

String uniqueName="c:/String Draw "+nrOfFramesToDraw+" [ ";

void setup() {
  char Cap; String Voc;
  // 15cm x 10.5cm @ 300dpi -> 1772px x 1240px
  size(1772,1240); maxWH=1772;
  background(128,124,32);
  colorMode(RGB,255);
  rectMode(CENTER);
  ellipseMode(CENTER);
  frameRate(1000);
  for (int i=1;i<=nrOfAttractors;i++) {
    atr[i]=new Attractor(i);
    float tmpmov=3.2*maxWH/800.0;
    at_xmov[i]=random(-tmpmov,tmpmov);
    at_ymov[i]=random(-tmpmov,tmpmov);
    Cap=char(65+int(25*atr[i].x/(float)width));
    uniqueName+=Cap;
    float tmp=int(4*atr[i].y/(float)height);
    if (tmp==0) {Voc="a";} else if (tmp==1) {Voc="e";} else if (tmp==2) {Voc="i";} else if (tmp==3) {Voc="o";} else  {Voc="o";}
    uniqueName+=Voc;
  }
  uniqueName+=" ].tga";
  drawFrames();
}

void loop() {
  drawFrames();
  for (int i=1;i<=nrOfAttractors;i++) {
    at_ymov[i]+=0.1*maxWH/800.0;
    at_xmov[i]*=0.75+brightness(atr[i].col)/1024.0;
    at_ymov[i]*=0.75+brightness(atr[i].col)/1024.0;
    if (atr[i].y+at_ymov[i]>height) {at_ymov[i]*=-0.9;}
    atr[i].x+=at_xmov[i];
    atr[i].y+=at_ymov[i];
  }
}

void drawFrames() {
  if (f<nrOfFramesToDraw) {
    f++;
    for (int i=1;i<=nrOfAttractors*nrOfDots;i++) {
      dot[i]=new Dotty();
    }
    for (int f=1;f<=nrOfTimesToDraw;f++) {
      for (int i=1;i<=nrOfDots;i++) {
        dot[i].move();
      }
    }
  } else if (f==nrOfFramesToDraw) {
    f++; //invertPicture();
    //delay(500);
  } else if (f==nrOfFramesToDraw+1) { 
    f++; //invertPicture();
    save(uniqueName);
  }
}

void invertPicture() {
  for (int x=1;x<=width;x++) {
    for (int y=1;y<=height;y++) {
      color pc=get(x,y);
      set(x,y,color(255-red(pc),255-green(pc),255-blue(pc)));
    }
  }
}

// Dotty CLASS -----------------------------------------------------------
// -----------------------------------------------------------------------

class Dotty {
  int type;
  int subtype;
  float rr,gg,bb;
  float tr,tg,tb;
  color col;
  color bgCol;
  
  float x=0,y=0;
  float xmov,ymov,ang,speed;
  
  Dotty() {
    init();
  }
  void init() {
    float tmp=maxWH/800.0;
    subtype=1;
    type=int(1+random(nrOfAttractors));
    rr=atr[type].rr+random(atr[type].r1,atr[type].r2); 
    gg=atr[type].gg+random(atr[type].g1,atr[type].g2);
    bb=atr[type].bb+random(atr[type].b1,atr[type].b2);
    col=color(rr,gg,bb);
    for (int i=1;i<=nrOfAttractors;i++) {
      if (i!=type) {
        x+=atr[type].x+random(-tmp,tmp); 
        y+=atr[type].y+random(-tmp,tmp);
      }
    }
    
    ang=atr[type].ang;//random(-0.02,0.02);
    speed=tmp*type;//random(0.4,2.2);
  }
  
  void move() {
    if (brightness(col)<128) {
      ang+=5/dist(x,y,atr[type].x,atr[type].y);
    } else {
      ang-=5/dist(x,y,atr[type].x,atr[type].y);
    }
    xmov=speed*cos(ang);
    ymov=speed*sin(ang);
    x+=xmov; if (x>width) {x-=width;} if (x<1) {x+=width;}
    y+=ymov; if (y>height) {y-=height;} if (y<1) {y+=height;}

    bgCol=get(int(x),int(y));
    tr=(5.0*red(bgCol)+rr)/6.0;
    tg=(5.0*green(bgCol)+gg)/6.0;
    tb=(5.0*blue(bgCol)+bb)/6.0;
    col=color(tr,tg,tb);
    
    set(int(x),int(y),col);   
  }
}

// Atractor CLASS -----------------------------------------------------------
// --------------------------------------------------------------------------

class Attractor {
  int nr;
  float x,y;
  float rr,gg,bb, avRGB;
  float r1,r2,g1,g2,b1,b2;
  color col;
  float ang;
  Attractor(int n) {
    nr=n;
    init();
    //show();
  }
  void init() {
    ang=random(0,TWO_PI);
    if (nr%3==0) {
      rr=255; r1=-40; r2=0;
      gg=200; g1=-12; g2=0;
      bb=32;  b1=0; b2=40;
    } else if (nr%3==1) {
      rr=77; r1=-60; r2=0;
      gg=50; g1=0; g2=12;
      bb=47; b1=-20; b2=0;
    } else {
      rr=128;r1=-10;r2=22;
      gg=40;r1=0;r2=40;
      bb=60;b1=-32;b2=13;
    }
    col=color(rr,gg,bb);
    avRGB=rr+gg+bb/3.0;
    x=random(width);//+width*0.1;
    y=random(height);//+height*0.1;
  }
  void show() {
    noStroke();
    fill(rr,gg,bb);
    ellipse(x,y,20,20);
  }
}