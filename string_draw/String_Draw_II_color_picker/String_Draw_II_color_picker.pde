color[] mainColors;
int pickColor;
boolean nextFrameInit;

int nrOfAttractors=12;  // was 12
Attractor[] atr = new Attractor[nrOfAttractors+1];
float[] at_xmov=new float[nrOfAttractors+1];
float[] at_ymov=new float[nrOfAttractors+1];

int nrOfTimesToDraw;  // per frame
int nrOfFramesToDraw; //
int f;
int frameTime;

String uniqueName="c:/String Draw "+nrOfFramesToDraw+" [ ";

void setup() {
  size(800,400,P3D);

//initPicker();
  nextFrameInit=false;
  mainColors=new color[4];
  mainColors[0]=color(128,124,32);  //0x807C20, 0xCCD67A,
  mainColors[1]=color(255,200,32);  //0xFFC820, 0x6C882A,
  mainColors[2]=color(77,50,47);    //0x4D322F, 0xFDA230,
  mainColors[3]=color(128,40,60);   //0x80283C, 0xAD011E,

  initAll();
}

void initAll() {
  initPicker();
  nrOfTimesToDraw=100;  // per frame
  nrOfFramesToDraw=3000; //
  f=0;
  frameTime=millis();
  background(mainColors[0]);
  colorMode(RGB,255); 
  rectMode(CORNER); 
  ellipseMode(CENTER);
  for (int i=1;i<=nrOfAttractors;i++) {
    atr[i]=new Attractor(i);
    at_xmov[i]=random(-3.2,3.2);
    at_ymov[i]=random(-3.2,3.2);
    char Cap; 
    String Voc;
    Cap=char(65+int(25*atr[i].x/(float)width));
    uniqueName+=Cap;
    float tmp=int(4*atr[i].y/(float)height);
    if (tmp==0) {
      Voc="a";
    } 
    else if (tmp==1) {
      Voc="e";
    } 
    else if (tmp==2) {
      Voc="i";
    } 
    else if (tmp==3) {
      Voc="o";
    } 
    else  {
      Voc="o";
    }
    uniqueName+=Voc;
  }
  uniqueName+=" ].tga";
  drawFrames();
}

void draw() {
  if (nextFrameInit) { 
    nextFrameInit=false; 
    initAll(); 
  }
  if (!pickerActive) {
    while (millis()-frameTime<40) {
    // normal activity
    drawFrames();
    for (int i=1;i<=nrOfAttractors;i++) {
      at_ymov[i]+=0.1;
      at_xmov[i]*=0.75+brightness(atr[i].col)/1024.0;
      at_ymov[i]*=0.75+brightness(atr[i].col)/1024.0;
      if (atr[i].y+at_ymov[i]>height) {
        at_ymov[i]*=-0.9;
      }
      atr[i].x+=at_xmov[i];
      atr[i].y+=at_ymov[i];
    }
    }
  } else {
    // color picker activity
    mainColors[pickColor]=pick(mainColors[pickColor]);
  }
  frameTime=millis();
}

void keyPressed() {
  switch (key) {
    case('0') : 
    pickColor=0; 
    pickerActive=true; 
    break;
    case('1') : 
    pickColor=1; 
    pickerActive=true; 
    break;
    case('2') : 
    pickColor=2; 
    pickerActive=true; 
    break;
    case('3') : 
    pickColor=3; 
    pickerActive=true; 
    break;
    case(' ') : 
    pickerActive=false; 
    nextFrameInit=true; 
    break; 
  }
}

void drawFrames() {
  if (f<nrOfFramesToDraw) {
    f++;
    for (int i=1;i<=nrOfAttractors*nrOfDots;i++) {
      dot[i]=new Dotty();
    }
    for (int ff=1;ff<=nrOfTimesToDraw;ff++) {
      for (int i=1;i<=nrOfDots;i++) {
        dot[i].move();
      }
    }
  } 
  else if (f==nrOfFramesToDraw) {
    f++; 
    invertPicture();
    delay(500);
  } 
  else if (f==nrOfFramesToDraw+1) { 
    f++; 
    invertPicture();
    //    save(uniqueName);
  }
}

void invertPicture() {
  for (int x=1;x<=width;x++) {
    for (int y=1;y<=height;y++) {
      color pc=get(x,y);
      set(x,y,color(255-red(pc),255-green(pc),255-blue(pc)));
    }
  }
}
