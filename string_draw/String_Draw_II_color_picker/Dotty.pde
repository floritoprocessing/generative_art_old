// Dotty CLASS -----------------------------------------------------------
// -----------------------------------------------------------------------

int nrOfDots=50;  // was 10
Dotty[] dot = new Dotty[nrOfAttractors*nrOfDots+1];

class Dotty {
  int type;
  int subtype;
  float rr,gg,bb;
  float tr,tg,tb;
  color col;
  color bgCol;

  float x=0,y=0;
  float xmov,ymov,ang,speed;

  Dotty() {
    init();
  }
  void init() {
    subtype=1;
    type=int(1+random(nrOfAttractors));
    int off=40;
    do { 
      rr=atr[type].rr+random(-off,off); 
    } 
    while (rr<0||rr>255); 
    do { 
      gg=atr[type].gg+random(-off,off); 
    } 
    while (gg<0||gg>255);
    do { 
      bb=atr[type].bb+random(-off,off); 
    } 
    while (bb<0||bb>255);
    col=color(rr,gg,bb);
    for (int i=1;i<=nrOfAttractors;i++) {
      if (i!=type) {
        x+=atr[type].x+random(-1,1); 
        y+=atr[type].y+random(-1,1);
      }
    }

    ang=atr[type].ang;//random(-0.02,0.02);
    speed=type;//random(0.4,2.2);
  }

  void move() {
    if (brightness(col)<128) {
      ang+=5.0/dist(x,y,atr[type].x,atr[type].y);
    } 
    else {
      ang-=5.0/dist(x,y,atr[type].x,atr[type].y);
    }
    xmov=speed*cos(ang);
    ymov=speed*sin(ang);
    x+=xmov; 
    if (x>width) {
      x-=width;
    } 
    if (x<0) {
      x+=width;
    }
    y+=ymov; 
    if (y>height) {
      y-=height;
    } 
    if (y<0) {
      y+=height;
    }

    bgCol=get(int(x),int(y));
    tr=(5.0*red(bgCol)+rr)/6.0;
    tg=(5.0*green(bgCol)+gg)/6.0;
    tb=(5.0*blue(bgCol)+bb)/6.0;
    col=color(tr,tg,tb);

    set(int(x),int(y),col);   
  }
}
