//////////////////////////////////////////////////////////////////////////
////////////////////////////// color picker //////////////////////////////

/*

 USAGE:
 
 In void setup() put initPicker();
 
 In void loop() create code similar to this:
 if (!pickerActive) {
 // normal activity
 } else {
 // color picker activity
 colorX=pick(colorX);
 }
 
 Press [p] or [P] to put color code in hexadecimal to output window
 
 */

// X/Y: top left corner of S/B field;  Size: size of S/B field and height of H field; Circle: size of S/B field picker
int pickSBX=50, pickSBY=50, pickSBSize=300, pickSBCircle=10; 
// X/Y: top left corner of H field;    Size: width of H field; Rect: Size of H field picker
int pickHX=370, pickHY=50, pickHSize=20, pickHRect=10;
// X/Y: top left corner of Sample field;  Size: width and height of Sample field
int pickSampX=410, pickSampY=50, pickSampSize=50;
boolean pickerActive;
PFont VerdanaSmooth;

color pick(color c) {
  updatePicker(c);
  if (mousePressed) {
    // pick S/B
    int x=mouseX, y=mouseY;
    if (x>=pickSBX&&x<=pickSBX+pickSBSize&&y>=pickSBY&&y<=pickSBY+pickSBSize) {
      float s=(x-pickSBX)/(float)pickSBSize;
      float b=1-(y-pickSBY)/(float)pickSBSize;
      colorMode(HSB,1.0); 
      c=color(hue(c),s,b);
    }
    if (x>=pickHX&&x<=pickHX+pickHSize&&y>=pickHY&&y<=pickHY+pickSBSize) {
      float h=1-(y-pickHY)/(float)pickSBSize;
      colorMode(HSB,1.0); 
      c=color(h,saturation(c),brightness(c));
    }
  }
  colorMode(RGB,255);
  return c;
}

void initPicker() {
  VerdanaSmooth=loadFont("Verdana.vlw");
  textFont(VerdanaSmooth,16); //textMode(ALIGN_LEFT);
  pickerActive=false;
}

void updatePicker(color c) {
  background(255,255,255);
  drawSBField(c); 
  drawSBCircle(c);
  drawHField(); 
  drawHRectangle(c);
  drawSampleField(c);
  showHex(c);
}

void drawSBField(color c) {
  for (int x=pickSBX;x<=pickSBX+pickSBSize;x++) { 
    for (int y=pickSBY;y<=pickSBY+pickSBSize;y++) {
      float h=hue(c);
      float s=(x-pickSBX)/(float)pickSBSize;
      float b=1-(y-pickSBY)/(float)pickSBSize;
      colorMode(HSB,1.0); 
      set(x,y,color(h,s,b));
    } 
  }
  rectMode(CORNERS); 
  colorMode(RGB,255); 
  noFill(); 
  stroke(0); 
  rect(pickSBX-1,pickSBY-1,pickSBX+pickSBSize+1,pickSBY+pickSBSize+1);
}

void drawSBCircle(color c) {
  colorMode(HSB,1.0);
  int x=int(saturation(c)*(float)pickSBSize+pickSBX);
  int y=int((1-brightness(c))*(float)pickSBSize+pickSBY);
  ellipseMode(CENTER); 
  colorMode(RGB,255); 
  noFill(); 
  stroke(0); 
  ellipse(x,y,pickSBCircle/2.0,pickSBCircle/2.0);
  noFill(); 
  stroke(255); 
  ellipse(x,y,pickSBCircle/2.0+1,pickSBCircle/2.0+1);
}

void drawHField() {
  for (int y=pickHY;y<=pickHY+pickSBSize;y++) {
    float h=1-(y-pickHY)/(float)pickSBSize;
    float s=1.0; 
    float b=1.0;
    colorMode(HSB,1.0);
    for (int x=pickHX;x<=pickHX+pickHSize;x++) { 
      set(x,y,color(h,s,b)); 
    }
  }
  rectMode(CORNERS); 
  colorMode(RGB,255); 
  noFill(); 
  stroke(0); 
  rect(pickHX-1,pickHY-1,pickHX+pickHSize+1,pickHY+pickSBSize+1);
}

void drawHRectangle(color c) {
  colorMode(HSB,1.0);
  int y=int((1-hue(c))*(float)pickSBSize+pickHY);
  rectMode(CORNER); 
  colorMode(RGB,255); 
  noFill(); 
  stroke(0); 
  rect(pickHX,y-pickHRect/2.0,pickHSize,pickHRect);
}

void drawSampleField(color c) {
  rectMode(CORNERS); 
  colorMode(RGB,255); 
  noFill(); 
  stroke(0); 
  rect(pickSampX-1,pickSampY-1,pickSampX+pickSampSize,pickSampY+pickSampSize);
  fill(c); 
  noStroke(); 
  rect(pickSampX,pickSampY,pickSampX+pickSampSize,pickSampY+pickSampSize);
}

void showHex(color c) {
  int r=int(red(c));
  int g=int(green(c));
  int b=int(blue(c));
  String hex=toHex(r<<16|g<<8|b,6);

  colorMode(RGB,255); 
  noStroke(); 
  fill(0);
  text("red: "+r,pickSampX,pickSampY+pickSampSize+20);
  text("green: "+g,pickSampX,pickSampY+pickSampSize+40);
  text("blue: "+b,pickSampX,pickSampY+pickSampSize+60);
  text("hex: "+hex,pickSampX,pickSampY+pickSampSize+100);
  if (keyPressed&&(key=='p'|key=='P')) { 
    println("color: "+hex); 
  }
}

String toHex(int n, int digits) {
  String o="";
  int t=n;
  while (n>0) {
    t=n%16;
    if (t>=0&&t<10) { 
      o=str(t)+o; 
    } 
    else { 
      o=char(t+55)+o; 
    }
    n-=t; 
    n/=16;
  }
  while (o.length()<digits) {
    o="0"+o;
  }
  return o;
}

////////////////////////////// end color picker //////////////////////////////
//////////////////////////////////////////////////////////////////////////////
