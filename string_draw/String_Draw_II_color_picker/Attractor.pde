// Atractor CLASS -----------------------------------------------------------
// --------------------------------------------------------------------------

class Attractor {
  int nr;
  float x,y;
  float rr,gg,bb, avRGB;
  color col;
  float ang;
  Attractor(int n) {
    nr=n;
    init();
  }
  void init() {
    ang=random(-PI,PI);//random(TWO_PI);
    if (nr%3==0) {
      rr=red(mainColors[1]); //r1=-60; r2=0;
      gg=green(mainColors[1]); //g1=-12; g2=0;
      bb=blue(mainColors[1]);  //b1=0; b2=40;
    } 
    else if (nr%3==1) {
      rr=red(mainColors[2]); //r1=-60; r2=0;
      gg=green(mainColors[2]); //g1=0; g2=12;
      bb=blue(mainColors[2]); //b1=-20; b2=0;
    } 
    else {
      rr=red(mainColors[3]);//r1=-10;r2=22;
      gg=green(mainColors[3]);//r1=0;r2=40;
      bb=blue(mainColors[3]);//b1=-32;b2=13;
    }
    col=color(rr,gg,bb);
    avRGB=rr+gg+bb/3.0;
    x=random(width);//+width*0.1;
    y=random(height);//+height*0.1;
  }
  void show() {
    noStroke();
    fill(rr,gg,bb);
    ellipse(x,y,10,10);
  }
}
