color[] mainColors;

int WID=800, HEI=400;
float mainScale;
float pixScale;

int nrOfTimesToDraw=100; // per frame
int inbetweenPicFrames=500;
int nrOfFramesToDraw=3000; // total

int f;
int startTime;



void setup() {
  initColors();
  size(6400,3200); background(mainColors[0]); colorMode(RGB,255);
  mainScale=width/(float)WID; pixScale=mainScale*mainScale;
  initAttractors();
  initDots();
  createFilename();
  f=0; startTime=millis();
}



void draw() {
  for (int ff=0;ff<100;ff++) {
    if (!paused) {
      drawFrames();
      updateAttractors();
    }

    if (saveTemp) { 
      saveTemp=false;
      
      PImage out = new PImage((int)(width*lowScale),(int)(height*lowScale));
      println(out.width+"x"+out.height);
      out.copy(this.g,0,0,width,height,0,0,out.width,out.height);
      println(out.width+"x"+out.height);
      String tmp = tempName;
      tmp = tmp.replaceAll(lowExt, " ("+nf(frameCount,5)+")"+lowExt);
      println("Temp save: "+tmp);
      out.save(tmp);
    }
  
    frameCount++;
  }
  frameCount--;
}



void drawFrames() {
  if (f<nrOfFramesToDraw) {
    f++;
    if (f==10) {feedbackToUser();}
    if (f==50) {feedbackToUser();}
    if (f%200==0) {feedbackToUser();}
    for (int i=0;i<nrOfAttractors*nrOfDots;i++) { dot[i]=new Dotty(); }
    for (int tf=0;tf<nrOfTimesToDraw;tf++) {
      for (int i=0;i<nrOfDots;i++) { dot[i].move(); }
    }
    
    if ((f%inbetweenPicFrames==0||f==100)&&(f!=nrOfFramesToDraw)) { saveFrame(tempName); println("Temp save: "+tempName); }
    
  } else if (f==nrOfFramesToDraw) {
    f++; 
    save(uniqueName); 
    println(uniqueName+" SAVED!");
    noLoop();
    System.exit(0);
  }
}



//////////////////////////////////////////////////// keyboard inputs while running
boolean saveTemp=false;
boolean paused=false;
void keyPressed() {
  if (key=='S') { saveTemp=true; }
  if (key=='P') { paused=!paused; println(paused?"paused":"running"); }
  if (key=='F') { feedbackToUser();}
}



//////////////////////////////////////////////////// DOTTY
int nrOfDots;
Dotty[] dot;

void initDots() {
  nrOfDots=int(50*pixScale);
  dot=new Dotty[nrOfAttractors*nrOfDots];
}

class Dotty {
  int type;
  float rr,gg,bb;
  float tr,tg,tb;
  color col;
  color bgCol;
  
  float x=0,y=0;
  float xmov,ymov,ang,speed;
  
  Dotty() {
    init();
  }
  
  void init() {
    type=constrain((int)random(nrOfAttractors),0,11);
    int off=40;
    do { rr=atr[type].rr+random(-off,off); } while (rr<0||rr>255); 
    do { gg=atr[type].gg+random(-off,off); } while (gg<0||gg>255);
    do { bb=atr[type].bb+random(-off,off); } while (bb<0||bb>255);
    col=color(rr,gg,bb);
    for (int i=0;i<nrOfAttractors;i++) {
      if (i!=type) {
        x+=atr[type].x+random(-1,1); 
        y+=atr[type].y+random(-1,1);
      }
    }
    
    ang=atr[type].ang;//random(-0.02,0.02);
    speed=(1+type);//random(0.4,2.2);
  }
  
  void move() {
    if (brightness(col)<128) {
      ang+=5.0/dist(x,y,atr[type].x,atr[type].y);
    } else {
      ang-=5.0/dist(x,y,atr[type].x,atr[type].y);
    }
    xmov=speed*cos(ang);
    ymov=speed*sin(ang);
    x+=xmov; while (x>=WID) {x-=WID;} while (x<0) {x+=WID;}
    y+=ymov; while (y>=HEI) {y-=HEI;} while (y<0) {y+=HEI;}

    // DRAW:
    bgCol=get(int(x*mainScale),int(y*mainScale));

    float dwPerc=(1/6.0);///mainScale;
    float bgPerc=1-dwPerc;
    
    tr=bgPerc*red(bgCol)+dwPerc*rr;
    tg=bgPerc*green(bgCol)+dwPerc*gg;
    tb=bgPerc*blue(bgCol)+dwPerc*bb;
    col=color(tr,tg,tb);
    set(int(x*mainScale),int(y*mainScale),col);   
  }
}



//////////////////////////////////////////////////// ATTRACTORS
int nrOfAttractors;
Attractor[] atr;

void initAttractors() {
  nrOfAttractors=12;
  atr=new Attractor[nrOfAttractors];
  for (int i=0;i<nrOfAttractors;i++) { atr[i]=new Attractor(i,random(-3.2,3.2),random(-3.2,3.2)); }
  
/*
  DoLaRaIaKaReCoEoFeBoIeXe
  atr[0].x=103.86901; atr[0].y=385.16272; atr[0].xmov=-1.0603976; atr[0].ymov=1.6637552; atr[0].ang=3.04891; atr[1].x=381.12042; atr[1].y=20.269827; atr[1].xmov=-1.4434172; atr[1].ymov=1.5387061; atr[1].ang=-2.1159186; atr[2].x=574.5004; atr[2].y=31.452978; atr[2].xmov=-1.8969698; atr[2].ymov=-2.2806644; atr[2].ang=-2.2670438; atr[3].x=272.80725; atr[3].y=66.509056; atr[3].xmov=-2.9097939; atr[3].ymov=1.2260058; atr[3].ang=0.87752366; atr[4].x=321.40228; atr[4].y=37.614487; atr[4].xmov=-2.469199; atr[4].ymov=1.0987427; atr[4].ang=-1.699452; atr[5].x=556.8842; atr[5].y=139.22891; atr[5].xmov=-1.0704148; atr[5].ymov=0.509737; atr[5].ang=-2.5490768; atr[6].x=64.9106; atr[6].y=307.54486; atr[6].xmov=1.4089682; atr[6].ymov=3.1480143; atr[6].ang=-2.9447532; atr[7].x=147.09091; atr[7].y=327.45483; atr[7].xmov=2.2017071; atr[7].ymov=2.5824845; atr[7].ang=-2.7385159; atr[8].x=185.46712; atr[8].y=131.8735; atr[8].xmov=0.107266665; atr[8].ymov=1.2498028; atr[8].ang=2.2197983; atr[9].x=43.130104; atr[9].y=396.07233; atr[9].xmov=-2.0260582; atr[9].ymov=2.1748736; atr[9].ang=-2.8347027; atr[10].x=275.0586; atr[10].y=170.38387; atr[10].xmov=-2.33445; atr[10].ymov=-1.6870842; atr[10].ang=0.70195127; atr[11].x=737.719; atr[11].y=189.73248; atr[11].xmov=-1.1199949; atr[11].ymov=0.5840237; atr[11].ang=2.3982694; 
*/
}

void updateAttractors() {
    for (int i=0;i<nrOfAttractors;i++) {
      atr[i].ymov+=0.1;
      atr[i].xmov*=0.75+brightness(atr[i].col)/1024.0;
      atr[i].ymov*=0.75+brightness(atr[i].col)/1024.0;
      if (atr[i].y+atr[i].ymov>HEI) {atr[i].ymov*=-0.9;}
      atr[i].x+=atr[i].xmov;
      atr[i].y+=atr[i].ymov;
      //atr[i].show();
    }
}

class Attractor {
  int nr;
  float x,y;
  float xmov, ymov;
  float rr,gg,bb, avRGB;
  color col;
  float ang;
  
  Attractor(int n, float _xmov, float _ymov) {
    nr=n;
    init();
    xmov=_xmov; ymov=_ymov;
    print("atr["+n+"].x="+x+"; "); print("atr["+n+"].y="+y+"; ");
    print("atr["+n+"].xmov="+xmov+"; "); print("atr["+n+"].ymov="+ymov+"; ");
    println("atr["+n+"].ang="+ang+"; ");
  }
  
  void init() {
    ang=random(-PI,PI);//random(TWO_PI);
    if (nr%3==0) {
      rr=red(mainColors[1]); //r1=-60; r2=0;
      gg=green(mainColors[1]); //g1=-12; g2=0;
      bb=blue(mainColors[1]);  //b1=0; b2=40;
    } else if (nr%3==1) {
      rr=red(mainColors[2]); //r1=-60; r2=0;
      gg=green(mainColors[2]); //g1=0; g2=12;
      bb=blue(mainColors[2]); //b1=-20; b2=0;
    } else {
      rr=red(mainColors[3]);//r1=-10;r2=22;
      gg=green(mainColors[3]);//r1=0;r2=40;
      bb=blue(mainColors[3]);//b1=-32;b2=13;
    }
    col=color(rr,gg,bb);
    avRGB=rr+gg+bb/3.0;
    x=random(WID);//+width*0.1;
    y=random(HEI);//+height*0.1;
  }
  
  void show() {
    noStroke();
    fill(rr,gg,bb);
    ellipse(x*mainScale,y*mainScale,20*mainScale,20*mainScale);
  }
  
}



//////////////////////////////////////////////////// COLORS
void initColors() {
  mainColors=new color[4];
  String baseName; baseName="LivingroomTextures";
  int nr;
  PImage[] img; img=new PImage[4];
  for (int i=0;i<4;i++) {
    nr=int(random(13)+1);
    img[i]=loadImage(baseName+nr+".jpg");
    color c=img[i].pixels[(int)random(img[i].pixels.length)];
    mainColors[i]=c;
    println("//color "+nr+" is taken from image "+baseName+nr);
    println("mainColors["+i+"]=color("+(int)red(c)+","+(int)green(c)+","+(int)blue(c)+");");
  }
//  mainColors[0]=color(128,124,32);  //0x807C20, 0xCCD67A,
//  mainColors[1]=color(255,200,32);  //0xFFC820, 0x6C882A,
//  mainColors[2]=color(77,50,47);    //0x4D322F, 0xFDA230,
//  mainColors[3]=color(128,40,60);   //0x80283C, 0xAD011E,
}



//////////////////////////////////////////////////// FILENAMING
String uniqueName;
String tempName;
String highExt = ".bmp";
String lowExt = ".bmp";
float lowScale = 0.15;
void createFilename() {
  uniqueName="V:\\Processing visual index new\\String_Draw_III_Livingroom ";
  tempName = "V:\\Processing visual index new\\String_Draw_III_Livingroom\\String_Draw_III_Livingroom ";
  //uniqueName="c:/String Draw III vLivingroom - ";
  //tempName="C:/String Draw III vLivingroom (temp) - ";
  for (int i=0;i<nrOfAttractors;i++){
    char Cap; String Voc;
    Cap=char(65+int(25*atr[i].x/(float)WID));
    uniqueName+=Cap; tempName+=Cap;
    float tmp=int(4*atr[i].y/(float)HEI);
    if (tmp==0) {Voc="a";} else if (tmp==1) {Voc="e";} else if (tmp==2) {Voc="i";} else if (tmp==3) {Voc="o";} else  {Voc="o";}
    uniqueName+=Voc; tempName+=Voc;
  }
  uniqueName+=" ["+width+"x"+height+"]"+highExt;
  tempName+=" ["+width+"x"+height+"]"+lowExt;
}



//////////////////////////////////////////////////// FEEDBACK TO USER
void feedbackToUser() {
  print(f+"/"+nrOfFramesToDraw+" ("+int(100*f/(float)nrOfFramesToDraw)+"%)");
  
  int timePassed=int((millis()-startTime)/1000);
  String seconds=((timePassed%60)<10)?(("0"+(timePassed%60))):(""+timePassed%60); 
  String minutes=""+(timePassed/60);
  
  print(" time: "+minutes+":"+seconds+" (");
  
  timePassed=int(timePassed*nrOfFramesToDraw/(float)f);
  seconds=((timePassed%60)<10)?("0"+(timePassed%60)):(""+timePassed%60); 
  minutes=""+(timePassed/60);
  println(minutes+":"+seconds+")");
}
