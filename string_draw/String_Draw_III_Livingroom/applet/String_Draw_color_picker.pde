color[] mainColors;
int pickColor;
boolean nextFrameInit;

int nrOfAttractors=12;
Attractor[] atr = new Attractor[nrOfAttractors+1];
float[] at_xmov=new float[nrOfAttractors+1];
float[] at_ymov=new float[nrOfAttractors+1];

int nrOfTimesToDraw;  // per frame
int nrOfFramesToDraw; //
int f;

String uniqueName="c:/String Draw "+String(nrOfFramesToDraw)+" [ ";

void setup() {
  size(800,400);
  
  nextFrameInit=false;
  mainColors=new color[4];
  mainColors[0]=color(128,124,32);
  mainColors[1]=color(255,200,32);
  mainColors[2]=color(77,50,47);
  mainColors[3]=color(128,40,60);
  
  initAll();
}

void initAll() {
  initPicker();
  nrOfTimesToDraw=100;  // per frame
  nrOfFramesToDraw=3000; //
  f=0;
  background(mainColors[0]);
  colorMode(RGB,255); rectMode(CENTER_DIAMETER); ellipseMode(CENTER_DIAMETER);
  for (int i=1;i<=nrOfAttractors;i++) {
    atr[i]=new Attractor(i);
    at_xmov[i]=random(-3.2,3.2);
    at_ymov[i]=random(-3.2,3.2);
    char Cap; String Voc;
    Cap=char(65+int(25*atr[i].x/(float)width));
    uniqueName+=Cap;
    float tmp=int(4*atr[i].y/(float)height);
    if (tmp==0) {Voc="a";} else if (tmp==1) {Voc="e";} else if (tmp==2) {Voc="i";} else if (tmp==3) {Voc="o";} else  {Voc="o";}
    uniqueName+=Voc;
  }
  uniqueName+=" ].tga";
  drawFrames();
}

void loop() {
  if (nextFrameInit) { nextFrameInit=false; initAll(); }
  if (!pickerActive) {
    // normal activity
    drawFrames();
    for (int i=1;i<=nrOfAttractors;i++) {
      at_ymov[i]+=0.1;
      at_xmov[i]*=0.75+brightness(atr[i].col)/1024.0;
      at_ymov[i]*=0.75+brightness(atr[i].col)/1024.0;
      if (atr[i].y+at_ymov[i]>height) {at_ymov[i]*=-0.9;}
      atr[i].x+=at_xmov[i];
      atr[i].y+=at_ymov[i];
    }
  } else {
    // color picker activity
    mainColors[pickColor]=pick(mainColors[pickColor]);
  }
}

void keyPressed() {
  switch (key) {
    case('0') : pickColor=0; pickerActive=true; break;
    case('1') : pickColor=1; pickerActive=true; break;
    case('2') : pickColor=2; pickerActive=true; break;
    case('3') : pickColor=3; pickerActive=true; break;
    case(' ') : pickerActive=false; nextFrameInit=true; break; 
  }
}

void drawFrames() {
  if (f<nrOfFramesToDraw) {
    f++;
    for (int i=1;i<=nrOfAttractors*nrOfDots;i++) {
      dot[i]=new Dotty();
    }
    for (int f=1;f<=nrOfTimesToDraw;f++) {
      for (int i=1;i<=nrOfDots;i++) {
        dot[i].move();
      }
    }
  } else if (f==nrOfFramesToDraw) {
    f++; invertPicture();
    delay(500);
  } else if (f==nrOfFramesToDraw+1) { 
    f++; invertPicture();
//    save(uniqueName);
  }
}

void invertPicture() {
  for (int x=1;x<=width;x++) {
    for (int y=1;y<=height;y++) {
      color pc=get(x,y);
      set(x,y,color(255-red(pc),255-green(pc),255-blue(pc)));
    }
  }
}

// Dotty CLASS -----------------------------------------------------------
// -----------------------------------------------------------------------

int nrOfDots=50;  // was 10
Dotty[] dot = new Dotty[nrOfAttractors*nrOfDots+1];

class Dotty {
  int type;
  int subtype;
  float rr,gg,bb;
  float tr,tg,tb;
  color col;
  color bgCol;
  
  float x=0,y=0;
  float xmov,ymov,ang,speed;
  
  Dotty() {
    init();
  }
  void init() {
    subtype=1;
    type=int(1+random(nrOfAttractors));
    int off=40;
    do { rr=atr[type].rr+random(-off,off); } while (rr<0||rr>255); 
    do { gg=atr[type].gg+random(-off,off); } while (gg<0||gg>255);
    do { bb=atr[type].bb+random(-off,off); } while (bb<0||bb>255);
    col=color(rr,gg,bb);
    for (int i=1;i<=nrOfAttractors;i++) {
      if (i!=type) {
        x+=atr[type].x+random(-1,1); 
        y+=atr[type].y+random(-1,1);
      }
    }
    
    ang=atr[type].ang;//random(-0.02,0.02);
    speed=type;//random(0.4,2.2);
  }
  
  void move() {
    if (brightness(col)<128) {
      ang+=5/dist(x,y,atr[type].x,atr[type].y);
    } else {
      ang-=5/dist(x,y,atr[type].x,atr[type].y);
    }
    xmov=speed*cos(ang);
    ymov=speed*sin(ang);
    x+=xmov; if (x>width) {x-=width;} if (x<0) {x+=width;}
    y+=ymov; if (y>height) {y-=height;} if (y<0) {y+=height;}

    bgCol=get(int(x),int(y));
    tr=(5.0*red(bgCol)+rr)/6.0;
    tg=(5.0*green(bgCol)+gg)/6.0;
    tb=(5.0*blue(bgCol)+bb)/6.0;
    col=color(tr,tg,tb);
    
    set(int(x),int(y),col);   
  }
}

// Atractor CLASS -----------------------------------------------------------
// --------------------------------------------------------------------------

class Attractor {
  int nr;
  float x,y;
  float rr,gg,bb, avRGB;
//  float r1,r2,g1,g2,b1,b2;
  color col;
  float ang;
  Attractor(int n) {
    nr=n;
    init();
    //show();
  }
  void init() {
    ang=random(0,TWO_PI);
    if (nr%3==0) {
      rr=red(mainColors[1]); //r1=-60; r2=0;
      gg=green(mainColors[1]); //g1=-12; g2=0;
      bb=blue(mainColors[1]);  //b1=0; b2=40;
    } else if (nr%3==1) {
      rr=red(mainColors[2]); //r1=-60; r2=0;
      gg=green(mainColors[2]); //g1=0; g2=12;
      bb=blue(mainColors[2]); //b1=-20; b2=0;
    } else {
      rr=red(mainColors[3]);//r1=-10;r2=22;
      gg=green(mainColors[3]);//r1=0;r2=40;
      bb=blue(mainColors[3]);//b1=-32;b2=13;
    }
    col=color(rr,gg,bb);
    avRGB=rr+gg+bb/3.0;
    x=random(width);//+width*0.1;
    y=random(height);//+height*0.1;
  }
  void show() {
    noStroke();
    fill(rr,gg,bb);
    ellipse(x,y,20,20);
  }
}







//////////////////////////////////////////////////////////////////////////
////////////////////////////// color picker //////////////////////////////

/*

USAGE:

In void setup() put initPicker();

In void loop() create code similar to this:
if (!pickerActive) {
  // normal activity
} else {
  // color picker activity
  colorX=pick(colorX);
}

Press [p] or [P] to put color code in hexadecimal to output window

*/

// X/Y: top left corner of S/B field;  Size: size of S/B field and height of H field; Circle: size of S/B field picker
int pickSBX=50, pickSBY=50, pickSBSize=300, pickSBCircle=10; 
// X/Y: top left corner of H field;    Size: width of H field; Rect: Size of H field picker
int pickHX=370, pickHY=50, pickHSize=20, pickHRect=10;
// X/Y: top left corner of Sample field;  Size: width and height of Sample field
int pickSampX=410, pickSampY=50, pickSampSize=50;
boolean pickerActive;
BFont VerdanaSmooth;

color pick(color c) {
  updatePicker(c);
  if (mousePressed) {
    // pick S/B
    int x=mouseX, y=mouseY;
    if (x>=pickSBX&&x<=pickSBX+pickSBSize&&y>=pickSBY&&y<=pickSBY+pickSBSize) {
      float s=(x-pickSBX)/(float)pickSBSize;
      float b=1-(y-pickSBY)/(float)pickSBSize;
      colorMode(HSB,1.0); c=color(hue(c),s,b);
    }
    if (x>=pickHX&&x<=pickHX+pickHSize&&y>=pickHY&&y<=pickHY+pickSBSize) {
      float h=1-(y-pickHY)/(float)pickSBSize;
      colorMode(HSB,1.0); c=color(h,saturation(c),brightness(c));
    }
  }
  colorMode(RGB,255);
  return c;
}

void initPicker() {
  VerdanaSmooth=loadFont("Verdana.vlw");
  textFont(VerdanaSmooth,16); textMode(ALIGN_LEFT);
  pickerActive=false;
}

void updatePicker(color c) {
  background(255,255,255);
  drawSBField(c); drawSBCircle(c);
  drawHField(); drawHRectangle(c);
  drawSampleField(c);
  showHex(c);
}

void drawSBField(color c) {
  for (int x=pickSBX;x<=pickSBX+pickSBSize;x++) { for (int y=pickSBY;y<=pickSBY+pickSBSize;y++) {
    float h=hue(c);
    float s=(x-pickSBX)/(float)pickSBSize;
    float b=1-(y-pickSBY)/(float)pickSBSize;
    colorMode(HSB,1.0); set(x,y,color(h,s,b));
  } }
  rectMode(CORNERS); colorMode(RGB,255); noFill(); stroke(0); rect(pickSBX-1,pickSBY-1,pickSBX+pickSBSize+1,pickSBY+pickSBSize+1);
}

void drawSBCircle(color c) {
  colorMode(HSB,1.0);
  int x=int(saturation(c)*(float)pickSBSize+pickSBX);
  int y=int((1-brightness(c))*(float)pickSBSize+pickSBY);
  ellipseMode(CENTER_DIAMETER); colorMode(RGB,255); 
  noFill(); stroke(0); ellipse(x,y,pickSBCircle,pickSBCircle);
  noFill(); stroke(255); ellipse(x,y,pickSBCircle+2,pickSBCircle+2);
}

void drawHField() {
  for (int y=pickHY;y<=pickHY+pickSBSize;y++) {
    float h=1-(y-pickHY)/(float)pickSBSize;
    float s=1.0; float b=1.0;
    colorMode(HSB,1.0);
    for (int x=pickHX;x<=pickHX+pickHSize;x++) { set(x,y,color(h,s,b)); }
  }
  rectMode(CORNERS); colorMode(RGB,255); noFill(); stroke(0); rect(pickHX-1,pickHY-1,pickHX+pickHSize+1,pickHY+pickSBSize+1);
}

void drawHRectangle(color c) {
  colorMode(HSB,1.0);
  int y=int((1-hue(c))*(float)pickSBSize+pickHY);
  rectMode(CENTER_DIAMETER); colorMode(RGB,255); noFill(); stroke(0); rect(pickHX+pickHSize/2.0,y,pickHSize,pickHRect);
}

void drawSampleField(color c) {
  rectMode(CORNERS); colorMode(RGB,255); noFill(); stroke(0); rect(pickSampX-1,pickSampY-1,pickSampX+pickSampSize,pickSampY+pickSampSize);
  fill(c); noStroke(); rect(pickSampX,pickSampY,pickSampX+pickSampSize,pickSampY+pickSampSize);
}

void showHex(color c) {
  int r=int(red(c));
  int g=int(green(c));
  int b=int(blue(c));
  String hex=toHex(r<<16|g<<8|b,6);
  
  colorMode(RGB,255); noStroke(); fill(0);
  text("red: "+r,pickSampX,pickSampY+pickSampSize+20);
  text("green: "+g,pickSampX,pickSampY+pickSampSize+40);
  text("blue: "+b,pickSampX,pickSampY+pickSampSize+60);
  text("hex: "+hex,pickSampX,pickSampY+pickSampSize+100);
  if (keyPressed&&(key=='p'|key=='P')) { println("color: "+hex); }
}

String toHex(int n, int digits) {
  String o="";
  int t=n;
  while (n>0) {
    t=n%16;
    if (t>=0&&t<10) { o=String(t)+o; } else { o=char(t+55)+o; }
    n-=t; n/=16;
  }
  while (o.length()<digits) {o="0"+o;}
  return o;
}

////////////////////////////// end color picker //////////////////////////////
//////////////////////////////////////////////////////////////////////////////
