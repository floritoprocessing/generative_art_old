void setup() {
  size(800,600,P3D);
  loadPixels();
}

void draw() {
  for (int i=0;i<pixels.length;i++) {
//    int br = (int)(Math.random()<0.5?0:255);
    int br = (int)random(0,255);//
    pixels[i] = 0xff<<24|br<<16|br<<8|br;
  }
  updatePixels();
}
