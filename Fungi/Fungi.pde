//Fungi

float a,b,c,d,e,f,g,h,z;

void setup() {
  size(640,480,P3D);
  background(0);
  next();
}

void next() {
  a=random(-4.0,4.0);
  b=random(-4.0,4.0);
  c=random(-4.0,4.0);
  d=random(-4.0,4.0);
  e=random(-4.0,4.0);
  f=random(-4.0,4.0); 
  g=random(-4.0,4.0);
  h=random(-4.0,4.0);
}

void mousePressed() {
  next();
}

void draw() {
  background(0);
  translate(width/2.0,height/2.0);
  color col=color(255,50,5,16);
  stroke(col);
  for (int i=0;i<200;i++) {
    float x = random(-1.0,1.0);
    float y = random(-1.0,1.0);
    for (int j=0; j<2000; j++) {
      float xp = sin(a*x) + sin(b*y) + sin(c*z); 
      float yp = sin(d*x) + sin(e*y) + sin(f*z);
      float zp = sin(g*x) + sin(h*y) + sin(i*z);
      x=xp;
      y=yp;
      //z=zp;
      point(x*100,y*100);
    }
  } 
}
