PImage img;
int maxDots=120001;
int nrOfDots;
Dot[] dot=new Dot[maxDots+1];
float lmx=0.0, lmy=0.0, mxmov=0.0, mymov=0.0;

void setup() {
  size(400,300);
  colorMode(RGB, 255);
  img = loadImage("Grass400x300.jpg"); 

  int i=0;
  for (int y=1;y<=height;y++) {
    for (int x=1;x<=width;x++) {
      color c=img.pixels[int(y-1)*width+int(x-1)];
      if (random(1)<0.15) {
        dot[i]=new Dot(x,y,c);
        i++;
      }
    }
  }
  nrOfDots=i-1;
}

void draw() {
  background(0);
  for (int i=1;i<=nrOfDots;i++) {
    dot[i].update();
  }
}

void mousePressed() {
  lmx=mouseX; lmy=mouseY;
}
void mouseDragged() {
  mxmov=mouseX-lmx;
  mymov=mouseY-lmy;
  lmx=mouseX; lmy=mouseY;
}

class Dot {
  float x,y,sx,sy;
  float d,dm=20000.0;
  float xmov=0,ymov=0;
  color col=color(255,255,255);
  
  Dot(int x, int y, color c) {
    col=c;
    xmov=random(-0.5,0.5);
    ymov=random(-0.5,0.5);
    sx=x;sy=y;
    this.x=x; this.y=y;
  }
  
  void update() {
    if (mousePressed) {
      dm=dist(x,y,mouseX,mouseY);
    } else {
      dm=(3*dm+20000)/4;
    }
    d=pow(1/(1+dm),1.7);
    xmov+=2*mxmov*d;
    ymov+=2*mymov*d;
      
    d=dist(x,y,sx,sy);
    xmov-=d*0.02*(x-sx);
    ymov-=d*0.02*(y-sy);
    xmov*=0.98;
    ymov*=0.98;
    x+=xmov;y+=ymov;
    
    if (x>=1&&x<=width&&y>=1&&y<=height) {set(int(x),int(y),col);}
  }
}
